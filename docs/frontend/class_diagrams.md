# Unity GameObjectekre kapcsolt komponens scriptek

Mivel nem ismerjük 100%-ban a Unity lelki világát, így elvárható a későbbi változtatása ezeknek az ábráknak.

> Itt minden osztály a `MonoBehavior`-től örököl.

### SceneRoot
```plantuml
	class SceneRootController {
		GameData gameData;
	}
```
- A `SceneRootConroller` felel a frontend és backend közti kommunikációért.
- A `SceneRootConroller` tartalmaz egy referenciát a `GameData` objektumra, amelly a játék logikáját leíró, azt számon tartó objektum (Model).
- A `SceneRootController`-nek kötelezően lesz egy `Camera`, `Map` és `UI` gyereke.

### Camera
```plantuml
	class CameraConroller {
		float speed
		float angle
		float angularVelocity
		Vector3 position
		MapController map
	}
```
- A `CameraController` felel a kamera pozíciójáért, annak mozgatásáért a felhasználó által lenyomott gombok alapján.
- A kamera nem kerül el a játéktér felől.
- A kamerát lehet forgatni a vertikális tengely körül.

### Map
```plantuml
	class MapController {
		
	}
```
- A `MapController`-nek kötelezően lesz `SpecialGrid`, `Trees`, `Streets`, `ZoneGrid`, `Fire` gyereke.

### ZoneGrid
```plantuml
	class ZoneGridController {
	}
```
- A `ZoneGridController`-nek lesznek `IndustrialZone`, `ServiceZone`,  `AccomodationZone`, gyerekei.
- Feliratkozik a gyerekei eventjeire.
- Lekéri a `Backend`-től, a zónainformációt.
### IndustrialZoneTile/ServiceZoneTile/AccomodationZoneTile/DefaultZoneTile
```plantuml
	class ZoneTile {
		event ZoneSelected()
	}
```
- Szól ha rákattintanak.

### IndustrialBuilding/ServiceBuilding/AccomodationBuilding
```plantuml
	class BuildingController {
		GetBuildingData()
	}
```
- A `BuildingController` kéri le a backendről a tárolt adatokat, és felel a megjelenítésért.
### PoliceStation/FireStation/Stadium/PowerPlant/PublicSchool/University
```plantuml
	class SpecialBuildingController {
		GetBuildingData()
	}
```
- A `SpecialBuildingController` kéri le a backendről a tárolt adatokat, és felel a megjelenítésért.

### Streets
```plantuml
	class StreetsController {
		
	}
```
- A `StreetsController` gyerekei a `StreetTile` elemek, amelyeket a `StreetsController` tart nyilván.

### StreetTile
```plantuml
	class StreetTile {
		event StreetSelected()
	}
```
- Szól ha rákattintanak.

### Trees
```plantuml
class TreesController {
}
```
- A `TreesController` gyerekei a `TreeTile` objektumok, amelyeket a `TreesController` kezel.

### TreeTile
```plantuml
	class TreeTile {
		event TreeSelected()
	}
```
- Szól ha rákattintanak.

### Electricity
```plantuml
	class ElectricWiresController {
	}
```
- Az `ElectricWiresController` gyerekei a `ElectricityWire` objektumok, amelyeket a `ElectricWiresController` kezel.

### SpecialBuilding
```plantuml
	class SpecialGridController {
	}
```
- A `SpecialGridController`-nek lesznek `PoliceStation`, `FireStation`, `Stadium`, `PowerPlant`, `PublicSchool`, `University` gyerekei.
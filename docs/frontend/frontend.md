# Simcity -- Lorem Frontend dokumentáció (átlátható verzió)

## Használt technológia

A gyors és könnyebb fejlesztés érdekében a Unity nevű játékfejlesztő szoftvert fogjuk használni.
A frontend dokumentáció három részből áll, a felhasználó által elvégezhető akciókból [[usecases]], az azt megvalósító rendszertervből [[class_diagrams]] és a wireframe tervekből [[SimcityMockup.pdf]]

## Unity GameObject hierarchia (nagy vonalakban)

```plantuml
	left to right direction

	rectangle SceneRoot
	(Map) - (SceneRoot)
	(SceneRoot) - (UI)
	(SceneRoot) - (Camera)
	(Map) -- (Zone Grid)
	(Map) -- (Trees)
	(Map) -- (Electricity)
	(Map) -- (Streets)
	(Zone Grid) -- (IndustrialZones)
	(Zone Grid) -- (ServiceZones)
	(Zone Grid) -- (AccomodationZones)
	(Zone Grid) -- (DefaultZones)
	(Building) -- (Special Building)
	(Map) -- (Building) 
	(Special Building) -- (Police Stations)
	(Special Building) -- (Fire Stations)
	(Special Building) -- (Stadiums)
	(Special Building) -- (PowerPlants)
	(Special Building) -- (PublicSchools)
	(Special Building) -- (Universities)
	(Building) -- (IndustrialBuildings)
	(Building) -- (ServiceBuildings)
	(Building) -- (AccomodationBuildings)
	(Streets) -- (StreetTiles)
	(Electricity) -- (ElectricWires)
	(Trees) -- (TreeTiles)

	(UI) -- (MenuContainer)
	(UI) -- (NewGameContainer)
	(UI) -- (LoadGameContainer)
	(UI) -- (SettingsContainer)

```

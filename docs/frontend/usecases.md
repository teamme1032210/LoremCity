# Átlátható verzió

## Főmenü

A főmenüben lehet kiválasztani, hogy új játékot szeretnénk-e kezdeni, vagy egy már régebbit szeretnénk betölteni.

```plantuml
	rectangle Játék
	:Felh: --> (Start)
	:Felh: --> (Load game)
	:Felh: --> (Settings)
	:Felh: --> (Exit)
	:Felh: --> (Mentés kiválasztása)
	:Felh: --> (Beállítások megváltoztatása)
	(Load game) ..> (Mentés kiválasztása) : <precedes>
	(Start) ..> (Játék) : <invokes>
	(Mentés kiválasztása) ..> Játék : <invokes>
	(Settings) ..> (Beállítások megváltoztatása) : <precedes>
```

## A játék menete

```plantuml
left to right direction
	rectangle Főmenü
	:Felh: --> (Építkezés/Rombolás)
	:Felh: --> (Adók emelése/csökkentése)
    :Felh: --> (A város adatainak megtekintése)
	:Felh: --> (Kilépés a játékból)
	:Felh: --> (Idő lépték beállítása)
	(Kilépés a játékból) ..> Főmenü : <invokes>
```

### Építkezés/Rombolás

```plantuml
	left to right direction
	:Felh: --> (Zóna területek megváltoztatása)
	:Felh: --> (Utak építése)
	:Felh: --> (Erdők ültetése)
	:Felh: --> (Speciális épületek építése)
	:Felh: --> (rombolás)
	note bottom of (rombolás)
		Épületek/Utak/Erdők
	end note
	(Utak építése) ..> (rombolás) : <precedes>
	(Erdők ültetése) ..> (rombolás) : <precedes>
	(Speciális épületek építése) ..> (rombolás) : <precedes>
```

### A város adatainak megtekintése

```plantuml
	left to right direction
	(Speciális épület kiválasztása) as spec
	rectangle "Épület adatok megtekintése" as epulet
	rectangle "Lakos adatok megtekintése" as lakos
	:Felh: --> spec
	:Felh: --> (Zóna épület kiválasztása)
	:Felh: --> (Lakó/Dolgozó kiválasztása)
	spec ..> epulet : <invokes>
	(Zóna épület kiválasztása) ..> epulet : <invokes>
	(Lakó/Dolgozó kiválasztása) ..> lakos : <invokes>
```

# Nem átlátható, de elvárt verzió

```plantuml
 	left to right direction
	rectangle Játék
	rectangle "Építkezés/Rombolás" as er
	rectangle "A város adatainak megtekintése" as va
	:Felh: --> (Start)
	:Felh: --> (Load game)
	:Felh: --> (Settings)
	:Felh: --> (Exit)
	:Felh: --> (Mentés kiválasztása)
	:Felh: --> (Beállítások megváltoztatása)
	(Load game) ..> (Mentés kiválasztása) : <precedes>
	(Start) ..> (Játék) : <invokes>
	(Mentés kiválasztása) ..> Játék : <invokes>
	(Settings) ..> (Beállítások megváltoztatása) : <precedes>
	rectangle Főmenü
	:Felh: --> er
	:Felh: --> (Adók emelése/csökkentése)
    :Felh: --> va
	:Felh: --> (Kilépés a játékból)
	:Felh: --> (Idő lépték beállítása)
	(Kilépés a játékból) ..> Főmenü : <invokes>
	:Felh: --> (Zóna területek megváltoztatása)
	:Felh: --> (Utak építése)
	:Felh: --> (Erdők ültetése)
	:Felh: --> (Speciális épületek építése)
	:Felh: --> (rombolás)
	note bottom of (rombolás)
		Épületek/Utak/Erdők
	end note
	(Utak építése) ..> (rombolás) : <precedes>
	(Erdők ültetése) ..> (rombolás) : <precedes>
	(Speciális épületek építése) ..> (rombolás) : <precedes>
 
	(Speciális épület kiválasztása) as spec
	rectangle "Épület adatok megtekintése" as epulet
	rectangle "Lakos adatok megtekintése" as lakos
	:Felh: --> spec
	:Felh: --> (Zóna épület kiválasztása)
	:Felh: --> (Lakó/Dolgozó kiválasztása)
	spec ..> epulet : <invokes>
	(Zóna épület kiválasztása) ..> epulet : <invokes>
	(Lakó/Dolgozó kiválasztása) ..> lakos : <invokes>

	(Játék) .> er : <precedes>
	(Játék) .> (Adók emelése/csökkentése) : <precedes>
	(Játék) .> (A város adatainak megtekintése) : <precedes>
	(Játék) .> (Kilépés a játékból) : <precedes>
	(Játék) .> (Idő lépték beállítása) : <precedes>

	er .> (Zóna területek megváltoztatása) : <precedes>
	er .> (Utak építése) : <precedes>
	er .> (Erdők ültetése) : <precedes>
	er .> (Speciális épületek építése) : <precedes>

	va .> (Zóna épület kiválasztása) : <precedes>
 
```
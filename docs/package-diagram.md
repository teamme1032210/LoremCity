```plantuml
left to right direction

[Frontend]
[Backend]
() "Unity"

[Frontend] --> Unity : fn
[Frontend] <-- Unity : event
[Backend] <-- [Frontend] : fn
[Backend] --> [Frontend] : event

note top of Unity
	view
end note

note top of Frontend
	presenter
end note

note top of Backend
	model
end note
```

# Simcity -- Lorem dokumentáció

A játék két rétegben fog megvalósulni. Frontend/backend néven fogjuk hívni őket, de lehet model/view néven is hivatkozni rá.

- A frontend a játék megjelenítéséért, a felhasználóval való interaktálásért fog felelni. [[frontend]]
- A backend a játék mechanikái-, folyamataiért felel. [[Backend]]

A két réteget külön fájlokban dolgozzuk ki, hogy könnyebb legyen az együttműködés.
A két réteg közötti kommunikációról a `backend-api.md` fájlban tervezzük leírni. [[backend-api]].
A szoftvert alakító komponensek a [[package-diagram]] fájlban vannak.
```plantuml
package Model.Inhabitants
{
    class Inhabitant
    {
        .. Fields ..
        - Home _home
        - Workplace _workPlace
        - double _satisfaction
        - Qualification _qualification
        - int _age
        - int _currentIncome
        - int _totalIncome
        - int _distanceFromWorkPlace
        - string _name
        .. Properties ..
        {method} + Home Home {get; set;}
        {method} + WorkPlace WorkPlace {get; set;}
        {method} + double Satisfaction {get; set;}
        {method} + Qualification Qualification {get; set;}
        {method} + int Age {get; set;}
        {method} + int CurrentIncome {get; set;}
        {method} + int DistanceFromWorkplace {get;}
        {method} + string Name {get;}
        
        .. Constructors ..
        + Inhabitant(int age, Qualification qualification)
        + Inhabitant()
        .. Methods ..
        - Home PickHome()
        - Workplace PickWorkplace()
        - string GenerateRandomName()
    }
    enum Qualification
    {
        PrimaryEducation,
        SecondaryEducation,
        HigherEducation
    }
    
}
package Model.Utilities
{
    class MapCell
    {
        .. Properties ..
        + {method} Zone? Zone {get; set;}
        + {method} Building? Buildng {get; set;}
        .. Constructors ..
        + MapCell()
    }

    class TransferEventArgs
    {
        .. Fields ..
        - ExpenseType _expenseType
        .. Properties ..
        {method} + ExpenseType ExpenseType {get;}
        .. Constructors ..
        + TransferEventArgs(ExpenseType expenseType)

    }

    class Variation
    {
        .. Fields ..
        - int _sizeX
        - int _sizeY
        - int _textureId
        .. Properties ..
        {method} + int SizeX {get;}
        {method} + int SizeY {get;}
        {method} + int _textureId {get;}
        .. Constructors ..
        + Variation(int sizeX, int sizeY, int textureId)
    }

    class DateResponse
    {
        .. Fields ..
        - int _year
        - int _month
        - int _day
        .. Properties ..
        {method} + int Year {get;}
        {method} + int Month {get;}
        {method} + int Day {get;}
        .. Constructors ..
        + DateResponse(int year, int month, int day)
    }
}
package Model.Zones
{
    class Zone
    {
        .. Fields ..
        - ZoneType _zoneType
        - List<Point> _cells
        - int _currentLevel
        .. Properties ..
        {method} + ZoneType ZoneType {get; set;}
        {method} + List<Point> Cells {get;}
        {method} + int CurrentLevel {get; set;}
        {method} + int Capacity {get;}
        .. Constructors ..
        + Zone(ZoneType zoneType)   
    }
    enum ZoneType
    {
        Special,
        Residential,
        Service,
        Industrial
    }
}

package Model.Data
{
    class GameData
    {
        .. Fields ..
        - int _mapWidth
        - int _mapHeight
        - bool _isPaused
        - int _gameSpeed
        - int _balance
        - int _secondaryEducationRatio
        - int _higherEducationRatio
        - DateTime _gameTime
        - FactoryLayer _factoryLayer
        + {static} Dictionary<HomeVariationKey, Variation> HomeVariationStorage
        + {static} Dictionary<ServiceWorkplaceVariationKey, Variation> ServiceWorkplaceVariationStorage
        + {static} Dictionary<IndustrialWorkplaceVariationKey, Variation> IndustrialWorkplaceVariationStorage
        .. Properties ..
        {method} + FactoryLayer FactoryLayer {get;}
        {method} + int MapHeight {get;}
        {method} + int MapWidth {get;}
        {method} + bool IsPaused {get; set;}
        {method} + int GameSpeed {get; set;}
        {method} + int Balance {get; set;}
        {method} + int Satisfaction {get;}
        {method} + int TotalPowerNeeded {get;}
        {method} + int TotalPowerCapacity {get;}
        {method} + List<Stadium> Stadiums
        {method} + List<PoliceStation> PoliceStations
        {method} + List<FireStation> FireStations
        {method} + List<Road> Roads
        {method} + List<Forest> Forests
        {method} + List<Inhabitant> Inhabitants
        {method} + List<SecondarySchool> SecondarySchools
        {method} + List<University> Universities
        {method} + List<Zone> Zones
        {method} + List<ZoneBuilding> ZoneBuildings
        {method} + List<PowerLine> PowerLines
        {method} + MapCell[,] _cellData
        .. Constructors ..
        + GameData(int mapHeight, int mapWidth, int gameSpeed, DateTime gameTime)
        .. Methods ..
        {method} - void OnTransferEvent(object? sender, TransferEventArgs e)
        {method} - void OnMapLayoutChanged(object? sender, EventArgs e)
        {method} - int GetExpense(ExpenseType e)
        {method} + DateResponse Tick()
        {method} - DateResponse GetGameTime()
        {method} - void CollectTax()
        {method} - void DeductYearlyMaintenance()
        {method} - void SetRandomBuildingOnFire()
        {method} - void HandleBurningBuildings()
        {method} - void DeleteDissatisfiedInhabitants()
        {method} - void HandlePopulationAgingAndEducation()
        .. Events ..
        + EventHandler? MapHasChanged
        + EventHandler? BalanceHasChanged
    }

    class FactoryLayer
    {
        .. Fields ..
        - GameData _dataLayer
        .. Constructos ..
        + FactoryLayer(GameData dataLayer)
        .. Methods ..
        {method} + bool ArrandeZone(int[,] cellMatrix)
        {method} + void BuildStadium(Point location, Orientation orientation)
        {method} + void DestroyStadium(Stadium stadium)
        {method} + void BuildFireStation(Point location, Orientation orientation)
        {method} + void DestroyFireStation(FireStation fireStation)
        {method} + void BuildForest(Point location, Orientation orientation)
        {method} + void DestroyForest(Forest forest)
        {method} + void BuildPoliceStation(Point location, Orientation orientation)
        {method} + void DestroyPoliceStation(PoliceStation policeStation)
        {method} + void BuildPowerLine(Point location, Orientation orientation)
        {method} + void DestroyPowerLine(PowerLine powerLine)
        {method} + void BuildPowerPlant(Point location, Orientation orientation)
        {method} + void DestroyPowerPlant(Powerplant powerPlant)
        {method} + void BuildRoad(Point location, Orientation orientation)
        {method} + void DestroyRoad(Road road)
        {method} + void BuildSecondarySchool(Point location, Orientation orientation)
        {method} + void DestroySecondarySchool(SecondarySchool secondarySchool)
        {method} + void UpgradeZone(Zone zone)
        {method} + void BuildRandomBuilding(Zone zone)
        .. Events ..
        + EventHandler<TransferEventArgs>? TransferEvent
        + EventHandler? MapLayoutHasChanged
    }
}

package Model.Building
{
    interface ICanBurn
    {
        .. Property ..
        {method} + bool IsOnFire {get; set;}
        {method} + DateTime? IsOnFireSince {get; set;}
        .. Method ..
        {method} + void SpreadToNeighboringBuildings()
        {method} + void Dispatch()
    }

    interface INeedPower
    {
        {method} + int PowerNeeded {get;}
        {method} + bool HasElectricity {get;}
    }

    abstract class Building {
        .. Fields ..
        - Point _location;
        - Orientation _orientation;
        - int _sizeX;
        - int _sizeY;
        .. Properties ..
        {method} + Point Location {get; set}
        {method} + Orientation Orientation {get: set}
        {method} + int SizeX {get;}
        {method} + int SizeY {get;}
        .. Constructors ..
        {method} ~Building(Point location, Orientation orientation, int sizex, int sizey)
    }

    abstract class ZoneBuilding extends Building implements INeedPower, ICanBurn{
        .. Fields ..
        - Zone _zone;
        - int _level;
        - int _capacity;
        - Variation _variation;
        - List<Inhabitant> _relatedInhabitants;
        .. Properties ..
        {method} + int ActivePoliceStations {get;}
        {method} + int ActiveStadiums {get;}
        {method} + bool HasFireStationNearby {get;}
        {method} + Zone Zone {get;}
        {method} + int Level {get; set;}
        {method} + int Capacity {get; set;}
        {method} + Variation Variation {get;}
        {method} + List<Inhabitant> RelatedInhabitants {get;}
        {method} + int PowerNeeded {get;}
        {method} + bool HasElectricity {get;}
        {method} + bool IsOnFire {get; set;}
        {method} + DateTime? IsOnFireSince {get; set;}
        .. Constructors ..
        {method} ~ ZoneBuilding(Point location, Orientation orientation, Zone zone, Variation variation)
        .. Methods ..
        {method} - void CheckSurroundingActiveSpecialBuildings()
        {method} + void SpreadToNeighboringBuildings()
        {method} + void Dispatch()
        .. Events ..
        + EventHandler? BurnedToTheGround
    }

    class Road extends Building {
        .. Constructors ..
        {method} + Road(Point location, Orientation orientation)
    }

    class Forest extends Building {
        .. Fields ..
        - DateTime _plantDate;
        .. Properties ..
        {method} + DateTime PlantDate
        .. Constructors ..
        {method} + Forest(Point location,DateTime plantDate)
        .. Methods ..
        {method} - void NotifySurroundingBuilding()
    }

    class PowerLine extends Building {
        .. Constructor ..
        {method} + PowerLine(Point location, Orientation orientation, int sizex, int sizey)
    }

    class PowerPlant extends Building implements ICanBurn { 
        .. Properties ..
        {method} + bool IsOnFire {get;set;}
        {method} + DateTime? IsOnFireSince {get;set;}
        .. Constructors ..
        {method} - Powerplant(Point location, Orientation orientation, int capacity)
        .. Methods ..
        {method} + void SpreadToNeighbouringBuildings()
        {method} + void Dispatch();
    }

    class Home extends ZoneBuilding {
        .. Properties ..
        {method} + bool HasIndustrialBuildingNearby {get;}
        {method} + bool HasForestNearby
        .. Constructors ..
        {method} + Home(Point location, Orientation orientation, Zone zone, HomeVariationKey variation)
    }

    abstract class Workplace extends ZoneBuilding {
        .. Constructors ..
        {method} ~ Workplace(Point location, Orientation orientation, Zone zone, Variation variation)
    }

    class IndustrialWorkplace extends Workplace {
        .. Constructors ..
        {method} + IndustrialWorkplace(Point location, Orientation, orientation, Zone zone, IndustrialWorkplaceVariationKey variation)
    }

    class ServiceWorkplace extends Workplace {
        .. Constructors ..
        {method} + ServiceWorkplace(Point location, Orientation, orientation, Zone zone, ServiceWorkplaceVariationKey variation)
    }

    class SpecialBuilding extends Building {
        .. Properties .. 
        {method} + int IsActive {get;}
        .. Constructors ..
        {method} + SpecialBuilding(Point location, Orientation orientation, int sizex, int sizey)
        .. Methods ..
        {abstract}{method} + void NotifySurroundingBuildings() 
    }

    class FireStation extends SpecialBuilding implements INeedPower {
        .. Fields ..
        - bool _isDispatched;
        - Building? _dispatchedTo;
        - Point _dispatchedTeamLocation;
        .. Properties ..
        {method} + bool HasElectricity {get;}
        {method} + int PowerNeedes {get;}
        .. Constructors ..
        {method} + FireStation(Point location, Orientation orientation)
        .. Methods ..
        {methods} - void MoveUnitsToDestination()
        {methods} - void PutOut()
        {methods} - void MoveBack()
        {method} + void NotifySurroundingBuildings() { override } 
        .. Events ..
        + EventHandler? FireIsPutOut;
        + EventHandler? ArrivedToDesitnation;
        + EventHandler? MovedToNewCell;
        
    }

    class PoliceStation extends SpecialBuilding implements INeedPower, ICanBurn {
        .. Properties ..
        {method} + int PowerNeeded {get;}
        {method} + bool HasElectricity {get;}
        {method} + bool IsOnFire {get; set;}
        {method} + DateTime? IsOnFireSince {get; set;}
        .. Constructors ..
        {method} + PoliceStation(Point location, Orientation orientation)
        .. Methods ..
        {method} + SpreadtoNeighBouringBuildings()
        {methods} + Dispatch()
        {method} + void NotifySurroundingBuildings() { override } 
    }

    class SecondarySchool extends SpecialBuilding implements INeedPower, ICanBurn {
        .. Properties ..
        {method} + int PowerNeeded {get;}
        {method} + bool HasElectricity {get;}
        {method} + bool IsOnFire {get; set;}
        {method} + DateTime? IsOnFireSince {get; set;}
        .. Constructors ..
        {method} + SecondarySchool(Point location, Orientation orientation)
        .. Methods ..
        {method} + SpreadtoNeighBouringBuildings()
        {methods} + Dispatch()
        {method} + void NotifySurroundingBuildings() { override } 
    }

    class Stadium extends SpecialBuilding implements INeedPower, ICanBurn {
        .. Properties ..
        {method} + int PowerNeeded {get;}
        {method} + bool HasElectricity {get;}
        {method} + bool IsOnFire {get; set;}
        {method} + DateTime? IsOnFireSince {get; set;}
        .. Constructors ..
        {method} + Stadium(Point location, Orientation orientation)
        .. Methods ..
        {method} + SpreadtoNeighBouringBuildings()
        {methods} + Dispatch()
        {method} + void NotifySurroundingBuildings() { override } 
    }

    class University extends SpecialBuilding implements INeedPower, ICanBurn {
        .. Properties ..
        {method} + int PowerNeeded {get;}
        {method} + bool HasElectricity {get;}
        {method} + bool IsOnFire {get; set;}
        {method} + DateTime? IsOnFireSince {get; set;}
        .. Constructors ..
        {method} + University(Point location, Orientation orientation)
        .. Methods ..
        {method} + SpreadtoNeighBouringBuildings()
        {methods} + Dispatch()
        {method} + void NotifySurroundingBuildings() { override } 
    }
    enum Orientation
    {
        North,
        East,
        South,
        West
    }
}
package Model.GlobalEnums
{
    enum ExpenseType
    {
        ResidentialAreaIsCreated,
        ServiceAreaIsCreated,
        IndustrialAreaIsCreated,
        ZoneTypeSetBackToDefault,
        StadiumIsBuilt,
        StadiumMaintenance,
        StadiumIsDestroyed,
        PoliceStationIsBuilt,
        PoliceStationMaintenance,
        PoliceStationIsDestroyed,
        FireStationIsBuilt,
        FireStationMaintenance,
        FireStationIsDestroyed,
        RoadIsBuilt,
        RoadMaintenance,
        RoadIsDestroyed,
        SchoolIsBuilt,
        SchoolMaintenance,
        SchoolIsDestroyed,
        UniversityIsBuilt,
        UniversityMaintenance,
        UniversityIsDestroyed,
        ServiceUpgradedToLevel2,
        ServiceUpgradedToLevel3,
        IndustrialUpgradedToLevel2,
        IndustrialUpgradedToLevel3,
        ResidentialUpgradedToLevel2,
        ResidentialUpgradedToLevel3,
        Compensation,
        ForestIsCreated,
        ForestMaintenance,
        ForestIsDestroyed,
        PowerplantIsBuilt,
        PowerplantMaintenance,
        PowerplantIsDestroyed,
        PowerLineIsBuilt,
        PowerlineMaintenance,
        PowerLineIsDestroyed
    }

    enum HomeVariationKey
    {
        Dummy1,
        Dummy2, 
        Dummy3,
        Dummy4,
        Dummy5
    }

    enum IndustrialWorkplaceVariationKey
    {
        Dummy1,
        Dummy2,
        Dummy3,
        Dummy4,
        Dummy5
    }
    enum ServiceWorkplaceVariationKey
    {
        Dummy1,
        Dummy2,
        Dummy3,
        Dummy4,
        Dummy5
    }
    
    enum WorkplaceVariationKey
    {
        Dummy1,
        Dummy2,
        Dummy3,
        Dummy4,
        Dummy5
    }
}
package Persistence
{
    interface IDataPersistence
    {
        .. Methods ..
        + SaveGameData LoadData(FileDataHandler dataHandler)
        + void SaveData(FileDataHandler dataHandler, SaveGameData save)
    }
    class SaveGameData
    {
        .. Fields ..
        + int _mapWidth
        + int _mapHeight
        + bool _isPaused
        + int _gameSpeed
        + int _balance
        + int _secondaryEducationRatio
        + int _higherEducationRatio
        + DateTime _gameTime
        + List<Stadium> _stadiums
        + List<PoliceStation> _policeStations
        + List<FireStation> _fireStations
        + List<Road> _roads
        + List<Forest> _forests
        + List<Inhabitant> _inhabitants
        + List<SecondarySchool> _secondarySchools
        + List<University> _universities
        + List<Zone> _zones
        + List<ZoneBuilding> _zoneBuildings
        + List<PowerLine> _powerLines
        + MapCell[,] _cellData
        .. Constructors ..
        SaveGameData(GameData data)
    }

    class DataPersistenceManager implements IDataPersistence
    {
        .. Fields ..
        - FileDataHandler _fileDataHandler
        .. Constructors ..
        + DataPersistenceManager(FileDataHandler fileDataHandler)
        .. Methods ..
        + SaveGameData LoadData(FileDataHandler dataHandler)
        + void SaveData(FileDataHandler dataHandler, SaveGameData save)
    }

    class FileDataHandler
    {
        .. Fields ..
        - string _directoryPath
        - string _fileName
        .. Constructor ..
        + FileDataHandler(string directoryPath, string fileName)
        .. Methods ..
        + SaveGameDate Load()
        + void Save(SaveGameData data)
    }
}

GameData o-- "*" Stadium
GameData o-- "*" PowerPlant
GameData o-- "*" PoliceStation 
GameData o-- "*" SecondarySchool
GameData o-- "*" University
GameData o-- "*" FireStation
GameData o-- "*" Forest
GameData o-- "*" Road
GameData o-- "*" PowerLine
GameData o-- "*" MapCell
GameData o-- "*" Inhabitant
GameData o-- "*" Zone
GameData o-- "*" ZoneBuilding
GameData "[HomeVariationKey]" o-- "1" Variation
GameData "[IndustrialWorkplaceVariationKey]" o-- "1" Variation
GameData "[CommercialWorkplaceVariationKey]" o-- "1" Variation
GameData o-- "1" FactoryLayer
GameData --- DateResponse
FactoryLayer o-- "1" GameData
FactoryLayer --- Stadium
FactoryLayer --- PowerPlant
FactoryLayer --- PoliceStation
FactoryLayer --- SecondarySchool
FactoryLayer --- University
FactoryLayer --- FireStation
FactoryLayer --- Forest
FactoryLayer --- Road
FactoryLayer --- PowerLine
FactoryLayer --- MapCell
FactoryLayer --- Inhabitant
FactoryLayer --- Zone
FactoryLayer --- ZoneBuilding
GameData --- TransferEventArgs
FactoryLayer --- TransferEventArgs
Inhabitant o-- Home
TransferEventArgs o-- "1" ExpenseType
MapCell o-- Zone
MapCell o-- Building
Zone o-- "1" ZoneType 
GameData --- ExpenseType
FileDataHandler <-- SaveGameData
DataPersistenceManager O-- "1" FileDataHandler
DataPersistenceManager <-- SaveGameData
```

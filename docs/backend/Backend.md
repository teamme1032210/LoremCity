# Simcity Backend
Ez a doksi a játék üzleti logikáját hivatott részleteiben tárgyalni. Az ehhez tartozó uml diagrammokat a [[Backend classes]] dokumentumban lehet megtekinteni.

# Fontos osztályok

## GameData (a backend "fő osztálya", ez tárolja a globális adatokat, mint pl az időt, a játékos pénzét, a játék sebességét, példányokat az épületekből, emberekből, etc..)
### Adarttagok:
- MapWidth (játékterület szélessége)
- MapHeight (játékterület magassága)
- Zones (zónákat tartalmazó gyűjtemény)
- ZoneBuildings (ZoneBuilding-eket, azaz a zónákhoz tartozó épülettípus példányokat tartalmazó gyűjtemény)
- FireStations (Tűzoltóságokat tartalmazó gyűjtemény)
- PoliceStations (Rendőrkapitányságokat tartalmazó gyűjtemény)
- Stadiums (Stadionokat tartalmazó gyűjtemény)
- Forests (Az erdő példányait eltároló gyűjtemény)
- Roads (Az útdarabkák példényait tároló gyűjtemény)
- Time (Játékidő)
- IsPaused (Logikai érték, amely azt jelzi, hogy meg van-e állítva a játék)
- GameSpeed (Jatékidő gyorsasága)
- Balance (Játékos egyenlege)
- ExpenseType (Az egyes költségeket tároló enumeráció)
- TotalIncome (Város összbevétele)
- TotalExpenses (Város fenntartásának összköltsége)
- TaxRate (Polgárok bevételének hány százaláka lesz adó)
- HigherEducationRatio (Felsőoktatásban résztvevők max aránya a teljes népességhez képest)
- static FirstNames (Random nevek generálásához felhasználható keresztnevek)
- static LastName (Random nevek generálásához felhasználható vezetéknevek)

- Új polgár létrejöttekor legyen fv, ami őt arányosan vagy szolgáltatási, vagy ipari zónába rakja, ennek a folyamatát TODO kigondolni
- Házak/Munkahelyek építése CSAK olyan mezőkre, ahol valamelyik szomszéd út
- Speciális épületek hatása csak akkor, ha út mellett vannak


### Metódusok:
#### Osztályszintű:
- int GetExpenseForType(ExpenseType e) : megadja, pl. egy switch-case alapján, hogy egy adott költségtípushoz milyen költség tartozik 
- string GenerateRandomName() : visszaad egy teljesen random nevet (választ egy random kereszt/vezetéknevet)
#### Példányszintű:
- void NewGame(int MapWidth, int MapHeight) : Új játék default adatokkal, megadott méretű grid-del





A lakosokat egy _Inhabitant_ osztállyal fogjuk reprezentálni, amelynek a következő adatokat kell tartalmaznia:

## Inhabitant:
- Home (x,y formában tárolja, hogy a griden belül melyik cella képviseli az adott lakos lakhelyét, kizárólag lakózóna lehet)
- Workplace (x,y formában tárolja, hogy a griden belül melyik cella képviseli az adott lakos munkahelyét, kizárólag szolgáltatási, vagy ipari zóna lehet)
- Name (Lakos neve (random generálódik majd))
- Satisfaction (polgárok elégedettségi mutatója)
- Qualification (alapfok, középfok, felsőfok)
- Age (Lakos kora, minimum 18)
- CurrentIncome (Lakos jelenlegi bevétele)
- TotalIncome (Lakos összbevétele élete során)

## Zone:
- ZoneType (special(semelyikhez sem tartozik az ezutániak közül, a leírásban "nem zóna"),industrial,service,residential)
- Cells (a zónához tartozó cellák x,y koordinátáinak gyűjteménye)
- Új zóna létrehozásakor váltódjon ki egy esemény, ami alapján a teljes budget-ből levonásra kerüljön a zóna átminősítési költsége
- Visszaminősítéskor is váltódjon ki esemény, ekkor adjuk vissza a megfelelő összeget a játékosnak

## Building (absztrakt ősosztály, minden épület alapja):
- Coordinates (elhelyezkedés, bal felső sarok)
- Orientation (merre áll)
- SizeX (Épület szélessége)
- SizeY (Épület hosszúsága)

## ZoneBuilding (a "Building" leszármazottja):
- Zone (melyik zónába tartozik)
- Level (milyen szintű)
- Capacity (hány lakó/dolgozó tartozhat hozzá)
- Variation (melyik konkrét épülettípus, megjelenítésnél lesz fontos)
- RelatedInhabitants (Kik laknak/dolgoznak egy adott buildingben, _Inhabitant_ példányokat tartalmazó gyűjtemény)

## SpecialBuilding (a "Building" leszármazottja)
- IsElectricityInUse (Áram alatt van-e)

- Építésükkor váltódjon ki esemény, amely levon egy előre megadott költséget
- Bontásukkor térüljön vissza egy előre megadott költség

## FireStation (A "SpecialBuilding" leszármazottja)

## Stadium (A "SpecialBuilding" leszármazottja)

- fv a közel lakók/dolgozók elégedettségének növelésére

## PoliceStation (A "SpecialBuilding" leszármazottja)

- fv. a közel lakók/dolgozók elégedettségének növelésére

## Forest (Jelentlegi elképzelés szerint egy 1x1-es cella/erdő)
- Coordinates (elhelyezkedés)
- PlantDate (mikor lett ültetve)

## Road (Az utak osztálya, by default minden útdarabka egy 1x1-es cellába kerül)
- Coordinates (elhelyeztkedés)
- Orientation (4 féle lehetséges irány)

- Ellenőrizni, hogy elbontható-e


## Zónák validációja
- Minimum X, maximum Y db cella/Zóna, tehát a számukat validáljuk
- Mátrix minden cellája milyen zóna

## ICanBurn (Olyan épületek interface-je, amelyek kigyulladhatnak)
- IsOnFire (Éppen ég-e)
- IsOnFireSince? (Mióta ég, ha éppen ég)

## Factory layer
- Metódusok, amiket a frontend fog hívni, a GameData osztályban példányosítja/törli ki a kívánt típust
- Minden metódusban kiváltódik egy esemény, ami a pénzt fogja majd megfelelően igazítani
- Minden metódusban eseményt fogunk küldeni a frontendnek, hogy renderelje újra a képet, mert fontos változtatás történt




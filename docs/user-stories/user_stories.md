# User Stories

Ez a fájl a [[user_stories_base]] alapján készült.

## Ui
**As a:** játékos

**I want to:** új játékot indítani
| | |
|-|-|
|**Given**|Főmenüben vagy játék közben a menüsorban vagyunk|
|**When**|"New game" gombra kattintunk|
|**Then**|Megjelenik egy új, alapállásban lévő pálya|

**As a:** játékos

**I want to:** elmenteni a játékot
| | |
|-|-|
|**Given**|Főmenüben vagy játék közben a menüsorban vagyunk|
|**When**|"Save game" gombra kattintunk, majd kiválasztjuk a mentés helyét|
|**Then**|A játék mentésre kerül|

**As a:** játékos

**I want to:** elmentett játékot betölteni
| | |
|-|-|
|**Given**|Főmenüben vagy játék közben a menüsorban vagyunk|
|**When**|"Load game" gombra kattintunk, majd kiválasztjuk a betöltendő játékot|
|**Then**|A mentett játék betöltődik|

**As a:** játékos

**I want to:** kilépni az alkalmazásból
| | |
|-|-|
|**Given**|Főmenüben vagy játék közben a menüsorban vagyunk|
|**When**|Az "Exit" gombra kattintunk|
|**Then**|A játékablak bezárul|

**As a:** játékos

**I want to:** belépni a beállítások menübe
| | |
|-|-|
|**Given**|Főmenüben vagyunk|
|**When**|A "Settings" gombra kattintunk|
|**Then**|A beállítások menü megjelenik|

**As a:** játékos

**I want to:** kitörölni egy objektumot
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|A törlés gombra kattintunk|
|**Then**|A kiválasztott objektum törlődik|

**As a:** játékos

**I want to:** zónát lerakni/törölni
| | |
|-|-|
|**Given**|Zóna kezelő módban vagyunk|
|**When**|Kiválasztjuk a lerakni/törölni kívánt zónákat|
|**Then**|Az adott zónák létrejönnek/kitörlődnek|

**As a:** játékos

**I want to:** speciális objektumot létrehozni
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Kiválasztjuk a palettáról a speciális objektumot és a helyét|
|**Then**|Az objektum létrejön|

## Idő

**As a:** játékos

**I want to:** idő sebességének változtatása
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Kiválasztjuk az idő sebességének változását|
|**Then**|Az idő és a játék sebessége megváltozik|

**As a:** játékos

**I want to:** megállítani a játékot
| | |
|-|-|
|**Given**|Az idő telik|
|**When**|Megállítjuk az időt|
|**Then**|A játék megáll|

## Pénz

**As a:** játékos

**I want to:** megnyitni a pénzről szóló menüt
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Rákattintunk a pénz menüre|
|**Then**|A pénz menü megjelenik|

**As a:** játékos

**I want to:** beállítani az adókat
| | |
|-|-|
|**Given**|A pénz menü van megnyitva|
|**When**|Átállítjuk az adó mértékét|
|**Then**|Az újonnan beállított adót kapjuk|

## Zónák

**As a:** játékos

**I want to:** zóna kezelő módba lépni
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Zónázó-mód gombra kattintunk|
|**Then**|A zóna kezelő módba kerülünk|

**As a:** játékos

**I want to:** zóna kezelő módból kilépni
| | |
|-|-|
|**Given**|Zóna kezelő módban vagyunk|
|**When**|A kilépés gombra kattintunk|
|**Then**|A változtatások helyességének ellenőrzése után kilépünk a zóna kezelő módból|

**As a:** játékos

**I want to:** épület adatait megtekinteni
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Egy zónában lévő épületre kattintunk|
|**Then**|Az épület adatai megjelennek|

**As a:** játékos

**I want to:** épület fejleszteni
| | |
|-|-|
|**Given**|Épület adatai menüben vagyunk|
|**When**|"Upgrade" gombra kattintunk|
|**Then**|Az épület fejlődik|

## Lakosok

**As a:** játékos

**I want to:** megtekinteni a lakossági adatokat
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Lakossági adatok gombra kattintunk|
|**Then**|Megjelennek a lakossági adatok|

**As a:** játékos

**I want to:** megtekinteni egy lakos adatait
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Kiválasztunk egy lakost|
|**Then**|Megjelennek az adatai|

## Oktatás

**As a:** játékos

**I want to:** megtekinteni az oktatási adatokat
| | |
|-|-|
|**Given**|Játékban vagyunk|
|**When**|Oktatási adatok gombra kattintunk|
|**Then**|Megjelennek az oktatási adatok|

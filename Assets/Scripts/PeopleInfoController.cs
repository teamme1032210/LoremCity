using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Backend.Inhabitants;

public class PeopleInfoController : MonoBehaviour
{
    public TMP_Text Name;
    public TMP_Text Age;
    public TMP_Text Sat;
    public TMP_Text Edu;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetInfo(string name, int age, double sat, Qualification q)
    {
        Name.text = name;
        Age.text = age.ToString();
        Sat.text = Mathf.Round(((float)sat)*100) + "%";
        if (q == Qualification.SecondaryEducation)
        {
            Edu.text = "Sec.";
        }
        else if (q == Qualification.PrimaryEducation)
        {
            Edu.text = "Prim.";
        }
        else
        {
            Edu.text = "High.";
        }
    }

}

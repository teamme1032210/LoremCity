using System;
using Backend.Buildings;
using Backend.Buildings.Interfaces;
using Backend.Utilities;
using Backend.Zones;
using System.Collections.Generic;
using UnityEngine;
using Util;

public class ElectricityWarningController : MonoBehaviour, IRenderable
{
    public GameObject electricityWarningPrefab = null!;

    private List<List<ElectricityWarningGridElementController>> electricityWarnings;

    private void Awake() {
        if (electricityWarningPrefab == null
        ) {
            throw new System.Exception("ElectricityWarningController not set up properly");
        }
    }

    public void ClearState()
    {
        electricityWarnings?.ForEach(a => a.ForEach(x => Destroy(x.gameObject)));
        electricityWarnings?.ForEach(a => a.Clear());
        electricityWarnings?.Clear();
    }

    public void GenerateState()
    {
        int w = RootController.Instance.GameData.MapWidth;
        int h = RootController.Instance.GameData.MapHeight;
        electricityWarnings = new List<List<ElectricityWarningGridElementController>>(w);
        for (int i = 0; i < w; i++)
        {
            List<ElectricityWarningGridElementController> gridElementRow = new List<ElectricityWarningGridElementController>(h);
            for (int j = 0; j < h; j++)
            {
                // Create instance and set position
                GameObject gridElement = Instantiate(electricityWarningPrefab, transform);
                gridElement.transform.position = new Vector3(i, j, 0);

                // Setup Controller script
                ElectricityWarningGridElementController gridElementController = gridElement.GetComponent<ElectricityWarningGridElementController>();
                gridElementController.SetGridPosition(i, j);
                gridElementController.spriteRenderer.color = new Color(223, 23, 23, 0);
                gridElementRow.Add(gridElementController);
            }
            electricityWarnings.Add(gridElementRow);
        }
        UpdateState();
    }

    public void UpdateState()
    {
        ResetGrid();
        int w = RootController.Instance.GameData.MapWidth;
        int h = RootController.Instance.GameData.MapHeight;
        WHMat<MapCell> cellData = RootController.Instance.GameData?.CellData ?? new HWMat<MapCell>(0, 0);
        for (int i = 0; i < w; i++)
        {
            for (int j = 0; j < h; j++)
            {
                Building? building =  cellData[i, j].Building;
                if (building != null && building is SpecialBuilding)
                {
                    SpecialBuilding sbuilding  = (SpecialBuilding)building;
                    if (PowerHelper.HasEnoughPower((INeedPower)sbuilding)) // aram alatt
                    {
                        for (int k = sbuilding.Location.X; k < sbuilding.Location.X + sbuilding.SizeX; k++)
                        {
                            for (int l = sbuilding.Location.Y; l < sbuilding.Location.Y + sbuilding.SizeY; l++)
                            {
                                electricityWarnings[k][l].spriteRenderer.color = new Color(223, 23, 23, 0);
                            }
                        }
                    }
                    else // nincs aram alatt
                    {
                        for (int k = sbuilding.Location.X; k < sbuilding.Location.X + sbuilding.SizeX; k++)
                        {
                            for (int l = sbuilding.Location.Y; l < sbuilding.Location.Y + sbuilding.SizeY; l++)
                            {
                                electricityWarnings[k][l].spriteRenderer.color = new Color(223, 23, 23, 100);
                            }
                        }
                    }
                }

                Zone? z = cellData[i, j].Zone;
                if (z != null && z.ZoneType != ZoneType.Special)
                {
                    if (PowerHelper.HasEnoughPower(z)) 
                    {
                        electricityWarnings[i][j].spriteRenderer.color = new Color(223, 23, 23, 0);
                    }
                    else 
                    {
                        electricityWarnings[i][j].spriteRenderer.color = new Color(223, 23, 23, 100);
                    }
                }

            }
        }
    }

    public void SetOpacity(bool val)
    {
        if (val)
        {
            foreach (var row in electricityWarnings)
            {
                foreach (var cell in row)
                {
                    Color oldcolor = cell.GetComponent<SpriteRenderer>().color;
                    cell.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g, oldcolor.b, 0.5f);
                }
            }
        }
        else
        {
            foreach (var row in electricityWarnings)
            {
                foreach (var cell in row)
                {
                    Color oldcolor = cell.GetComponent<SpriteRenderer>().color;
                    cell.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g, oldcolor.b, 1);
                }
            }
        }
    }

    public void ResetGrid()
    {
        for (int i = 0; i < electricityWarnings.Count; i++)
        {
            for (int j = 0; j < electricityWarnings[i].Count; j++)
            {
                electricityWarnings[i][j].spriteRenderer.color = new Color(223, 23, 23, 0);
            }
        }
    }
}

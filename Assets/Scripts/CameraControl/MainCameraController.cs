using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MainCameraController : MonoBehaviour
{
    public static MainCameraController Instance
    {
        get
        {
            if (_instance == null)
            {
                throw new System.Exception("MainCameraController not found in scene!");
            }
            return _instance;
        }
    }

    public float cameraSpeed = 2f;
    public float zoomInLimit = 2f;
    public float zoomOutLimit = 6f;

    public float panSpeedMultiplier = 1f;
    private Vector2 _panLimit;
    private Camera _camera = null!;
    private static MainCameraController _instance = null!;

    private void Awake()
    {
        if (_instance != null)
        {
            throw new System.Exception("More than one MainCameraController in scene!");
        }
        _instance = this;
        _camera = GetComponent<Camera>();
    }
    
    private float PanSpeed()
    {
        return _camera.orthographicSize * panSpeedMultiplier;
    }

    public void SetPanLimit()
    {
        float width = RootController.Instance.GameData.MapWidth;
        float height = RootController.Instance.GameData.MapHeight;
        _panLimit = new Vector2(width, height);
    }

    public void SetZoomOutLimit()
    {
        float width = RootController.Instance.GameData.MapWidth;
        float height = RootController.Instance.GameData.MapHeight;
        float max = Mathf.Max(width, height);
        zoomOutLimit = max * 0.5f;
    }

    void Update()
    {
        Vector3 pos = transform.position;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (_camera.orthographicSize > zoomInLimit)
                {
                    _camera.orthographicSize -= cameraSpeed * Time.deltaTime;
                }
            }
            else
            {
                pos.y += PanSpeed() * Time.deltaTime;
            }
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (_camera.orthographicSize < zoomOutLimit)
                {
                    _camera.orthographicSize += cameraSpeed * Time.deltaTime;
                }
            }
            else
            {
                pos.y -= PanSpeed() * Time.deltaTime;
            }
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            pos.x -= PanSpeed() * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            pos.x += PanSpeed() * Time.deltaTime;
        }

        pos.x = Mathf.Clamp(pos.x, 0, _panLimit.x);
        pos.y = Mathf.Clamp(pos.y, 0, _panLimit.y);
        transform.position = pos;
    }
}

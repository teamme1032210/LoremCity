using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveHighlightGridElementController : MonoBehaviour
{
    public Vector2 gridPosition;
    public SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetGridPosition(int x, int y)
    {
        gridPosition = new Vector2Int(x, y);
    }
}

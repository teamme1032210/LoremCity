using Backend.Buildings;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class FireTruckCanvasController : MonoBehaviour, IRenderable
{

    [FormerlySerializedAs("firetruckprefab")] public GameObject fireTruckPrefab = null!;
    [FormerlySerializedAs("firetrucks")] public List<FireTruckController> fireTrucks = null!;

    public void Awake() {
        if (
            fireTrucks == null ||
            fireTruckPrefab == null
        ) {
            throw new System.Exception("FireTruckCanvasController not set up properly");
        }
    }
    
    public void ClearState()
    {
        fireTrucks?.ForEach(x => Destroy(x.gameObject));
        fireTrucks?.Clear();
    }

    public void UpdateState()
    {
        bool hastruck = RootController.Instance.GameData.FireStations.Any(station => station.IsDispatched);
        if (hastruck)
        {
            fireTrucks?.ForEach(x => Destroy(x.gameObject));
            fireTrucks?.Clear();
            GenerateState();
            RootController.Instance.GameData.DeleteDisposableFireTrucks();
        }
        else if (fireTrucks.Count > 0)
        {
            fireTrucks?.ForEach(x => Destroy(x.gameObject));
            fireTrucks?.Clear();
        }
    }


    public void GenerateState()
    {
        fireTrucks = new List<FireTruckController>();
        List<FireStation> fireStations = RootController.Instance.GameData.FireStations;
        foreach (FireStation fireStation in fireStations)
        {
            if (fireStation.FireTruckLocation is (int x, int y))
            {
                GameObject truck = Instantiate(fireTruckPrefab, transform);
                truck.transform.position = new Vector3((float)y, (float)x);
                FireTruckController truckController = truck.GetComponent<FireTruckController>();
                truckController.SetPosition((float)y, (float)x, fireStation.FireTruckOrientation);
                fireTrucks.Add(truckController);
            }
        }
    }

}

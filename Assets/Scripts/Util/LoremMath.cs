using System.Drawing;
using UnityEngine;

public static class LoremMath
{
    public enum Ord
    {
        GT = 1,
        EQ = 0,
        LT = -1
    }

    public static Ord Compare<T>(T a, T b) where T : System.IComparable<T>
    {
        return (Ord)a.CompareTo(b);
    }

    public static Ord CompareLength(WHVec<int> a, WHVec<int> b)
    {
        return Compare(LenSq(a), LenSq(b));
    }
    
    public static Ord CompareLength(WHVec<int> a, int b)
    {
        return Compare(LenSq(a), b * b);
    }

    public static int LenSq(WHVec<int> a)
    {
        return a.X * a.X + a.Y * a.Y;
    }
    
    public static WHVec<int> Add(WHVec<int> a, WHVec<int> b)
    {
        return new WHVec<int>(a.X + b.X, a.Y + b.Y);
    }
    public static WHVec<float> Add(WHVec<float> a, WHVec<float> b)
    {
        return new WHVec<float>(a.X + b.X, a.Y + b.Y);
    }
    public static WHVec<float> Add(WHVec<int> a, WHVec<float> b)
    {
        return new WHVec<float>(a.X + b.X, a.Y + b.Y);
    }
    public static WHVec<int> Sub(WHVec<int> a, WHVec<int> b)
    {
        return new WHVec<int>(a.X - b.X, a.Y - b.Y);
    }

    public static WHVec<float> Mul(WHVec<float> a, float b)
    {
        return new WHVec<float>(a.X * b, a.Y * b);
    }
    
    public static WHVec<float> Mul(WHVec<int> a, float b)
    {
        return new WHVec<float>(a.X * b, a.Y * b);
    }

    public static Vector3 ToUVec(WHVec<int> a, float z = 0)
    {
        return new Vector3(a.X, a.Y, z);
    }
    public static Vector3 ToUVec(WHVec<float> a, float z = 0)
    {
        return new Vector3(a.X, a.Y, z);
    }

    public static HWVec<int> HW(Point point)
    {
        return new HWVec<int>(point.X, point.Y);
    }
    
    public static WHVec<int> WH(Point point)
    {
        return new WHVec<int>(point.X, point.Y);
    }
    public static WHVec<int> WH(Vector2Int point)
    {
        return new WHVec<int>(point.x, point.y);
    }

}
using System;

[Serializable]
internal class Mat<T>
{
    private T[,] _innerData;
    
    public Mat(int width, int height)
    {
        _innerData = new T[width, height];
    }
    
    public int Width
    {
        get => _innerData.GetLength(0);
    }
    
    public int Height
    {
        get => _innerData.GetLength(1);
    }
    
    public T this[int x, int y]
    {
        get => _innerData[x, y];
        set => _innerData[x, y] = value;
    }
}

[Serializable]
public class WHMat<T>
{
    internal Mat<T> _innerMat;
    
    public int Width
    {
        get => _innerMat.Width;
    }
    
    public int Height
    {
        get => _innerMat.Height;
    }
    public T this[int x, int y]
    {
        get => _innerMat[x, y];
        set => _innerMat[x, y] = value;
    }
    
    public T this[WHVec<int> vec]
    {
        get => _innerMat[vec.X, vec.Y];
        set => _innerMat[vec.X, vec.Y] = value;
    }
    
    public WHMat(int width, int height)
    {
        _innerMat = new Mat<T>(width, height);
    }
    private WHMat(Mat<T> innerMat)
    {
        _innerMat = innerMat;
    }

    public static implicit operator WHMat<T>(HWMat<T> hw)
    {
        return new WHMat<T>(hw._innerMat);
    }
}

[Serializable]
public class HWMat<T>
{
    internal Mat<T> _innerMat;
    
    public int Width
    {
        get => _innerMat.Width;
    }
    
    public int Height
    {
        get => _innerMat.Height;
    }
    
    public T this[int y, int x]
    {
        get => _innerMat[x, y];
        set => _innerMat[x, y] = value;
    }

    public T this[HWVec<int> vec]
    {
        get => _innerMat[vec.X, vec.Y];
        set => _innerMat[vec.X, vec.Y] = value;
    }

    public HWMat(int height, int width)
    {
        _innerMat = new Mat<T>(width, height);
    }
    
    private HWMat(Mat<T> innerMat)
    {
        _innerMat = innerMat;
    }

    public static implicit operator HWMat<T>(WHMat<T> wh)
    {
        return new HWMat<T>(wh._innerMat);
    }
}
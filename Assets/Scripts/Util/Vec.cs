internal class Vec<T>
{
    internal T x;
    internal T y;

    internal Vec(T _x, T _y)
    {
        x = _x;
        y = _y;
    }
    
}

public class HWVec<T>
{
    internal Vec<T> _inner_vec;
    public T X
    {
        get => _inner_vec.x;
        set => _inner_vec.x = value;
    }
    public T Y
    {
        get => _inner_vec.y;
        set => _inner_vec.y = value;
    }
    
    public HWVec(T y, T x)
    {
        _inner_vec = new Vec<T>(x, y);
    }
    
    private HWVec(Vec<T> inner_vec)
    {
        _inner_vec = inner_vec;
    }
    
    public static implicit operator HWVec<T>(WHVec<T> wh)
    {
        return new HWVec<T>(wh._inner_vec);
    }
}

public class WHVec<T>
{
    internal Vec<T> _inner_vec;
    public T X
    {
        get => _inner_vec.x;
        set => _inner_vec.x = value;
    }
    public T Y
    {
        get => _inner_vec.y;
        set => _inner_vec.y = value;
    }
    
    public WHVec(T x, T y)
    {
        _inner_vec = new Vec<T>(x, y);
    }

    private WHVec(Vec<T> inner_vec)
    {
        _inner_vec = inner_vec;
    }
    
    public static implicit operator WHVec<T>(HWVec<T> hw)
    {
        return new WHVec<T>(hw._inner_vec);
    }
}
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SaveGameButtonController : MonoBehaviour {
    public TextMeshProUGUI cityNameText;
    public Button loadGameButton;
    public UnityEvent loadGameEvent;
    void Awake() {
        if(cityNameText == null) {
            throw new Exception("SaveGameButtonController: cityNameText not set!");
        }
        if(loadGameButton == null) {
            throw new Exception("SaveGameButtonController: loadGameButton not set!");
        }
    }

    void Start() {
        loadGameButton.onClick.AddListener(LoadGameClicked);
    }
    
    public void UpdateCityNameText(string cityName) {
        cityNameText.text = cityName;
    }

    public void LoadGameClicked() {
        loadGameEvent?.Invoke();
    }
}
using Backend.Buildings;
using Backend.Data;
using UnityEngine;

public class UIRootController : MonoBehaviour, IDataUser
{

    public UI_IngameController UIIngame = null!;
    public UI_MainMenuController UIMainMenu = null!;
    public MapController Map = null;
    private MonoBehaviour prev_controller = null!;
    

    void Start()
    {
        RootController.Instance.DataAttach.AddListener(OnDataAttach);
        RootController.Instance.DataDetach.AddListener(OnDataDetach);
        UIIngame.HideUI();
        UIMainMenu.ShowUI();
    }

    public void OnDataAttach()
    {
        RootController.Instance.GameData.BalanceHasChanged += OnBalanceChanged;
        RootController.Instance.GameData.NewZoneBuildingWasBuilt += OnNewZoneBuildingBuilt;
        RootController.Instance.GameData.MapHasChanged += OnMapHasChanged;
        RootController.Instance.GameData.FireWasExtinguised += OnFireExtinguished;
        RootController.Instance.GameData.RoadDeletionConflictEvent += OnConflictDelete;
        RootController.Instance.GameData.ZoneBuildingDeletionConflictEvent += OnZoneBuildingDeletionConflictEvent;
        RootController.Instance.GameData.GameOverEvent += OnGameOverEvent;
    }

    public void OnDataDetach() {
        RootController.Instance.GameData.BalanceHasChanged -= OnBalanceChanged;
        RootController.Instance.GameData.NewZoneBuildingWasBuilt -= OnNewZoneBuildingBuilt;
        RootController.Instance.GameData.MapHasChanged -= OnMapHasChanged;
        RootController.Instance.GameData.FireWasExtinguised -= OnFireExtinguished;
        RootController.Instance.GameData.RoadDeletionConflictEvent -= OnConflictDelete;
        RootController.Instance.GameData.ZoneBuildingDeletionConflictEvent -= OnZoneBuildingDeletionConflictEvent;
        RootController.Instance.GameData.GameOverEvent -= OnGameOverEvent;
    }

    private void OnZoneBuildingDeletionConflictEvent(object sender, System.EventArgs e)
    {
        throw new System.NotImplementedException();
    }

    private void OnGameOverEvent(object sender, System.EventArgs e)
    {
        UIMainMenu.BackToGame = false;
        UIIngame.ShowGameOverPopUp();
    }

    private void OnConflictDelete(object sender, Building e)
    {
        UIIngame.ShowPopUp(e, e.Location.X, e.Location.Y);
    }

    private void OnFireExtinguished(object sender, System.EventArgs e)
    {
        Map.firesController.UpdateState();
    }

    private void OnMapHasChanged(object sender, System.EventArgs e)
    {
        Map.UpdateState();
    }

    private void OnNewZoneBuildingBuilt(object sender, System.EventArgs e)
    {
;        Map.zoneBuildingsController.UpdateState();
    }

    private void OnBalanceChanged(object sender, System.EventArgs e)
    {
        UIIngame.SetFunds();
    }

    public void OnStartGameEvent(int w, int h, string cityname)
    {
        UIMainMenu.HideUI();
        UIIngame.ShowUI();
        UIIngame.LoadMainContainer();

    }

    public void OnLoadGameEvent()
    {
        UIIngame.HideUI();
        UIMainMenu.ShowUI();
        UIMainMenu.LoadGameClicked();
    }

    public void OnNewGameEvent()
    {
        UIIngame.HideUI();
        UIMainMenu.ShowUI();
        UIMainMenu.NewGameClicked();
    }

    public void OnBackToGameEvent()
    {
        UIMainMenu.HideUI();
        UIIngame.ShowUI();
    }

    public void OnGameOverEvent()
    {
        UIIngame.HideUI();
        UIMainMenu.ShowUI();
    }
}

public enum EditorModeEnum
{
    Default,
    Tree,
    Road,
    Delete,
    Stadium,
    FireStation,
    PoliceStation,
    Powerplant,
    Powerline,
    University,
    SecondarySchool,
}
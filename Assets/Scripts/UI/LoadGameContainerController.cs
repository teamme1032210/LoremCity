using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoadGameContainerController : MonoBehaviour {
    
    public SaveGameButtonController[] saveGameButtons = Array.Empty<SaveGameButtonController>();
    
    public UnityEvent loadGameEvent1;
    public UnityEvent loadGameEvent2;
    public UnityEvent loadGameEvent3;

    void Awake() {
        if (saveGameButtons.Length != DataPersistenceManager.MAX_SAVE_COUNT) {
            throw new Exception(
                "LoadGameContainerController: saveGameButtons length must be equal to DataPersistenceManager.MAX_SAVE_COUNT");
        }
    }
    void Start() {
        saveGameButtons[0].loadGameEvent.AddListener(() => loadGameEvent1?.Invoke());
        saveGameButtons[1].loadGameEvent.AddListener(() => loadGameEvent2?.Invoke());
        saveGameButtons[2].loadGameEvent.AddListener(() => loadGameEvent3?.Invoke());
    }

    public void UpdateSaveGameTexts(DataPersistenceManager.SaveDescription[] saveDescriptions) {
        for (int i = 0; i < saveGameButtons.Length; i++) {
            saveGameButtons[i].UpdateCityNameText(saveDescriptions[i]?.name ?? "Empty");
        }
    }
}
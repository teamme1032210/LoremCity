using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(RectTransform))]
public class ScaleToFillParent : MonoBehaviour
{
    public RectTransform parentRect = null!;

    private Image image = null!;
    private RectTransform rectTransform = null!;

    void Start()
    {
        image = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
    }

    void ScaleToParent()
    {
        var iw = image.sprite.rect.width;
        var ih = image.sprite.rect.height;
        var pw = parentRect.rect.width;
        var ph = parentRect.rect.height;

        var sw = ph * (iw / ih);
        var sh = pw * (ih / iw);

        var w = sh <= ph ? sw : sh * (iw / ih);
        var h = sw <= pw ? sh : sw * (ih / iw);

        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
    }

    // Update is called once per frame
    void Update()
    {
        if (parentRect != null)
        {
            ScaleToParent();
        }
    }
}

using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_MainMenuController : MonoBehaviour
{
    public UnityEvent<int,int,string> StartGameEvent = null!;
    public UnityEvent BackToGameEvent = null!;
    public bool BackToGame = false;

    public LoadGameContainerController loadGameContainerController;

    public GameObject MenuContainer = null!;
    public GameObject NewGameContainer = null!;
    public GameObject LoadGameContainer = null!;
    public GameObject SettingsContainer = null!;
    public GameObject BgBg = null!;
    public GameObject BgBgPanel = null!;
    private GameObject _inContainer = null!;

    public GameObject GameWidthField = null!;
    public GameObject GameHeightField = null!;
    public GameObject GameNameField = null!;
    public GameObject ActualVolumeLabel = null!;

    public int w;
    public int h;
    public string cityname;
    public Slider slider;

    private void Awake() {
        if(loadGameContainerController == null) {
            throw new Exception("UI_MainMenuController: loadGameContainerController not set!");
        }
    }

    void Start()
    {
        w = 15;
        h = 11;
        cityname = "Loremcity";
        
        DataPersistenceManager.SaveDescription[] saveDescriptions =
            DataPersistenceManager.GetSaveDescriptions(Application.persistentDataPath);
        loadGameContainerController.UpdateSaveGameTexts(saveDescriptions);
        loadGameContainerController.loadGameEvent1.AddListener(() => LoadButtonClicked(1));
        loadGameContainerController.loadGameEvent2.AddListener(() => LoadButtonClicked(2));
        loadGameContainerController.loadGameEvent3.AddListener(() => LoadButtonClicked(3));
        UI_IngameController.Instance.SaveGameEvent.AddListener(SaveGame);
    }

    private void SaveGame() {
        DataPersistenceManager.SaveDescription[] saveDescriptions =
            DataPersistenceManager.GetSaveDescriptions(Application.persistentDataPath);
        int slot = -1;
        for (int i = 0; i < saveDescriptions.Length; i++) {
            if (saveDescriptions[i]?.name.Equals(cityname) ?? false) {
                slot = i;
                break;
            }
        }
        if (slot == -1) {
            for (int i = 0; i < saveDescriptions.Length; i++) {
                if (saveDescriptions[i] == null) {
                    slot = i;
                    break;
                }
            }
        }

        if (slot == -1) {
            long min = long.MaxValue;
            for (int i = 0; i < saveDescriptions.Length; i++) {
                if (saveDescriptions[i].time < min) {
                    min = saveDescriptions[i].time;
                    slot = i;
                }
            }
        }
        
        DataPersistenceManager.SaveGame(slot + 1, Application.persistentDataPath, RootController.Instance.GameData);
        saveDescriptions =
            DataPersistenceManager.GetSaveDescriptions(Application.persistentDataPath);
        loadGameContainerController.UpdateSaveGameTexts(saveDescriptions);
    }

    public void HideUI()
    {
        BgBg.SetActive(false);
        BgBgPanel.SetActive(false);
        MenuContainer.SetActive(false);
        NewGameContainer.SetActive(false);
        SettingsContainer.SetActive(false);
        LoadGameContainer.SetActive(false);
    }

    public void ShowUI()
    {
        BgBg.SetActive(true);
        BgBgPanel.SetActive(true);
        MenuContainer.SetActive(true);
        SettingsContainer.SetActive(false);
        NewGameContainer.SetActive(false);
        LoadGameContainer.SetActive(false);
        _inContainer = MenuContainer;
    }



    public void NewGameClicked()
    {
        MenuContainer.SetActive(false);
        NewGameContainer.SetActive(true);
        _inContainer = NewGameContainer;
        GameWidthField.GetComponent<TMP_InputField>().text = w.ToString();
        GameHeightField.GetComponent<TMP_InputField>().text = h.ToString();

    }
    public void LoadGameClicked()
    {
        MenuContainer.SetActive(false);
        LoadGameContainer.SetActive(true);
        _inContainer = LoadGameContainer;
    }
    public void SettingsClicked()
    {
        MenuContainer.SetActive(false);
        SettingsContainer.SetActive(true);
        _inContainer = SettingsContainer;
        slider.value = 0.5f;
    }
    public void ExitClicked()
    {
        HideUI();
        Application.Quit();
    }

    public void StartGameClicked()
    {
        BackToGame = true;
        StartGameEvent?.Invoke(w,h,cityname);
    }

    public void BackClicked()
    {
        if (!BackToGame)
        {
            Debug.Log("back is false");
            _inContainer.SetActive(false);
            MenuContainer.SetActive(true);
            _inContainer = MenuContainer;
        }
        else
        {
            BackToGameEvent?.Invoke();
        }
    }
    
    public void LoadButtonClicked(int slot) {
        RootController.Instance.LoadGame(slot);
        BackToGame = true;
    }
    
    public void CheckWidth()
    {
        TMP_InputField inp = GameWidthField.GetComponent<TMP_InputField>();
        w = Int32.Parse(inp.text);
        if (w < 11)
        {
            w = 11;
        }
        if (w > 50) 
        {
            w = 50;
        }
        inp.text = w.ToString();
    }
    public void CheckHeight()
    {
        TMP_InputField inp = GameHeightField.GetComponent<TMP_InputField>();
        h = Int32.Parse(inp.text);
        if (h < 11)
        {
            h = 11;
        }
        if (h > 50)
        {
            h = 50;
        }
        inp.text = h.ToString();
    }

    public void SetName()
    {
        cityname = GameNameField.GetComponent<TMP_InputField>().text;
    }

    public void SetVolume()
    {
        ActualVolumeLabel.GetComponent<TMP_Text>().text = Math.Floor(slider.value * 100).ToString();
    }

    public void OnDataAttach() {
    }

    public void OnDataDetach() {
    }
}
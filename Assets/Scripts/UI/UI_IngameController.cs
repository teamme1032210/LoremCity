using System;
using Backend.Buildings;
using Backend.Utilities;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Backend.Zones;
using Backend.Buildings.Interfaces;
using Backend.Utilities.Enums;
using Color = UnityEngine.Color;
using Image = UnityEngine.UI.Image;
using Backend.Data;

public class UI_IngameController : MonoBehaviour, IDataUser
{
    public static UI_IngameController Instance { get; private set; } = null!;

    public GameObject MainContainer = null!;
    public GameObject FundsContainer = null!;
    public GameObject ZoneInfoContainer = null!;
    public GameObject SpecialBuildingContainer = null!;
    public GameObject ZoneEditorContainer = null!;
    public GameObject TopOptions = null!;
    public GameObject LeftUI = null!;
    public GameObject PopUpContainer = null!;
    public GameObject GameOverContainer = null!;
    public GameObject PeopleInfoprefab = null!;
    public ZoneSelectController zoneSelectController = null!;
    private GameObject _inContainer = null!;

    public UnityEvent LoadGameEvent = null!;
    public UnityEvent SaveGameEvent = null!;
    public UnityEvent NewGameEvent = null!;

    public UnityEvent TreeButtonClickedEvent;
    public UnityEvent RoadButtonClickedEvent;
    public UnityEvent PowerlineButtonClickedEvent;
    public UnityEvent PoliceStationClickedEvent;
    public UnityEvent FireStationButtonClickedEvent;
    public UnityEvent StadiumButtonClickedEvent;
    public UnityEvent UniversityButtonClickedEvent;
    public UnityEvent SecondarySchoolButtonClickedEvent;
    public UnityEvent PowerplantButtonClickedEvent;
    public UnityEvent ZoneEditorButtonClickedEvent;
    public UnityEvent ZoneEditorBackButtonClickedEvent;
    public UnityEvent ZoneEditorSetButtonClickedEvent;

    public UnityEvent ResidentialZoneButtonClickedEvent;
    public UnityEvent ServiceZoneButtonClickedEvent;
    public UnityEvent IndustrialZoneButtonClickedEvent;
    public UnityEvent SpecialZoneButtonClickedEvent;

    public UnityEvent DeleteButtonClickedEvent;

    public Color ButtonSelectedColor = Color.green;
    public Color ButtonDefaultColor = Color.white;

    public Image PauseTimeControlButtonImage = null!;
    public Image[] TimeControlButtonImages = new Image[] { };

    private int TaxRate;

    private Zone _selectedZone;
    private Building _selectedBuilding;

    private Building _buildingfordelete;
    private int _buildingfordeleteLocationX;
    private int _buildingfordeleteLocationY;

    public GameObject CityName = null!;
    public GameObject TreePriceLabel = null!;
    public GameObject RoadPriceLabel = null!;
    public GameObject PowerlinePriceLabel = null!;
    public GameObject StadiumPriceLabel = null!;
    public GameObject FireStationPriceLabel = null!;
    public GameObject PoliceStationPriceLabel = null!;
    public GameObject UniversityPriceLabel = null!;
    public GameObject SecondarySchoolPriceLabel = null!;
    public GameObject PowerplantPriceLabel = null!;
    public GameObject Funds = null!;
    public GameObject Date = null!;
    public GameObject GSatisfaction = null!;
    public GameObject Taxes = null!;
    public GameObject TreeCont = null!;
    public GameObject RoadCont = null;
    public GameObject PowerlineCont = null;
    public GameObject StadiumCont = null;
    public GameObject FireStationCont = null;
    public GameObject PoliceCont = null;
    public GameObject UniversityCont = null;
    public GameObject SecondarySchoolCont = null;
    public GameObject PowerplantCont = null;
    public GameObject FundsLabel = null!;
    public GameObject IncomeLabel = null!;
    public GameObject ExpensesLabel = null!;
    public GameObject ZoneLabel = null!;
    public GameObject LevelLabel = null!;
    public GameObject ZoneFundsLabel = null!;
    public GameObject UpgradeButton = null!;
    public GameObject InhabitantsScroll = null!;
    public GameObject SatisfactionLabel = null!;
    public GameObject PeopleLabel = null!;
    public GameObject SpecialBuildingLabel = null!;
    public GameObject BuildDateLabel = null!;
    public GameObject MaintenanceCostLabel = null!;
    public GameObject SendFiretruckButton = null!;


    void Awake()
    {
        if (Instance != null)
        {
            throw new Exception("UI_IngameController is a singleton!");
        }
        Instance = this;

        if (zoneSelectController == null)
        {
            throw new Exception("ZoneSelectController is not set!");
        }

        if (MainContainer == null ||
            FundsContainer == null ||
            ZoneInfoContainer == null ||
            SpecialBuildingContainer == null ||
            ZoneEditorContainer == null ||
            TopOptions == null ||
            LeftUI == null ||
            PopUpContainer == null ||
            GameOverContainer == null
           )
        {
            throw new Exception("One or more containers are uninitialized.");
        }

        if (
            PauseTimeControlButtonImage == null ||
            TimeControlButtonImages == null ||
            TimeControlButtonImages.Length != 3 ||
            TimeControlButtonImages[0] == null ||
            TimeControlButtonImages[1] == null ||
            TimeControlButtonImages[2] == null
        )
        {
            throw new Exception("One or more time control buttons are uninitialized.");
        }

        if (
            CityName == null ||
            TreePriceLabel == null ||
            RoadPriceLabel == null ||
            PowerlinePriceLabel == null ||
            StadiumPriceLabel == null ||
            FireStationPriceLabel == null ||
            PoliceStationPriceLabel == null ||
            UniversityPriceLabel == null ||
            SecondarySchoolPriceLabel == null ||
            PowerplantPriceLabel == null ||
            Funds == null ||
            Date == null ||
            GSatisfaction == null ||
            Taxes == null ||
            TreeCont == null ||
            RoadCont == null ||
            PowerlineCont == null ||
            StadiumCont == null ||
            FireStationCont == null ||
            PoliceCont == null ||
            UniversityCont == null ||
            SecondarySchoolCont == null ||
            PowerplantCont == null ||
            FundsLabel == null ||
            IncomeLabel == null ||
            ExpensesLabel == null ||
            ZoneLabel == null ||
            LevelLabel == null ||
            ZoneFundsLabel == null ||
            UpgradeButton == null ||
            InhabitantsScroll == null ||
            SatisfactionLabel == null ||
            PeopleLabel == null ||
            SpecialBuildingLabel == null ||
            BuildDateLabel == null ||
            MaintenanceCostLabel == null ||
            SendFiretruckButton == null
        ) {
            throw new Exception("References are missing.");
        }

        _inContainer = MainContainer;
    }

    void Start() {
        RootController.Instance.DataAttach.AddListener(OnDataAttach);
        RootController.Instance.DataDetach.AddListener(OnDataDetach);
    }

    public void SetZoneEditorValidationState(bool isValid, int price)
    {
        zoneSelectController.SetSetZoneButtonEnabled(isValid);
        zoneSelectController.SetPrice(price);
    }

    public void ShowUI()
    {
        TopOptions.SetActive(true);
        LeftUI.SetActive(true);
        MainContainer.SetActive(true);
        FundsContainer.SetActive(false);
        ZoneInfoContainer.SetActive(false);
        SpecialBuildingContainer.SetActive(false);
        ZoneEditorContainer.SetActive(false);
        PopUpContainer.SetActive(false);
        GameOverContainer.SetActive(false);
        UpdateGameTimeControls();
    }


    public void HideUI()
    {
        MainContainer.SetActive(false);
        TopOptions.SetActive(false);
        LeftUI.SetActive(false);
        FundsContainer.SetActive(false);
        ZoneInfoContainer.SetActive(false);
        SpecialBuildingContainer.SetActive(false);
        ZoneEditorContainer.SetActive(false);
        PopUpContainer.SetActive(false);
        GameOverContainer.SetActive(false);
    }

    public void LoadMainContainer()
    {
        SetFunds();
        SetDate();
        SetSatisfaction();
        CityName.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.CityName}";
        TreePriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.ForestIsCreated)}$";
        RoadPriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.RoadIsBuilt)}$";
        PowerlinePriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.PowerLineIsBuilt)}$";
        StadiumPriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.StadiumIsBuilt)}$";
        FireStationPriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.FireStationIsBuilt)}$";
        PoliceStationPriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.PoliceStationIsBuilt)}$";
        UniversityPriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.UniversityIsBuilt)}$";
        SecondarySchoolPriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.SchoolIsBuilt)}$";
        PowerplantPriceLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.GetExpense(Backend.GlobalEnums.ExpenseType.PowerplantIsBuilt)}$";

    }
    public void SetFunds()
    {
        int fund = RootController.Instance.GameData.Balance;
        if (Funds!= null)
        {
            Funds.GetComponent<TMP_Text>().text = $"{fund}$";
        }
    }

    public void SetDate()
    {
        DateResponse time = RootController.Instance.GameData.GetGameDate();
        if (Date != null)
        {
            TMP_Text datetext = Date.GetComponent<TMP_Text>();
            datetext.text = $"Date: {time.Year}. {time.Month}. {time.Day}.";
        }
    }

    public void SetSatisfaction()
    {
        int sat = (int)(MathF.Round((float)RootController.Instance.GameData.GlobalSatisfaction*100));
        if (GSatisfaction != null)
        {
            GSatisfaction.GetComponent<TMP_Text>().text = $"Global Satisfaction: {sat}%";
        }
    }

    public void BackButtonClicked()
    {
        _inContainer.SetActive(false);
        MainContainer.SetActive(true);
        _inContainer = MainContainer;
        ZoneEditorBackButtonClickedEvent?.Invoke();
        LoadMainContainer();
    }

    public void ZoneEditorSetButtonClicked()
    {
        _inContainer.SetActive(false);
        MainContainer.SetActive(true);
        _inContainer = MainContainer;
        ZoneEditorSetButtonClickedEvent?.Invoke();
        LoadMainContainer() ;
        MapController.Instance.activeHighlightController.UpdateState();
    }

    public void FundsButtonClicked()
    {
        _inContainer.SetActive(false);
        FundsContainer.SetActive(true);
        _inContainer = FundsContainer;
        Taxes.GetComponent<TMP_Text>().text = $"Taxes: {TaxRate}%";
        LoadFundsContainer();

    }

    public void ZoneClicked(int x, int y)
    {
        _inContainer.SetActive(false);
        ZoneInfoContainer.SetActive(true);
        _inContainer = ZoneInfoContainer;
        LoadZoneInfoContainer(x,y);
    }



    public void SpecialBuildingClicked(int x, int y)
    {
        _inContainer.SetActive(false);
        SpecialBuildingContainer.SetActive(true);
        _inContainer = SpecialBuildingContainer;
        SpecialBuildingContainerLoad(x,y); //argument possibly a coordinate
    }



    public void ZoneEditorButtonClicked()
    {
        if (!RootController.Instance.GameData.IsPaused)
        {
            RootController.Instance.GameData.IsPaused = true;
            UpdateGameTimeControls();
        }
        MapController.Instance._mode = EditorModeEnum.Default;
        RemoveHighlights();
        _inContainer.SetActive(false);
        ZoneEditorContainer.SetActive(true);
        _inContainer = ZoneEditorContainer;
        ZoneEditorButtonClickedEvent?.Invoke();
    }

    public void ShowPopUp(Building e, int x, int y)
    {
        _buildingfordelete = e;
        _buildingfordeleteLocationX = x;
        _buildingfordeleteLocationY = y;
        PopUpContainer.SetActive(true);
        if (!RootController.Instance.GameData.IsPaused)
        {
            TogglePauseClicked();
        }
    }

    public void ConfirmConflictDeleteButtonClicked()
    {
        if (_buildingfordelete is Road)
        {
            RootController.Instance.GameData.FactoryLayer.DestroyRoad((Road)_buildingfordelete, false);
            MapController.Instance.UpdateState();
        }
        else if (_buildingfordelete is ZoneBuilding)
        {
            MapController.Instance.zoneBuildingsController.HideBuilding((ZoneBuilding)_buildingfordelete);
            if (_buildingfordelete is Home)
            {
                MapController.Instance._homeZoneBuildingList.Add((Home)_buildingfordelete);
            }
            else if (_buildingfordelete is Workplace)
            {
                MapController.Instance._workZoneBuildingList.Add((Workplace)_buildingfordelete);
            }
            MapController.Instance.ZoneEditorState[_buildingfordeleteLocationX, _buildingfordeleteLocationY] = (int)ZoneType.Special;
            var (isValid, price, warnings, errors) =  RootController.Instance.GameData.FactoryLayer.ValidateZoneArrangement(MapController.Instance.ZoneEditorState);
            SetZoneEditorValidationState(isValid, price);
            MapController.Instance.zoneEditorErrorsController.UpdateState(warnings, errors);
            MapController.Instance.zoneGridController.UpdateState();
            GameData gameData = RootController.Instance.GameData;
            try
            {
                gameData.FactoryLayer.RearrangeZoneLayout(MapController.Instance.ZoneEditorState, MapController.Instance._homeZoneBuildingList, MapController.Instance._workZoneBuildingList);
                MapController.Instance.UpdateState();
                MapController.Instance._homeZoneBuildingList.Clear();
                MapController.Instance._homeZoneBuildingList.Clear();

            }
            catch (Exception){}

        }
        PopUpContainer.SetActive(false);
    }

    public void CancelConflictDeleteButtonClicked()
    {
        PopUpContainer.SetActive(false);
        if (_buildingfordelete is Road)
        {
            TogglePauseClicked();
        }
    }


    public void ShowGameOverPopUp()
    {
        if (!RootController.Instance.GameData.IsPaused)
        {
            TogglePauseClicked();
        }
        GameOverContainer.SetActive(true);
        
    }


    public void LoadGameClicked()
    {
        LoadGameEvent?.Invoke();
    }

    public void SaveGameClicked()
    {
        SaveGameEvent?.Invoke();
    }

    public void NewGameClicked()
    {
        NewGameEvent?.Invoke();
    }

    public void UpdateGameTimeControls()
    {
        PauseTimeControlButtonImage.color =
            RootController.Instance.GameData.IsPaused ? ButtonSelectedColor : ButtonDefaultColor;
            int found = 0;
            for (int i = 0; i < TimeControlButtonImages.Length; i++)
            {
                if (i == (int)RootController.Instance.GameData.GameSpeed - 1)
                {
                    TimeControlButtonImages[i].color = ButtonSelectedColor;
                    found++;
                }
                else
                {
                    TimeControlButtonImages[i].color = ButtonDefaultColor;
                }
            }

            if (found != 1)
            {
                throw new Exception("Time control buttons not found or too many found.");
            }
    }

    public void TogglePauseClicked()
    {
        RootController.Instance.GameData.IsPaused = !RootController.Instance.GameData.IsPaused;
        UpdateGameTimeControls();
    }

    public void ToggleSpeedClicked()
    {
        RootController.Instance.GameData.IsPaused = false;
        RootController.Instance.GameData.GameSpeed = GameSpeed.Normal;
        UpdateGameTimeControls();
    }
    public void ToggleSpeed2Clicked()
    {
        RootController.Instance.GameData.IsPaused = false;
        RootController.Instance.GameData.GameSpeed = GameSpeed.Fast;
        UpdateGameTimeControls();
    }
    
    public void ToggleSpeed3Clicked()
    {
        RootController.Instance.GameData.IsPaused = false;
        RootController.Instance.GameData.GameSpeed = GameSpeed.Insane;
        UpdateGameTimeControls();
    }

    private void RemoveHighlights()
    {
        TreeCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        RoadCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        PowerlineCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        StadiumCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        FireStationCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        PoliceCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        UniversityCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        SecondarySchoolCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
        PowerplantCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0, 0, 0, 0);
    }

    public void TreeButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.Tree)
        {
            TreeCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141,80);
        }
        TreeButtonClickedEvent?.Invoke();
    }

    public void RoadButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.Road)
        {
            RoadCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        RoadButtonClickedEvent?.Invoke();
    }


    public void PowerLineButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.Powerline)
        {
            PowerlineCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        PowerlineButtonClickedEvent?.Invoke();
    }

    public void PoliceStationButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.PoliceStation)
        {
            PoliceCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        PoliceStationClickedEvent?.Invoke();
    }

    public void FireStationButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.FireStation)
        {
            FireStationCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        FireStationButtonClickedEvent?.Invoke();
    }

    public void StadiumButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.Stadium)
        {
            StadiumCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        StadiumButtonClickedEvent?.Invoke();
    }

    public void UniversityButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.University)
        {
            UniversityCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        UniversityButtonClickedEvent?.Invoke();
    }
    public void SecondarySchoolButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.SecondarySchool)
        {
            SecondarySchoolCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        SecondarySchoolButtonClickedEvent?.Invoke();
    }
    public void PowerplantButtonClicked()
    {
        RemoveHighlights();
        if (MapController.Instance._mode != EditorModeEnum.Powerplant)
        {
            PowerplantCont.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(132, 236, 141, 80);
        }
        PowerplantButtonClickedEvent?.Invoke();
    }

    public void DeleteButtonClicked()
    {
        RemoveHighlights();
        DeleteButtonClickedEvent?.Invoke();
    }

    public void ResidentialZoneButtonClicked()
    {
        ResidentialZoneButtonClickedEvent?.Invoke();
    }
    
    public void ServiceZoneButtonClicked()
    {
        ServiceZoneButtonClickedEvent?.Invoke();
    }
    
    public void IndustrialZoneButtonClicked()
    {
        IndustrialZoneButtonClickedEvent?.Invoke();
    }
    
    public void SpecialZoneButtonClicked()
    {
        SpecialZoneButtonClickedEvent?.Invoke();
    }


    private void LoadFundsContainer()
    {
        int fund = RootController.Instance.GameData.Balance;
        FundsLabel.GetComponent<TMP_Text>().text = $"{fund}$";
        TaxRate = RootController.Instance.GameData.SalaryDeductionPercentage;
        Taxes.GetComponent<TMP_Text>().text = $"Taxes: {TaxRate}%";
        int expenses = RootController.Instance.GameData.GetQuarterExpenseOfCity();
        ExpensesLabel.GetComponent<TMP_Text>().text = $"Expenses: {expenses} $";
        int income = RootController.Instance.GameData.GetQuarterIncome();
        IncomeLabel.GetComponent<TMP_Text>().text = $"Income: {income}$";
    }
    public void IncrementTax()
    {
        if (TaxRate < 100)
            TaxRate++;
        Taxes.GetComponent<TMP_Text>().text = $"Taxes: {TaxRate}%";
    }

    public void DecrementTax()
    {
        if (TaxRate > 0)
            TaxRate--;
        Taxes.GetComponent<TMP_Text>().text = $"Taxes: {TaxRate.ToString()}%";
    }

    public void SetTaxesClicked()
    {
        RootController.Instance.GameData.SalaryDeductionPercentage = TaxRate;
        LoadFundsContainer();
    }


    private void LoadZoneInfoContainer(int x, int y)
    {
        Zone zone = RootController.Instance.GameData.CellData[y, x].Zone;
        _selectedZone = zone;
        ZoneLabel.GetComponent<TMP_Text>().text = $"{zone.ZoneType}";
        LevelLabel.GetComponent<TMP_Text>().text = $"Level: {zone.CurrentLevel}/3";
        ZoneFundsLabel.GetComponent<TMP_Text>().text = $"{RootController.Instance.GameData.Balance} $";
        if (zone.CurrentLevel == 3)
        {
            UpgradeButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
        }
        else
        {
            UpgradeButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
        }

        LoadInhabitants();
    }

    public void LoadInhabitants()
    {   if (_inContainer != ZoneInfoContainer)
        {
            return;
        }
        int inhabitant_count = 0;
        double satisfaction_sum = 0;
        InhabitantsScroll.GetComponent<UnityEngine.UI.ScrollRect>().verticalNormalizedPosition = 0f;
        Transform content = InhabitantsScroll.GetComponent<UnityEngine.UI.ScrollRect>().content;
        
        for (int i = 1; i < content.childCount; i++)
        {
            Destroy(content.GetChild(i).gameObject);

        }
        foreach (var point in _selectedZone.Cells)
        {
            foreach (var inhabitant in RootController.Instance.GameData.CellData[point.Y, point.X].RelatedInhabitants)
            {
                inhabitant_count++;
                satisfaction_sum += inhabitant.Satisfaction * 100;
                Transform par = InhabitantsScroll.GetComponent<UnityEngine.UI.ScrollRect>().content.transform;
                GameObject item = Instantiate(PeopleInfoprefab);
                item.GetComponent<PeopleInfoController>().SetInfo(inhabitant.Name, inhabitant.Age, inhabitant.Satisfaction, inhabitant.Qualification);
                item.transform.SetParent(par);
                item.transform.localScale = Vector3.one;
            }
        }
        PeopleLabel.GetComponent<TMP_Text>().text = $"People Count: {inhabitant_count}/{_selectedZone.Capacity}";
        SatisfactionLabel.GetComponent<TMP_Text>().text = $"Zone Satisfaction: {MathF.Round((float)satisfaction_sum / inhabitant_count)}%";
    }

    public void UpgradeZoneClicked()
    {
        RootController.Instance.GameData.FactoryLayer.UpgradeZone(_selectedZone);
        ZoneGridController.Instance.UpdateState();
        LoadZoneInfoContainer(_selectedZone.Cells[0].X, _selectedZone.Cells[0].Y);
        MapController.Instance.activeHighlightController.UpdateState();
        MapController.Instance.zoneBuildingsController.UpdateState();
    }




    public void SpecialBuildingContainerLoad(int x, int y)
    {
        Building building = RootController.Instance.GameData.CellData[y, x].Building;
        _selectedBuilding = building;
        if (building is ZoneBuilding)
        {
            if (((ZoneBuilding)building).Zone.ZoneType == ZoneType.Residential)
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = "Home Building";
            }
            if (((ZoneBuilding)building).Zone.ZoneType == ZoneType.Industrial)
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = "Factory Building";
            }
            if (((ZoneBuilding)building).Zone.ZoneType == ZoneType.Service)
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = "Service Building";
            }

            BuildDateLabel.GetComponent<TMP_Text>().text = "";
            MaintenanceCostLabel.GetComponent<TMP_Text>().text = "";
        }
        if (building is Forest)
        {
            DateResponse date = ((Forest)building).BuildDate;
            int maintenancecost = RootController.Instance.GameData.GetExpense(((Forest)building).MaintenanceExpenseType);
            BuildDateLabel.GetComponent<TMP_Text>().text = $"Build Date:{date.Year} {date.Month} {date.Day}";
            MaintenanceCostLabel.GetComponent<TMP_Text>().text = $"Maintenance Cost: -{maintenancecost}$/year%";
            SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Forest";
        }
        else if (building is Road)
        {
            DateResponse date = ((Road)building).BuildDate;
            int maintenancecost = RootController.Instance.GameData.GetExpense(((Road)building).MaintenanceExpenseType);
            BuildDateLabel.GetComponent<TMP_Text>().text = $"Build Date:{date.Year} {date.Month} {date.Day}";
            MaintenanceCostLabel.GetComponent<TMP_Text>().text = $"Maintenance Cost: -{maintenancecost}$/year%";
            SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Road";
        }
        else if (building is PowerLine)
        {
            DateResponse date = ((PowerLine)building).BuildDate;
            int maintenancecost = RootController.Instance.GameData.GetExpense(((PowerLine)building).MaintenanceExpenseType);
            BuildDateLabel.GetComponent<TMP_Text>().text = $"Build Date:{date.Year} {date.Month} {date.Day}";
            MaintenanceCostLabel.GetComponent<TMP_Text>().text = $"Maintenance Cost: -{maintenancecost}$/year%";
            SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Powerline";
        }
        else if (building is PowerPlant)
        {
            DateResponse date = ((PowerPlant)building).BuildDate;
            int maintenancecost = RootController.Instance.GameData.GetExpense(((PowerPlant)building).MaintenanceExpenseType);
            BuildDateLabel.GetComponent<TMP_Text>().text = $"Build Date:{date.Year} {date.Month} {date.Day}";
            MaintenanceCostLabel.GetComponent<TMP_Text>().text = $"Maintenance Cost: -{maintenancecost}$/year%";
            SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Powerplant";
        }
        else if (building is SpecialBuilding)
        {
            DateResponse date = ((SpecialBuilding)building).BuildDate;
            int maintenancecost = RootController.Instance.GameData.GetExpense(((SpecialBuilding)building).MaintenanceExpenseType);
            BuildDateLabel.GetComponent<TMP_Text>().text = $"Build Date:{date.Year} {date.Month} {date.Day}";
            MaintenanceCostLabel.GetComponent<TMP_Text>().text = $"Maintenance Cost: -{maintenancecost}$/year%";
            if (building is FireStation)
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Fire Station";
            }
            else if (building is PoliceStation) 
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Police Station";
            }
            else if (building is University)
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"University";
            }
            else if (building is SecondarySchool)
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Secondary School";
            }
            else if (building is Stadium)
            {
                SpecialBuildingLabel.GetComponent<TMP_Text>().text = $"Stadium";
            }
        }

        if (building is not FireStation && building is ICanBurn b && b.IsOnFire && !b.HasDispatchedTruck)
        {
            SendFiretruckButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
        }
        else
        {
            SendFiretruckButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
        }
    }

    public void SendFireTruck()
    {
        RootController.Instance.GameData.HandleFiretruck(_selectedBuilding);
        SpecialBuildingContainerLoad(_selectedBuilding.Location.X, _selectedBuilding.Location.Y);
    }

    public void OnDataAttach() {
        LoadMainContainer();
    }

    public void OnDataDetach() {
        
    }

}

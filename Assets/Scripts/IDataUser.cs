using Backend.Data;

public interface IDataUser {
    public void OnDataAttach();
    public void OnDataDetach();
}
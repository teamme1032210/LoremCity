using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityWarningGridElementController : MonoBehaviour
{
    public Vector2 gridPosition;
    public SpriteRenderer spriteRenderer;

    public void SetGridPosition(int x, int y)
    {
        gridPosition = new Vector2Int(x, y);
    }
}

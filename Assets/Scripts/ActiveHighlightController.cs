using System;
using Backend.Buildings;
using System.Collections.Generic;
using Backend.Utilities;
using UnityEngine;
using Backend.Zones;
using UnityEngine.Serialization;

public class ActiveHighlightController : MonoBehaviour, IRenderable
{
    public ActiveHighlightGridElementController activeHighlightPrefab;

    public List<List<ActiveHighlightGridElementController>> activeHighlights;

    void Awake() {
        if (activeHighlightPrefab == null)
        {
            throw new Exception("ActiveHighlightController: No active highlight prefab set!");
        }
    }

    public void ClearState()
    {
        activeHighlights?.ForEach(a => a.ForEach(x => Destroy(x.gameObject)));
        activeHighlights?.ForEach(a => a.Clear());
        activeHighlights?.Clear();
    }

    public void GenerateState()
    {
        int w = RootController.Instance.GameData.MapWidth;
        int h = RootController.Instance.GameData.MapHeight;
        activeHighlights = new List<List<ActiveHighlightGridElementController>>(w);
        for (int i = 0; i < w; i++)
        {
            List<ActiveHighlightGridElementController> gridElementRow = new List<ActiveHighlightGridElementController>(h);
            for (int j = 0; j < h; j++)
            {
                // Create instance and set position
                GameObject gridElement = Instantiate(activeHighlightPrefab.gameObject, transform);
                gridElement.transform.position = new Vector3(i, j, 0);

                // Setup Controller script
                ActiveHighlightGridElementController gridElementController = gridElement.GetComponent<ActiveHighlightGridElementController>();
                gridElementController.SetGridPosition(i, j);
                gridElementController.spriteRenderer.color = new Color(223,23,23,0);
                gridElementRow.Add(gridElementController);
            }
            activeHighlights.Add(gridElementRow);
        }
        UpdateState();
    }

    public void UpdateState()
    {
        int w = RootController.Instance.GameData.MapWidth;
        int h = RootController.Instance.GameData.MapHeight;
        WHMat<MapCell> cellData = RootController.Instance.GameData?.CellData ?? new HWMat<MapCell>(0, 0);
        for (int i = 0; i < w; i++)
        {
            for (int j = 0; j < h; j++)
            {
                Building? b = cellData[i, j].Building;
                Zone? z = cellData[i,j].Zone;
                if (b is Road r)
                {
                    if (!r.IsActive)
                    {
                        activeHighlights[r.Location.X][r.Location.Y].spriteRenderer.color = new Color(223, 23, 23, 100);
                    }
                    else
                    {
                        activeHighlights[r.Location.X][r.Location.Y].spriteRenderer.color = new Color(223, 23, 23, 0);
                    }
                }
                else if (b is SpecialBuilding s)
                {
                    if (!s.IsActive)
                    {
                        for (int k = s.Location.X; k < s.Location.X + s.SizeX ; k++)
                        {
                            for (int l = s.Location.Y; l < s.Location.Y + s.SizeY; l++)
                            {
                                activeHighlights[k][l].spriteRenderer.color = new Color(223, 23, 23, 100);
                            }
                        }
                    }
                    else
                    {
                        for (int k = s.Location.X; k < s.Location.X + s.SizeX; k++)
                        {
                            for (int l = s.Location.Y; l < s.Location.Y + s.SizeY; l++)
                            {
                                activeHighlights[k][l].spriteRenderer.color = new Color(223, 23, 23, 0);
                            }
                        }
                    }
                }
                else
                {
                    activeHighlights[i][j].spriteRenderer.color = new Color(223, 23, 23, 0);
                }

                if (z != null && z.ZoneType != ZoneType.Special)
                {
                    if (!z.IsActive)
                    {
                        activeHighlights[i][j].spriteRenderer.color = new Color(223, 23, 23, 100);
                    }
                    else
                    {
                        activeHighlights[i][j].spriteRenderer.color = new Color(223, 23, 23, 0);
                    }
                }

            }
        }
    }
}

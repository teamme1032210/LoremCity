using UnityEngine;
using System.Collections.Generic;
using Backend.Buildings;

public class TreesController : MonoBehaviour, IRenderable
{
    public GameObject treeTilePrefab = null!;


    private List<GameObject> treeTiles = new List<GameObject>();

    

    public void Awake()
    {
        if (treeTilePrefab == null)
        {
            throw new System.Exception("Tree tile prefab is not set");
        }
    }

    public void ClearState()
    {
        treeTiles?.ForEach(x => Destroy(x.gameObject));
        treeTiles?.Clear();
    }

    public void GenerateState()
    {
        List<Forest> forests = RootController.Instance.GameData.Forests;
        treeTiles = new List<GameObject>(forests.Count);
        for (int i = 0; i < forests.Count; i++)
        {
            GameObject treeTile = Instantiate(treeTilePrefab, transform);
            treeTile.transform.position = new Vector3(forests[i].Location.X, forests[i].Location.Y, 0);
            treeTiles.Add(treeTile);
        }
    }

    public void UpdateState() {
        treeTiles.ForEach(treeTile => Destroy(treeTile.gameObject));
        treeTiles.Clear();
        GenerateState();
    }


    public void SetOpacity(bool val)
    {
        if (val)
        {
            foreach (var cell in treeTiles)
            {
                Color oldcolor = cell.GetComponent<SpriteRenderer>().color;
                cell.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g ,oldcolor.b, 0.5f);
            }
        }
        else
        {
            foreach (var cell in treeTiles)
            {
                Color oldcolor = cell.GetComponent<SpriteRenderer>().color;
                cell.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g, oldcolor.b, 1);
            }
        }

    }

}

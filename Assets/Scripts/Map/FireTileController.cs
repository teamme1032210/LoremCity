using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class FireTileController : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void SetOpacity(bool opaque)
    {
        Color color = _spriteRenderer.color;
        color.a = opaque ? 1.0f : 0.8f;
        _spriteRenderer.color = color;
    }
}

using UnityEngine;
using System;
using Backend.Zones;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

[RequireComponent(typeof(SpriteRenderer))]
public class GridElementController : MonoBehaviour
{
    
    public Sprite defaultZoneSprite = null!;
    [FormerlySerializedAs("residentalZoneSprites")] public Sprite[] residentialZoneSprites = Array.Empty<Sprite>();
    public Sprite[] serviceZoneSprites = Array.Empty<Sprite>();
    public Sprite[] industrialZoneSprites = Array.Empty<Sprite>();
    
    public Vector2 noiseOffsetR = Vector2.zero;
    public Vector2 noiseOffsetG = Vector2.zero;
    public Vector2 noiseOffsetB = Vector2.zero;
    public float noiseScale = 1f;
    public float noiseAmplitude = 0.1f;
    
    public SpriteRenderer spriteRenderer = null!;
    private Vector2 _gridPosition;
    public UnityEvent<Vector2Int> CellClickedEvent;

    // This object is dynamically created
    void Awake()
    {
        if (defaultZoneSprite == null ||
            residentialZoneSprites.Length != 3 ||
            serviceZoneSprites.Length != 3 ||
            industrialZoneSprites.Length != 3
           )
        {
            throw new Exception("GridElementController: Sprites not set!");
        }
        foreach (var spritearr in new Sprite[][] {residentialZoneSprites, serviceZoneSprites, industrialZoneSprites})
        {
            foreach (var sprite in spritearr)
            {
                if (sprite == null)
                {
                    throw new Exception("GridElementController: Sprites not set!");
                }
            }
        }
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = defaultZoneSprite;
    }

    private float CalcNoise(Vector2 pos, Vector2 offset, float scale, float amplitude)
    {
        return 1.0f - (
            Mathf.PerlinNoise(
                (pos.x + offset.x) * scale,
                (pos.y + offset.y) * scale
            ) * amplitude
        );
    }

    private void UpdateNoise()
    {
        float r = CalcNoise(_gridPosition, noiseOffsetR, noiseScale, noiseAmplitude);
        float g = CalcNoise(_gridPosition, noiseOffsetG, noiseScale, noiseAmplitude);
        float b = CalcNoise(_gridPosition, noiseOffsetB, noiseScale, noiseAmplitude);
        spriteRenderer.color = new Color(r, g, b);
    }

    public void SetGridPosition(float x, float y)
    {
        _gridPosition = new Vector2(x, y);
        UpdateNoise();
    }

    public void SetZoneInfo(ZoneType zoneType, int level)
    {
        switch (zoneType)
        {
            case ZoneType.Special:
                spriteRenderer.sprite = defaultZoneSprite;
                break;
            case ZoneType.Residential:
                spriteRenderer.sprite = residentialZoneSprites[level - 1];
                break;
            case ZoneType.Service:
                spriteRenderer.sprite = serviceZoneSprites[level - 1];
                break;
            case ZoneType.Industrial:
                spriteRenderer.sprite = industrialZoneSprites[level - 1];
                break;
            default:
                throw new System.Exception("GridElementController: Unknown zone type!");
                break;
        }
    }

}

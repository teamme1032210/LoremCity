using System;
using UnityEngine;
using static LoremMath;

[RequireComponent(typeof(LineRenderer))]
public class ElectricityWireController : MonoBehaviour
{
    public float hangingMultiplier = 1.0f;
    private WHVec<float> _startPosition;
    private WHVec<float> _endPosition;
    private LineRenderer _lineRenderer;
    private const int WIRE_POINT_COUNT = 10;
    
    private static readonly WHVec<float> offset = new WHVec<float>(0, 0.2f);

    public Gradient opaqueGradient;
    public Gradient translucentGradient;

    public void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
    }

    private float WireDepth(float x)
    {
        return (float)(Math.Cosh(x - 0.5f) - Math.Cosh(0.5)) * hangingMultiplier;
    }

    private void UpdateLineRenderer()
    {
        Vector3[] positions = new Vector3[WIRE_POINT_COUNT];
        for (int i = 0; i < WIRE_POINT_COUNT; i++)
        {
            WHVec<float> wirePoint = Add(
                Add(
                    Mul(_startPosition, 1 - i / (float)(WIRE_POINT_COUNT - 1)),
                    Mul(_endPosition, i / (float)(WIRE_POINT_COUNT - 1))
                    ),
                    new WHVec<float>(0.0f, WireDepth(i / (float)(WIRE_POINT_COUNT - 1)))
                );
            positions[i] = ToUVec(wirePoint, -1);
        }
        _lineRenderer.positionCount = WIRE_POINT_COUNT;
        _lineRenderer.SetPositions(positions);
    }
    
    public void SetStartEndPositions(WHVec<int> startPosition, WHVec<int> endPosition)
    {
        _startPosition = Add(startPosition, offset);
        _endPosition = Add(endPosition, offset);
        UpdateLineRenderer();
    }

    public void SetOpacity(bool opaque)
    {
        _lineRenderer.colorGradient = opaque ? translucentGradient : opaqueGradient;
    }
}
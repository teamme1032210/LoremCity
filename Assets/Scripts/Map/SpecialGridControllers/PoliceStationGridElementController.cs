using Backend.Buildings;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceStationGridElementController : MonoBehaviour, IOpacity
{
    private Vector2Int gridPosition;

    public SpriteRenderer renderer = null!;

    public void Awake()
    {
        if (renderer == null)
        {
            throw new System.Exception("No renderer for policestation");
        }
    }

    public void SetGridPosition(int x, int y)
    {
        gridPosition = new Vector2Int(x, y);
    }

    public void SetOpacity(bool val)
    {
        if (val)
        {
            Color oldcolor = renderer.GetComponent<SpriteRenderer>().color;
            renderer.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g, oldcolor.b, 0.5f);
        }
        else
        {
            Color oldcolor = renderer.GetComponent<SpriteRenderer>().color;
            renderer.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g, oldcolor.b, 1);

        }
    }
}

using Backend.Buildings;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialGridController : MonoBehaviour, IRenderable
{
    private List<GameObject> specialbuildings; 

    public GameObject policestationprefab;
    public GameObject firestationprefab;
    public GameObject stadiumprefab;
    public GameObject powerplantprefab;
    public GameObject powerlineprefab;
    public GameObject secondaryschoolprefab;
    public GameObject universityprefab;


    void Awake()
    {
       specialbuildings = new List<GameObject>();
    }

    public void ClearState()
    {
        specialbuildings?.ForEach(x => { Destroy(x.gameObject);});
        specialbuildings?.Clear();
    }

    public void GenerateState()
    {
        List<PoliceStation> policestations = RootController.Instance.GameData.PoliceStations;
        for (int i = 0; i < policestations.Count; i++)
        {
            GameObject ins = Instantiate(policestationprefab, transform);
            ins.transform.position = new Vector3(policestations[i].Location.X, policestations[i].Location.Y, 0);
            PoliceStationGridElementController policestationscont = ins.GetComponent<PoliceStationGridElementController>();
            policestationscont.SetGridPosition(policestations[i].Location.X, policestations[i].Location.Y);
            specialbuildings.Add(ins);
        }
        
        List<FireStation> firestations = RootController.Instance.GameData.FireStations;
        for (int i = 0; i < firestations.Count; i++)
        {
            GameObject ins = Instantiate(firestationprefab, transform);
            ins.transform.position = new Vector3(firestations[i].Location.X, firestations[i].Location.Y, 0);
            FireStationGridElementController firestationcont = ins.GetComponent<FireStationGridElementController>();
            firestationcont.SetGridPosition(firestations[i].Location.X, firestations[i].Location.Y);
            specialbuildings.Add(ins);
        }

        List<Stadium> stadiums = RootController.Instance.GameData.Stadiums;
        for (int i = 0; i < stadiums.Count; i++)
        {
            GameObject ins = Instantiate(stadiumprefab, transform);
            ins.transform.position = new Vector3(stadiums[i].Location.X, stadiums[i].Location.Y, 0);
            StadiumGridElementController stadiumcont = ins.GetComponent<StadiumGridElementController>();
            stadiumcont.SetGridPosition(stadiums[i].Location.X, stadiums[i].Location.Y);
            specialbuildings.Add(ins);
        }

        List<PowerPlant> powerplants= RootController.Instance.GameData.PowerPlants;
        for (int i = 0; i < powerplants.Count; i++)
        {
            GameObject ins = Instantiate(powerplantprefab, transform);
            ins.transform.position = new Vector3(powerplants[i].Location.X, powerplants[i].Location.Y, 0);
            PowerplantGridElementController powerplantscont = ins.GetComponent<PowerplantGridElementController>();
            powerplantscont.SetGridPosition(powerplants[i].Location.X, powerplants[i].Location.Y);
            specialbuildings.Add(ins);
        }

        List<SecondarySchool> secondarys = RootController.Instance.GameData.SecondarySchools;
        for (int i = 0; i < secondarys.Count; i++)
        {
            GameObject ins = Instantiate(secondaryschoolprefab, transform);
            ins.transform.position = new Vector3(secondarys[i].Location.X, secondarys[i].Location.Y, 0);
            SecondarySchoolGridElementController secondarycont = ins.GetComponent<SecondarySchoolGridElementController>();
            secondarycont.SetGridPosition(secondarys[i].Location.X, secondarys[i].Location.Y);
            specialbuildings.Add(ins);
        }

        List<University> unis = RootController.Instance.GameData.Universities;
        for (int i = 0; i < unis.Count; i++)
        {
            GameObject ins = Instantiate(universityprefab, transform);
            ins.transform.position = new Vector3(unis[i].Location.X, unis[i].Location.Y, 0);
            UniversityGridElementController unicont = ins.GetComponent<UniversityGridElementController>();
            unicont.SetGridPosition(unis[i].Location.X, unis[i].Location.Y);
            specialbuildings.Add(ins);
        }

        List<PowerLine> plines = RootController.Instance.GameData.PowerLines;
        for (int i = 0; i < plines.Count; i++)
        {
            GameObject ins = Instantiate(powerlineprefab, transform);
            ins.transform.position = new Vector3(plines[i].Location.X, plines[i].Location.Y, 0);
            PowerlineGridElementController plinescon = ins.GetComponent<PowerlineGridElementController>();
            plinescon.SetGridPosition(plines[i].Location.X, plines[i].Location.Y);
            specialbuildings.Add(ins);
        }

    }

    public void UpdateState()
    {
        specialbuildings.ForEach(b => Destroy(b.gameObject));
        specialbuildings.Clear();
        GenerateState();
    }

    public void SetOpacity(bool val)
    {
        if (val)
        {
            foreach (var cell in specialbuildings)
            {
                cell.GetComponent<IOpacity>().SetOpacity(true);
            }
        }
        else
        {
            foreach (var cell in specialbuildings)
            {
                cell.GetComponent<IOpacity>().SetOpacity(false);
            }
        }

    }
}

using Backend.Zones;
using UnityEngine;

public class ZoneBuildingController : MonoBehaviour
{

    const int UNIT_SIZE = 32;

    
    public Texture2D buildingTexturelevel1 = null!;
    public Texture2D buildingTexturelevel2 = null!;
    public Texture2D buildingTexturelevel3 = null!;
    
    public Texture2D factoryTexturelevel1 = null!;
    public Texture2D factoryTexturelevel2 = null!;
    public Texture2D factoryTexturelevel3 = null!;


    public Texture2D serviceTexturelevel1 = null!;
    public Texture2D serviceTexturelevel2 = null!;
    public Texture2D serviceTexturelevel3 = null!;

    public static Sprite[,] componentSprites = null!;

    private GameObject[,] children = null!;
    private int width;
    private int height;

    // This is a dynamically generated object
    void Awake()
    {
    }

    public void GenerateComponentSprites(ZoneType ztype, int zlevel) {
        componentSprites = new Sprite[4,4];
        Texture2D texture = null!;
        switch (ztype)
        {
            case ZoneType.Residential:
                switch (zlevel)
                {
                    case 1:
                        texture = buildingTexturelevel1;
                        break;
                    case 2:
                        texture = buildingTexturelevel2;
                        break;
                    case 3:
                        texture = buildingTexturelevel3;
                        break;
                    default:
                        break;
                }
                break;
            case ZoneType.Service:
                switch (zlevel)
                {
                    case 1:
                        texture = serviceTexturelevel1;
                        break;
                    case 2:
                        texture = serviceTexturelevel2;
                        break;
                    case 3:
                        texture = serviceTexturelevel3;
                        break;
                    default:
                        break;
                }
                break;
            case ZoneType.Industrial:
                switch (zlevel)
                {
                    case 1:
                        texture = factoryTexturelevel1;
                        break;
                    case 2:
                        texture = factoryTexturelevel2;
                        break;
                    case 3:
                        texture = factoryTexturelevel3;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        if (texture != null) {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    componentSprites[i, j] = Sprite.Create(
                        texture,
                        new Rect(UNIT_SIZE * i, UNIT_SIZE * j, UNIT_SIZE, UNIT_SIZE),
                        new Vector2(0.5f, 0.5f),
                        32
                    );
                }
            }
        }
    }

    private void PutRenderChild(int x, int y, int i, int j) {
      children[x,y] = new GameObject("Building Component");
      children[x,y].transform.parent = transform;
      children[x,y].transform.localPosition = new Vector3(x, y, 0);
      SpriteRenderer renderer = children[x,y].AddComponent<SpriteRenderer>() as SpriteRenderer;
      renderer.sprite = componentSprites[i,j];
      renderer.sortingOrder = 3;
    }

    public void GenerateChildren(int width, int height) {
      children = new GameObject[width, height];
      if(width == 1 && height == 1) {
        PutRenderChild(0, 0, 0, 0);
      } else if(width == 1 && height > 1) {
        PutRenderChild(0, 0, 0, 1);
        PutRenderChild(0, height - 1, 0, 3);
        for(int i = 1; i < height - 1; i++) {
          PutRenderChild(0, i, 0, 2);
        }
      } else if(width > 1 && height == 1) {
        PutRenderChild(0, 0, 1, 0);
        PutRenderChild(width - 1, 0, 3, 0);
        for(int i = 1; i < width - 1; i++) {
          PutRenderChild(i, 0, 2, 0);
        }
      } else {
        PutRenderChild(0, 0, 1, 1);
        PutRenderChild(width - 1, 0, 3, 1);
        PutRenderChild(0, height - 1, 1, 3);
        PutRenderChild(width - 1, height - 1, 3, 3);
        for(int i = 1; i < width - 1; i++) {
          PutRenderChild(i, 0, 2, 1);
          PutRenderChild(i, height - 1, 2, 3);
        }
        for(int i = 1; i < height - 1; i++) {
          PutRenderChild(0, i, 1, 2);
          PutRenderChild(width - 1, i, 3, 2);
        }
        for(int i = 1; i < width - 1; i++) {
          for(int j = 1; j < height - 1; j++) {
            PutRenderChild(i, j, 2, 2);
          }
        }
      }
    }
}

public interface IRenderable {
    public void GenerateState();
    public void UpdateState();
    public void ClearState();
}
using UnityEngine;
using UnityEngine.EventSystems;
using Backend.Buildings;
using UnityEngine.Events;
using Unity.VisualScripting;
using static LoremMath;
using UnityEngine.UIElements;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class HighlightTileController : MonoBehaviour
{
    public delegate void HighlightTileClickedHandler(Vector2Int gridPosition, KeyCode? keycode);
    public event HighlightTileClickedHandler? OnHighlightTileClicked;

    public delegate void MouseHandler(WHVec<int> position);
    public event MouseHandler? OnHighlight;
    public event MouseHandler? OnHighlightLeave;

    public Color spriteColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    public Color spriteColorHover = new Color(1.0f, 1.0f, 1.0f, 0.2f);

    private SpriteRenderer _spriteRenderer = null!;
    private Vector2Int _gridPosition;

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.color = spriteColor;
    }

    public void SetGridPosition(Vector2Int gridPosition)
    {
        _gridPosition = gridPosition;
    }

    void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            OnHighlight?.Invoke(WH(_gridPosition));
        }
    }
    public void SetHoverColor()
    {
        if (_spriteRenderer == null)
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _spriteRenderer.color = spriteColorHover;
        }
        _spriteRenderer.color = spriteColorHover;
    }

    public void UnSetHoverColor()
    {
        if (_spriteRenderer == null)
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _spriteRenderer.color = spriteColor;
        }
        _spriteRenderer.color = spriteColor;
    }

    void OnMouseExit()
    {
        OnHighlightLeave?.Invoke(WH(_gridPosition));
    }

    void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                OnHighlightTileClicked?.Invoke(_gridPosition, KeyCode.LeftShift);
            }
            else
            {
                OnHighlightTileClicked?.Invoke(_gridPosition, null);
            }
        }
    }

}

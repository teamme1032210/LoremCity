using System;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class StreetTileController : MonoBehaviour
{

    // 0 - no neighbours
    // 1 - one neighbour
    // 2 - two neighbours on opposite sides
    // 3 - two neighbours on adjacent sides
    // 4 - three neighbours
    // 5 - four neighbours
    public Sprite[] streetSprites = null!;
    
    private SpriteRenderer _spriteRenderer = null!;
    
    public void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    public void SetNeighbours(byte neighbourMask)
    {
        if(streetSprites.Length != 6)
        {
            throw new System.Exception("Street Sprites array must have 4 elements");
        }
        int neighbourCount = 0;
        int firstNeighbour = -1;
        int secondNeighbour = -1;
        int firstNonNeighbour = -1;
        for (int i = 0; i < 4; i++)
        {
            if ((neighbourMask & (1 << i)) != 0)
            {
                neighbourCount++;
                if (firstNeighbour == -1)
                {
                    firstNeighbour = i;
                }
                else if(secondNeighbour == -1)
                {
                    secondNeighbour = i;
                }
            }
            else
            {
                
                if (firstNonNeighbour == -1)
                {
                    firstNonNeighbour = i;
                }
            }
        }

        switch (neighbourCount)
        {
            case 0:
                _spriteRenderer.sprite = streetSprites[0];
                break;
            case 1:
                _spriteRenderer.sprite = streetSprites[1];
                transform.localEulerAngles = new Vector3(0, 0, firstNeighbour * 90);
                break;
            case 2:
                if ((secondNeighbour - firstNeighbour) % 2 == 0)
                {
                    _spriteRenderer.sprite = streetSprites[2];
                    transform.localEulerAngles = new Vector3(0, 0, firstNeighbour * 90);
                }
                else
                {
                    _spriteRenderer.sprite = streetSprites[3];
                    if (secondNeighbour - firstNeighbour == 3)
                    {
                        transform.localEulerAngles = new Vector3(0, 0, secondNeighbour * 90);
                    }
                    else
                    {
                        transform.localEulerAngles = new Vector3(0, 0, firstNeighbour * 90);
                    }
                }
                break;
            case 3:
                _spriteRenderer.sprite = streetSprites[4];
                transform.localEulerAngles = new Vector3(0, 0, firstNonNeighbour * 90);
                break;
            case 4:
                _spriteRenderer.sprite = streetSprites[5];
                break;
            default:
                throw new Exception("How did we get here??");
        }
    }
    
}
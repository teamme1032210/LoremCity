using System;
using System.Collections.Generic;
using Backend.Buildings;
using Backend.Data;
using Unity.VisualScripting;
using UnityEngine;
using static LoremMath;

public class ElectricityWiresController : MonoBehaviour, IRenderable
{
    public GameObject electricityWire;
    private List<ElectricityWireController> electricityWires;
    public void Awake()
    {
        if(electricityWire == null)
        {
            throw new Exception("ElectricityWiresController: Electricity wire texture is null!");
        }
    }

    public void ClearState()
    {
        electricityWires?.ForEach(x => Destroy(x.gameObject));
        electricityWires?.Clear();
    }

    public void GenerateState()
    {
        GameData gameData = RootController.Instance.GameData;
        List<PowerLine> powerLines = gameData.PowerLines;
        electricityWires = new List<ElectricityWireController>();
        for (int i = 0; i < powerLines.Count; i++)
        {
            for (int j = i+1; j < powerLines.Count; j++)
            {
                WHVec<int> powerLineA = WH(powerLines[i].Location);
                WHVec<int> powerLineB = WH(powerLines[j].Location);
                Ord cmp = CompareLength(Sub(powerLineA, powerLineB), GameData.POWERLINE_REACH);
                if (cmp == Ord.LT || cmp == Ord.EQ)
                {
                    GameObject wire = Instantiate(electricityWire, transform);
                    ElectricityWireController controller = wire.GetComponent<ElectricityWireController>();
                    if(controller == null)
                    {
                        throw new Exception("ElectricityWiresController: Electricity wire controller is null!");
                    }
                    electricityWires.Add(controller);
                    wire.transform.position = ToUVec(Mul(Add(powerLineA, powerLineB), 0.5f));
                    controller.SetStartEndPositions(powerLineA, powerLineB);
                }
            }
        }
    }

    public void UpdateState()
    {
        electricityWires.ForEach(w => Destroy(w.gameObject));
        electricityWires.Clear();
        GenerateState();
    }

    public void SetOpacity(bool opacity)
    {
        if (electricityWires != null)
        {
            foreach(ElectricityWireController wire in electricityWires)
            {
                wire.SetOpacity(opacity);
            }
        }
    }
}
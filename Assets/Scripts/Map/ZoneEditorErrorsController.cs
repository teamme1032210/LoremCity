using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class ZoneEditorErrorsController : MonoBehaviour
{
    public GameObject errorTilePrefab = null!;
    private List<ZoneEditorErrorTileController> errorTiles = new List<ZoneEditorErrorTileController>();
    void Awake()
    {
        if(errorTilePrefab == null)
        {
            throw new Exception("Error tile prefab not set in ZoneEditorErrorsController");
        }
    }
    
    public void Clear()
    {
        errorTiles?.ForEach(tile => { Destroy(tile.gameObject); });
        errorTiles?.Clear();
    }

    private void CreateErrorTile(Point point, ZoneEditorErrorTileController.ErrorState state)
    {
            GameObject errorTile = Instantiate(errorTilePrefab, transform);
            errorTile.transform.position = new Vector3(point.X, point.Y, 0);
            ZoneEditorErrorTileController errorTileController = errorTile.GetComponent<ZoneEditorErrorTileController>();
            if (errorTileController == null)
            {
                throw new Exception("Error tile prefab does not have ZoneEditorErrorTileController!");
            }
            errorTileController.SetErrorState(state);
            errorTiles.Add(errorTileController);
    }
    
    public void UpdateState(List<Point> warnings, List<Point> errors)
    {
        Clear();
        errorTiles = new List<ZoneEditorErrorTileController>(warnings.Count + errors.Count);
        foreach (Point warning in warnings)
        {
            CreateErrorTile(warning, ZoneEditorErrorTileController.ErrorState.Warning);
        }
        foreach (Point error in errors)
        {
            CreateErrorTile(error, ZoneEditorErrorTileController.ErrorState.Error);
        }
    }
}

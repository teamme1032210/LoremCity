using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ZoneEditorErrorTileController : MonoBehaviour
{
    public enum ErrorState
    {
        None,
        Warning,
        Error,
    }

    public Sprite warningSign = null!;
    public Sprite errorSign = null!;
        
    private SpriteRenderer spriteRenderer = null!;
    private ErrorState _errorState = ErrorState.None;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (warningSign == null || errorSign == null)
        {
            throw new Exception("Warning or error sign not set in ZoneEditorErrorTileController");
        }
    }

    public void SetErrorState(ErrorState state)
    {
        switch (state)
        {
            case ErrorState.None:
                spriteRenderer.sprite = null;
                break;
            case ErrorState.Warning:
                spriteRenderer.sprite = warningSign;
                break;
            case ErrorState.Error:
                spriteRenderer.sprite = errorSign;
                break;
        }
    }

}

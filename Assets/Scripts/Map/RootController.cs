using UnityEngine;
using System;
using Backend.Data;
using UnityEngine.Events;
using Backend.Utilities;

public class RootController : MonoBehaviour, IDataProvider
{
    
    public UnityEvent DataAttach { get; set; } = new UnityEvent();
    public UnityEvent DataDetach { get; set; } = new UnityEvent();
    
    private static readonly float[] tickTimes = new float[]
    {
        1.0f,
        0.6f,
        0.3f,
    };
    
    private float lastTick = 0.0f;

    public MapController mapController = null!;

    public static RootController Instance
    {
        get
        {
            if (_instance == null)
            {
                throw new System.Exception("RootController not found in the scene!");
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }
    
    public bool IsInitialized { get; private set; } = false;

    private GameData? _gameData = null;

    private static RootController? _instance = null!;

    public GameData GameData {
        get {
            if (_gameData == null) {
                throw new Exception("GameData accessed before initialization!");
            }
            return _gameData;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            throw new System.Exception("More than one RootController in the scene!");
        }
        if (mapController == null)
        {
            throw new System.Exception("RootController is missing a reference to a child controller!");
        }
        _instance = this;
    }

    public void InitializeNew(int w, int h, string cname)
    {
        if (_gameData != null)
        {
            mapController.ClearState();
        }
        _gameData = new GameData(w, h, 1, DateTime.Now, cname);
        DataAttach.Invoke();
        _gameData.StartNewGame();
        mapController.GenerateState();
        lastTick = (long)Time.fixedTime;
        IsInitialized = true;
    }
    
    public void LoadGame(int slot) {
        DataPersistenceManager.SaveDescription[] saveDescriptions =
            DataPersistenceManager.GetSaveDescriptions(Application.persistentDataPath);
        if (saveDescriptions[slot - 1] != null) {
            if (_gameData != null) {
                DataDetach.Invoke();
                mapController.ClearState();
            }
            _gameData = DataPersistenceManager.LoadGame(slot, Application.persistentDataPath);
            DataAttach.Invoke();
            mapController.GenerateState();
            lastTick = (long)Time.fixedTime;
            IsInitialized = true;
        }
    }
    
    void FixedUpdate()
    {
        if (IsInitialized)
        {
            if (_gameData.IsPaused) {
                lastTick = (int)Time.fixedTime;
            } else {
                float currentTime = Time.fixedTime;
                while (currentTime - lastTick >= tickTimes[(int)_gameData.GameSpeed - 1])
                {
                    lastTick += tickTimes[(int)_gameData.GameSpeed - 1];
                    DateResponse time = _gameData.Tick();
                    MapController.Instance.fireTruckCanvasController.UpdateState();
                    UI_IngameController.Instance.LoadInhabitants();
                    UI_IngameController.Instance.SetDate();
                    UI_IngameController.Instance.SetSatisfaction();
                }
            }
        }
    }
}

using UnityEngine;
using System.Collections.Generic;
using Backend.Buildings;
using Backend.Data;
using static LoremMath;
using Unity.VisualScripting;

public class StreetsController : MonoBehaviour, IRenderable
{
    public GameObject streetTilePrefab;

    private static readonly int[,] sides = new int[,]
    {
        {-1, 0},
        {0, 1},
        {1, 0},
        {0, -1},
    };

    private List<StreetTileController> streetTileControllers = new List<StreetTileController>();

    public void Awake()
    {
        if (streetTilePrefab == null)
        {
            throw new System.Exception("Street Tile Prefab is not set");
        }
    }

    public void ClearState()
    {
        streetTileControllers?.ForEach(x => Destroy(x.gameObject));
        streetTileControllers?.Clear();
    }

    public void GenerateState()
    {
        
        GameData gameData = RootController.Instance.GameData;
        int mapwidth = gameData.MapWidth;
        int mapheight = gameData.MapHeight;

        List<Road> roads = RootController.Instance.GameData.Roads;
        streetTileControllers = new List<StreetTileController>(roads.Count);
        for (int i = 0; i < roads.Count; i++)
        {
            Road road = roads[i];
            GameObject streetTile = Instantiate(streetTilePrefab, transform);
            streetTile.transform.position = new Vector3(road.Location.X, road.Location.Y);
            StreetTileController streetTileController = streetTile.GetComponent<StreetTileController>();
            if (streetTileController == null)
            {
                throw new System.Exception("Street Tile Prefab does not have StreetTileController");
            }
            streetTileControllers.Add(streetTileController);
            
            // Get neighbours
            byte neighbourMask = 0;
            int locx = road.Location.Y;
            int locy = road.Location.X;
            for (int j = 0; j < sides.GetLength(0); j++)
            {
                if ( locx + sides[j,0] >= 0 && locx + sides[j, 0] < mapheight &&
                     locy + sides[j, 1] >= 0 && locy + sides[j, 1] < mapwidth &&
                     gameData.CellData[locx + sides[j, 0], locy + sides[j, 1]].Building is Road)
                {
                    neighbourMask |= (byte)(1 << j);
                }
            }
            
            streetTileController.SetNeighbours(neighbourMask);
        }
    }

    public void UpdateState() {
        streetTileControllers.ForEach(streetTileController => Destroy(streetTileController.gameObject));
        streetTileControllers.Clear();
        GenerateState();
    }

    public void SetOpacity(bool val)
    {
        if (val)
        {
            foreach (var cell in streetTileControllers)
            {
                Color oldcolor = cell.GetComponent<SpriteRenderer>().color;
                cell.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g, oldcolor.b, 0.5f);
            }
        }
        else
        {
            foreach (var cell in streetTileControllers)
            {
                Color oldcolor = cell.GetComponent<SpriteRenderer>().color;
                cell.GetComponent<SpriteRenderer>().color = new Color(oldcolor.r, oldcolor.g, oldcolor.b, 1);
            }
        }
    }

}


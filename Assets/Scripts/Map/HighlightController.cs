using UnityEngine;
using UnityEngine.Events;
using Backend.Buildings;
using System.Collections.Generic;
using static LoremMath;
using UnityEditor;
using static System.Runtime.CompilerServices.RuntimeHelpers;

public class HighlightController : MonoBehaviour, IRenderable
{

    public GameObject hightlightTilePrefab = null!;

    public UnityEvent<Vector2Int, KeyCode?> CellClickedEvent;
    public List<List<HighlightTileController>> Highlighttiles;

    private bool MouseIsDown = false;

    public void Awake()
    {
        if (hightlightTilePrefab == null)
        {
            throw new System.Exception("Highlight Tile Prefab is not set!");
        }
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            MouseIsDown = true;
        }
        if (Input.GetMouseButtonUp(0)) 
        {
            MouseIsDown = false;
        }
    }

    public void UpdateState()
    {
        ClearState();
        GenerateState();
    }
    public void ClearState()
    {
        Highlighttiles?.ForEach(x => x.ForEach(y => Destroy(y.gameObject)));
        Highlighttiles?.ForEach(x => x.Clear());
        Highlighttiles?.Clear();
    }

    public void GenerateState()
    {
        int width = RootController.Instance.GameData.MapWidth;
        int height = RootController.Instance.GameData.MapHeight;
        Highlighttiles = new List<List<HighlightTileController>>(width);
        for (int i = 0; i < width; i++)
        {
            List<HighlightTileController> row = new List<HighlightTileController>(height);
            for (int j = 0; j < height; j++)
            {
                GameObject tile = Instantiate(hightlightTilePrefab, transform);
                tile.transform.position = new Vector3(i, j, transform.position.z);

                // Get the controller
                HighlightTileController controller = tile.GetComponent<HighlightTileController>();
                if (controller == null)
                {
                    throw new System.Exception("Highlight Tile Prefab does not have a HighlightTileController!");
                }
                controller.SetGridPosition(new Vector2Int(i, j));
                controller.OnHighlightTileClicked += HighlightTileClicked;
                controller.OnHighlight += OnHighlightHandler;
                controller.OnHighlightLeave += OnHighlightLeaveHandler;
                row.Add(controller);
            }
            Highlighttiles.Add(row);
        }
    }

    private void OnHighlightLeaveHandler(WHVec<int> position)
    {
        Highlighttiles[position.X][position.Y].UnSetHoverColor();
        Building? building = RootController.Instance.GameData.CellData[position.Y, position.X].Building;
        if (building != null)
        {
            for (int i = building.Location.X; i < building.Location.X + building.SizeX; i++)
            {
                for (int j = building.Location.Y; j < building.Location.Y + building.SizeY; j++)
                {
                    Highlighttiles[i][j].UnSetHoverColor();
                }
            }
        }
    }

    private void OnHighlightHandler(WHVec<int> position)
    {
        Highlighttiles[position.X][position.Y].SetHoverColor();
        Building? building = RootController.Instance.GameData.CellData[position.Y, position.X].Building;
        if (building != null)
        {
            if (building is ZoneBuilding && building is Home  && MapController.Instance._homeZoneBuildingList.Contains((Home)building))
            {
                return;
            }
            if (building is ZoneBuilding && building is Workplace && MapController.Instance._workZoneBuildingList.Contains((Workplace)building))
            {
                return;
            }
            for (int i = building.Location.X; i < building.Location.X + building.SizeX; i++)
            {
                for (int j = building.Location.Y; j < building.Location.Y + building.SizeY; j++)
                {
                    Highlighttiles[i][j].SetHoverColor();
                }
            }
        }
        if (MouseIsDown)
        {
            CellClickedEvent?.Invoke(new Vector2Int(position.X, position.Y), null);
        }
    }



    void HighlightTileClicked(Vector2Int position, KeyCode? keycode)
    {
        CellClickedEvent?.Invoke(position, keycode);
    }
}

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Backend.Buildings;
using Backend.Buildings.Interfaces;
using Backend.Data;
using Backend.Utilities;
using Backend.Zones;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

public enum MapModeEnum
{
    ShowAll,
    ZoneEditor,
}

public enum ZoneEditorModeEnum
{
    SpecialZone,
    ResidentialZone,
    ServiceZone,
    IndustrialZone,
}

public class MapController : MonoBehaviour, IRenderable, IDataUser
{
    public UnityEvent<int, int> SpecialBuildingClicked;
    public UnityEvent<int, int> ZoneClicked;
    

    public static MapController Instance
    {
        get
        {
            if (_instance == null)
            {
                throw new System.Exception("No MapController in scene!");
            }
            return _instance;
        }
        private set { _instance = value; }
    }
    public MapModeEnum MapMode { get; private set; } = MapModeEnum.ShowAll;

    public ZoneGridController zoneGridController = null!;
    public StreetsController streetsController = null!;
    public TreesController treesController = null!;
    public SpecialGridController specialGridController = null!;
    public HighlightController highlightController = null!;
    public ZoneEditorErrorsController zoneEditorErrorsController = null!;
    public ElectricityWiresController electricityWiresController = null!;
    public FiresController firesController = null!;
    public ActiveHighlightController activeHighlightController = null!;
    public ZoneBuildingsController zoneBuildingsController = null!;
    public FireTruckCanvasController fireTruckCanvasController = null!;
    public ElectricityWarningController electricityWarningController = null!;

    private List<IRenderable> _renderables = null!;

        private static MapController _instance = null!;
    public EditorModeEnum _mode = EditorModeEnum.Default;
    private ZoneEditorModeEnum _zoneEditorMode = ZoneEditorModeEnum.ResidentialZone;
    public List<Home> _homeZoneBuildingList = new List<Home>();
    public List<Workplace> _workZoneBuildingList = new List<Workplace>();

    [CanBeNull] public WHMat<int> ZoneEditorState { get; private set; } = null;

    void Awake()
    {
        if (_instance != null)
        {
            throw new Exception("More than one MapController in scene!");
        }
        if (zoneGridController == null ||
            streetsController == null ||
            treesController == null ||
            specialGridController == null ||
            highlightController == null ||
            zoneEditorErrorsController == null ||
            electricityWiresController == null ||
            firesController == null ||
            activeHighlightController == null || 
            zoneBuildingsController == null ||
            fireTruckCanvasController == null ||
            electricityWarningController == null
            )
        {
            throw new Exception("MapController is missing a reference to a child controller!");
        }
        else {
            _renderables = new List<IRenderable>()
            {
                zoneGridController,
                streetsController,
                treesController,
                specialGridController,
                highlightController,
                electricityWiresController,
                firesController,
                activeHighlightController,
                zoneBuildingsController,
                fireTruckCanvasController,
                electricityWarningController,
            };
        }

        _instance = this;
    }

    void Start()
    {
        RootController.Instance.DataAttach.AddListener(OnDataAttach);
        RootController.Instance.DataDetach.AddListener(OnDataDetach);
    }

    void OnGameDataCreated()
    {
    }

    public void SetMapMode(MapModeEnum mode)
    {
        if (mode != MapMode)
        {
            switch (mode)
            {
                case MapModeEnum.ShowAll:
                    DeInitializeZoneEditor();
                    MapMode = MapModeEnum.ShowAll;
                    break;
                case MapModeEnum.ZoneEditor:
                    InitializeZoneEditor();
                    MapMode = MapModeEnum.ZoneEditor;
                    break;
            }
        }
    }

    private void InitializeZoneEditor()
    {
        if (RootController.Instance.IsInitialized)
        {
            GameData gameData = RootController.Instance.GameData;
            int width = gameData.MapWidth;
            int height = gameData.MapHeight;
            WHMat<MapCell> cellData = gameData.CellData;
            ZoneEditorState = new WHMat<int>(width, height);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    ZoneEditorState[i, j] = 
                        (int)cellData[i,j].Zone.ZoneType;
                }
            }

            streetsController.SetOpacity(true);
            specialGridController.SetOpacity(true);
            treesController.SetOpacity(true);
            electricityWiresController.SetOpacity(true);
            firesController.SetOpacity(true);
            zoneGridController.UseZoneEditorState = true;
            zoneGridController.UpdateState();
            electricityWarningController.gameObject.SetActive(false);
            activeHighlightController.gameObject.SetActive(false);

            zoneEditorErrorsController.gameObject.SetActive(true);
            zoneEditorErrorsController.Clear();
        }
        else
        {
            throw new SystemException("RootController not initialized!");
        }
    }

    private void DeInitializeZoneEditor()
    {
        ZoneEditorState = null;
        streetsController.SetOpacity(false);
        specialGridController.SetOpacity(false);
        treesController.SetOpacity(false);
        electricityWiresController.SetOpacity(false);
        firesController.SetOpacity(false);
        zoneEditorErrorsController.gameObject.SetActive(false);
        electricityWarningController.gameObject.SetActive(true);
        zoneGridController.UseZoneEditorState = false;
        zoneGridController.UpdateState();
        electricityWarningController.UpdateState();
        activeHighlightController.gameObject.SetActive(true);
    }


    public void OnZoneEditorButtonClicked()
    {
        SetMapMode(MapModeEnum.ZoneEditor);
    }

    public void OnZoneEditorSetButtonClicked()
    {
        GameData gameData = RootController.Instance.GameData;
        gameData.FactoryLayer.RearrangeZoneLayout(ZoneEditorState,_homeZoneBuildingList, _workZoneBuildingList); //two lists here
        SetMapMode(MapModeEnum.ShowAll);
        UpdateState();
    }

    public void OnZoneEditorBackButtonClicked()
    {
        SetMapMode(MapModeEnum.ShowAll);
        MapController.Instance.UpdateState();
    }
    
    public void GenerateState()
    {
        MainCameraController.Instance.SetPanLimit();
        MainCameraController.Instance.SetZoomOutLimit();
        _renderables.ForEach(renderable => renderable.GenerateState());
    }

    public void UpdateState() {
        _renderables.ForEach(renderable => renderable.UpdateState());
    }

    public void ClearState()
    {
        _mode = EditorModeEnum.Default;  
        MainCameraController.Instance.SetPanLimit();
        MainCameraController.Instance.SetZoomOutLimit();
        _renderables.ForEach(renderable => renderable.ClearState());
    }
    
    public void OnSpecialZoneButtonClicked()
    {
        _zoneEditorMode = ZoneEditorModeEnum.SpecialZone;
    }
    
    public void OnResidentialZoneButtonClicked()
    {
        _zoneEditorMode = ZoneEditorModeEnum.ResidentialZone;
    }
    
    public void OnServiceZoneButtonClicked()
    {
        _zoneEditorMode = ZoneEditorModeEnum.ServiceZone;
    }
    
    public void OnIndustrialZoneButtonClicked()
    {
        _zoneEditorMode = ZoneEditorModeEnum.IndustrialZone;
    }

    public void OnTreeButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.Tree) ? EditorModeEnum.Default : EditorModeEnum.Tree;
    }

    public void OnRoadButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.Road) ? EditorModeEnum.Default: EditorModeEnum.Road;
    }

    public void OnPowerlineButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.Powerline) ? EditorModeEnum.Default: EditorModeEnum.Powerline;
    }


    public void OnPoliceStationButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.PoliceStation) ? EditorModeEnum.Default: EditorModeEnum.PoliceStation;
    }
    public void OnFireStationButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.FireStation) ? EditorModeEnum.Default: EditorModeEnum.FireStation;
    }

    public void OnStadiumButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.Stadium) ? EditorModeEnum.Default: EditorModeEnum.Stadium;
    }

    public void OnUniversityButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.University) ? EditorModeEnum.Default: EditorModeEnum.University;
    }

    public void OnSecondarySchoolButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.SecondarySchool) ? EditorModeEnum.Default: EditorModeEnum.SecondarySchool;
    }

    public void OnPowerplantButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.Powerplant) ? EditorModeEnum.Default: EditorModeEnum.Powerplant;
    }

    public void OnDeleteButtonClicked()
    {
        _mode = (_mode == EditorModeEnum.Delete) ? EditorModeEnum.Default: EditorModeEnum.Delete;
    }

    private bool HasBurningBuilding()
    {
        return RootController.Instance.GameData.ZoneBuildings.Any(building => building.IsOnFire) ||
        RootController.Instance.GameData.GetSpecialBuildings().Any(sbuilding => sbuilding is ICanBurn && ((ICanBurn)sbuilding).IsOnFire);
    }

    public void OnCellClicked(Vector2Int point, KeyCode? keycode)
    {
        GameData gameData = RootController.Instance.GameData;
        switch (MapMode)
        {
            case MapModeEnum.ShowAll:
                switch (_mode)
                {
                    case EditorModeEnum.Default:
                        if (keycode == KeyCode.LeftShift)
                        {
                            Zone selzone = gameData.CellData[point.y, point.x].Zone;
                            if (selzone.ZoneType != ZoneType.Special)
                            {
                                ZoneClicked?.Invoke(point.x, point.y);
                            }
                        }
                        else
                        {
                            Building sbuilding = gameData.CellData[point.y, point.x].Building;
                            if (sbuilding != null)
                            {
                                SpecialBuildingClicked?.Invoke(point.x, point.y);
                            }
                        }                     
                        break;
                    case EditorModeEnum.Tree:
                        gameData.FactoryLayer.BuildForest(new Point(point.x, point.y));
                        treesController.UpdateState();
                        break;
                    case EditorModeEnum.Road:
                        gameData.FactoryLayer.BuildRoad(new Point(point.x, point.y));
                        streetsController.UpdateState();
                        break;
                    case EditorModeEnum.Powerline:
                        gameData.FactoryLayer.BuildPowerLine(new Point(point.x, point.y));
                        specialGridController.UpdateState();
                        electricityWiresController.UpdateState();
                        electricityWarningController.UpdateState();
                        break;
                    case EditorModeEnum.PoliceStation:
                        gameData.FactoryLayer.BuildPoliceStation(new Point(point.x, point.y));
                        specialGridController.UpdateState();
                        break;
                    case EditorModeEnum.FireStation:
                        gameData.FactoryLayer.BuildFireStation(new Point(point.x, point.y));
                        specialGridController.UpdateState();
                        break;
                    case EditorModeEnum.Stadium:
                        gameData.FactoryLayer.BuildStadium(new Point(point.x, point.y));
                        specialGridController.UpdateState();
                        break;
                    case EditorModeEnum.University:
                        gameData.FactoryLayer.BuildUniversity(new Point(point.x, point.y));
                        specialGridController.UpdateState();
                        break;
                    case EditorModeEnum.SecondarySchool:
                        gameData.FactoryLayer.BuildSecondarySchool(new Point(point.x, point.y));
                        specialGridController.UpdateState();
                        break;
                    case EditorModeEnum.Powerplant:
                        gameData.FactoryLayer.BuildPowerPlant(new Point(point.x, point.y));
                        specialGridController.UpdateState();
                        break;
                    case EditorModeEnum.Delete:
                        Building building = gameData.CellData[point.y, point.x].Building;
                        if (building is Forest)
                        {
                            gameData.FactoryLayer.DestroyForest((Forest)building);
                            treesController.UpdateState();
                        }
                        else if (building is Road)
                        {
                            if (!HasBurningBuilding())
                            {
                                gameData.FactoryLayer.DestroyRoad((Road)building, true);
                                streetsController.UpdateState();
                            }
                        }
                        else if (building is Stadium) 
                        {
                            gameData.FactoryLayer.DestoryStadium((Stadium)building);
                        }
                        else if (building is PoliceStation)
                        {
                            gameData.FactoryLayer.DestroyPoliceStation((PoliceStation)building);
                        }
                        else if (building is FireStation)
                        {
                            if (((FireStation)building).FireTruckLocation is null)
                            {
                                gameData.FactoryLayer.DestroyFireStation((FireStation)building);
                            }
                        }
                        else if (building is PowerPlant)
                        {
                            gameData.FactoryLayer.DestroyPowerPlant((PowerPlant)building);
                        }
                        else if (building is University)
                        {
                            gameData.FactoryLayer.DestroyUniversity((University)building);
                        }
                        else if (building is PowerLine)
                        {
                            gameData.FactoryLayer.DestroyPowerLine((PowerLine)building);
                            electricityWiresController.UpdateState();
                            electricityWarningController.UpdateState();
                        }
                        else if (building is SecondarySchool)
                        {
                            gameData.FactoryLayer.DestroySecondarySchool((SecondarySchool)building);
                        }

                        specialGridController.UpdateState();
                        highlightController.UpdateState();
                                break;
                }
                electricityWarningController.UpdateState();
                activeHighlightController.UpdateState();
                break;
            case MapModeEnum.ZoneEditor:
                if (!RootController.Instance.IsInitialized)
                {
                    throw new Exception("Game is not initialized");
                }
                switch (_zoneEditorMode)
                {
                    case ZoneEditorModeEnum.SpecialZone:
                        Building? building = RootController.Instance.GameData.
                            CellData[point.y, point.x].Building;
                        if (building is not null and ZoneBuilding)
                        {
                            ZoneBuilding zbuilding = (ZoneBuilding)building;
                            if (zbuilding != null && !zbuilding.IsOnFire && !_homeZoneBuildingList.Contains(zbuilding) && !_workZoneBuildingList.Contains(zbuilding))
                            {
                                UI_IngameController.Instance.ShowPopUp(zbuilding, point.x, point.y);
                            }
                            else
                            {
                                ZoneEditorState[point.x, point.y] = (int)ZoneType.Special;
                            }
                        }
                        else
                        {
                                ZoneEditorState[point.x, point.y] = (int)ZoneType.Special;
                        }
                        break;
                    case ZoneEditorModeEnum.ResidentialZone:
                        ZoneEditorState[point.x, point.y] = (int)ZoneType.Residential;
                        break;
                    case ZoneEditorModeEnum.ServiceZone:
                        ZoneEditorState[point.x, point.y] = (int)ZoneType.Service;
                        break;
                    case ZoneEditorModeEnum.IndustrialZone:
                        ZoneEditorState[point.x, point.y] = (int)ZoneType.Industrial;
                        break;
                }

                var (isValid, price, warnings, errors) = gameData.FactoryLayer.ValidateZoneArrangement(ZoneEditorState);
                UI_IngameController.Instance.SetZoneEditorValidationState(isValid, price);
                zoneGridController.UpdateState();
                zoneEditorErrorsController.UpdateState(warnings, errors);
                break;
        }

        
    }

    private void UpdateStateHandler(object sender, Point pont) {
        UpdateState();
    }

    public void OnDataAttach() {
        RootController.Instance.GameData.BuildingCaughtFire += UpdateStateHandler;
    }

    public void OnDataDetach() {
        RootController.Instance.GameData.BuildingCaughtFire -= UpdateStateHandler;
    }
}

using UnityEngine;
using System;
using System.Collections.Generic;
using Backend.Utilities;
using Backend.Zones;

public class ZoneGridController : MonoBehaviour, IRenderable
{
    public static ZoneGridController Instance { get; private set; } = null!;
    public GameObject gridElementPrefab = null!;

    private List<List<GridElementController>> gridElements = null!;
    
    public bool UseZoneEditorState { get; set; } = false;
    
    void Awake()
    {
        if (gridElementPrefab == null)
        {
            throw new Exception("Grid element prefab is not set!");
        }
        if (gridElementPrefab.GetComponent<GridElementController>() == null)
        {
            throw new Exception("Grid element prefab does not have GridElementController!");
        }
        if (Instance != null)
        {
            throw new Exception("ZoneGridController is a singleton!");
        }
        Instance = this;
    }

    public void ClearState()
    {
        gridElements?.ForEach(x => x.ForEach(y => Destroy(y.gameObject)));
        gridElements?.ForEach( x => x.Clear());
        gridElements?.Clear();
    }

    public void GenerateState()
    {
        System.Random rand = new System.Random(314159265);
        int width = RootController.Instance.GameData.MapWidth;
        int height = RootController.Instance.GameData.MapHeight;
        gridElements = new List<List<GridElementController>>(width);
        for (int i = 0; i < width; i++)
        {
            List<GridElementController> gridElementRow = new List<GridElementController>(height);
            for (int j = 0; j < height; j++)
            {
                // Create instance and set position
                GameObject gridElement = Instantiate(gridElementPrefab, transform);
                gridElement.transform.position = new Vector3(i, j, 0);
                gridElement.transform.localEulerAngles = new Vector3(
                        0,
                        0,
                        90 * rand.Next(0, 4)
                    );

                // Setup Controller script
                GridElementController gridElementController = gridElement.GetComponent<GridElementController>();
                gridElementController.SetGridPosition(i / (float)width, j / (float)height);
                gridElementRow.Add(gridElementController);
            }
            gridElements.Add(gridElementRow);
        }
        UpdateState();
    }

    public void UpdateState()
    {
        int width = RootController.Instance.GameData.MapWidth;
        int height = RootController.Instance.GameData.MapHeight;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                // Get zone type (defaulting to Special if no cell data is available)
                var (zoneType, zoneLevel) = GetZoneInfo(i, j);
                // Set zone type
                GridElementController gridElementController = gridElements[i][j];
                if (gridElementController == null)
                {
                    throw new Exception("Grid element prefab does not have GridElementController!");
                }
                gridElementController.SetZoneInfo(zoneType, zoneLevel);
            }
        }
    }

    private WHMat<MapCell> CellData => RootController.Instance.GameData?.CellData;

    private (ZoneType, int) GetZoneInfo(int i, int j)
    {
        Zone zone = CellData[i, j]?.Zone;
        if (!UseZoneEditorState)
        {
            return (zone.ZoneType, zone.CurrentLevel);
        }
        else
        {
            return ((ZoneType)MapController.Instance.ZoneEditorState[i, j], zone.CurrentLevel);
        }
    }
}

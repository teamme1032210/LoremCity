using System.Collections;
using System.Collections.Generic;
using Backend.Buildings.Interfaces;
using Backend.Data;
using Backend.Utilities;
using UnityEngine;

public class FiresController : MonoBehaviour, IRenderable
{
    public GameObject fireTilePrefab;
    private List<FireTileController> fireTileControllers = new List<FireTileController>();
    void Awake()
    {
        if(fireTilePrefab == null)
        {
            throw new System.Exception("FiresController: Fire tile prefab is null!");
        }
    }

    public void ClearState()
    {
        fireTileControllers?.ForEach(x => Destroy(x.gameObject));
        fireTileControllers?.Clear();
    }

    public void GenerateState()
    {
        GameData gameData = RootController.Instance.GameData;
        fireTileControllers.Clear();

        WHMat<MapCell> cellData = gameData.CellData;
        for (int i = 0; i < cellData.Width; i++)
        {
            for (int j = 0; j < cellData.Height; j++)
            {
                if (cellData[i, j].Building is ICanBurn burnable && burnable.IsOnFire)
                {
                    GameObject fireTile = Instantiate(fireTilePrefab, transform);
                    fireTile.transform.position = new Vector3(i, j);
                    FireTileController fireTileController = fireTile.GetComponent<FireTileController>();
                    if(fireTileController == null)
                    {
                        throw new System.Exception("FiresController: Fire tile controller is null!");
                    }
                    fireTileControllers.Add(fireTileController);
                }
            }
        }
    }

    public void UpdateState()
    {
        fireTileControllers.ForEach( f => Destroy(f.gameObject));
        fireTileControllers.Clear();
        GenerateState();
    }

    public void SetOpacity(bool opaque)
    {
        foreach (FireTileController controller in fireTileControllers)
        {
            controller.SetOpacity(opaque);
        }
    }
}

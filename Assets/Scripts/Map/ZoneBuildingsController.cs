using UnityEngine;
using System.Collections.Generic;
using Backend.Buildings;

public class ZoneBuildingsController : MonoBehaviour, IRenderable
{
    public GameObject zoneBuildingPrefab = null!;

    private List<ZoneBuildingController> zoneBuildingControllers = new List<ZoneBuildingController>();

    public void StartGame()
    {
        if (zoneBuildingPrefab == null)
        {
            throw new System.Exception("ZoneBuildingPrefab is null");
        }

        GenerateState();
    }

    public void GenerateState()
    {
        List<ZoneBuilding> zoneBuildings = RootController.Instance.GameData.ZoneBuildings;
        zoneBuildingControllers = new List<ZoneBuildingController>(
                zoneBuildings.Count
            );
        for (int i = 0; i < zoneBuildings.Count; i++)
        {
            GameObject zoneBuilding = Instantiate(
                zoneBuildingPrefab,
                transform
            );
            zoneBuilding.transform.position = new Vector3(zoneBuildings[i].Location.X, zoneBuildings[i].Location.Y, 0);
            ZoneBuildingController zoneBuildingController = zoneBuilding
                .GetComponent<ZoneBuildingController>();
            if (zoneBuildingController == null)
            {
                throw new System.Exception("ZoneBuildingController is null");
            }
            zoneBuildingController.GenerateComponentSprites(zoneBuildings[i].Zone.ZoneType, zoneBuildings[i].Zone.CurrentLevel);
            zoneBuildingController.GenerateChildren(
                zoneBuildings[i].Variation.SizeX,
                zoneBuildings[i].Variation.SizeY
            );
            zoneBuildingControllers.Add(zoneBuildingController);
        }
    }

    public void UpdateState()
    {
        ClearState();
        GenerateState();
    }
    
    public void ClearState()
    {
        zoneBuildingControllers?.ForEach(x => Destroy(x.gameObject));
        zoneBuildingControllers?.Clear();
    }

    public void HideBuilding(ZoneBuilding zbuilding)
    {
        ZoneBuildingController selected_item = null!;
        foreach (var item in zoneBuildingControllers)
        {
            if (item.transform.position.x == zbuilding.Location.X && item.transform.position.y == zbuilding.Location.Y)
            {
                selected_item = item;
            }
        }
        if (selected_item != null)
        {
            zoneBuildingControllers.Remove(selected_item);
            Destroy(selected_item?.gameObject);
        }
    }
}

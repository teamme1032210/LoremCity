using Backend.Data;
using UnityEngine.Events;

public interface IDataProvider {
    public UnityEvent DataAttach { get; set; }
    public UnityEvent DataDetach { get; set; }
}
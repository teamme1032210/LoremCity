using System;
using UnityEngine;
using UnityEngine.UI;

public class ZoneSelectController : MonoBehaviour
{
    public Button setZoneButton = null!;
    
    void Awake()
    {
        if (setZoneButton == null)
        {
            throw new Exception("Set zone button is not set!");
        }
    }

    public void SetSetZoneButtonEnabled(bool enabled)
    {
        setZoneButton.interactable = enabled;
    }

    public void SetPrice(int price)
    {
        // TODO
    }

    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Backend.Buildings;

public class FireTruckController : MonoBehaviour
{
    public float pos_x;
    public float pos_y;

    public SpriteRenderer renderer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SetPosition(float x, float y , FireStation.Orientation? orientation)
    {
        pos_x = x;
        pos_y = y;
        switch (orientation) 
        {
            case FireStation.Orientation.South:
                transform.Rotate(0f, 0f, 90f);
                break;
            case FireStation.Orientation.North:
                transform.Rotate(0f, 0f, -90f);
                break;
            case FireStation.Orientation.West:
                transform.Rotate(0f, 0f, 0f);
                break;
            case FireStation.Orientation.East:
                transform.Rotate(0f, 0f, 0f);
                break;
        
        }            
    }
}

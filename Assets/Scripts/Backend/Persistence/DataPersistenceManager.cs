using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Backend.Data;
using UnityEngine;

public static class DataPersistenceManager {

    [Serializable]
    public class SaveDescription {
        public bool isEmpty;
        public string name;
        public string file;
        public int num;
        public long time;
    }

    [Serializable]
    private class SaveDescriptionContainer {
        public SaveDescription?[] saveDescriptions;
    }
    
    public const int MAX_SAVE_COUNT = 3;
    public static GameData LoadGame(int num, string persistentDataPath) {
        if (num < 1 || num > 3) {
            throw new Exception("Invalid save number");
        }

        UnityEngine.Debug.Log($"Loading game {num} from {persistentDataPath}");
        string path = Path.Combine(persistentDataPath, $"{num}.loremsave");
        BinaryFormatter formatter = new BinaryFormatter();
        using (FileStream fs = new FileStream(path, FileMode.Open)) {
            return (GameData)formatter.Deserialize(fs);
        }
    }

    public static void SaveGame(int num, string persistentDataPath, GameData gameData) {
        if (num < 1 || num > 3) {
            throw new Exception("Invalid save number");
        }
        UnityEngine.Debug.Log($"Saving game {num} to {persistentDataPath}");
        string path = Path.Combine(persistentDataPath, $"{num}.loremsave");
        BinaryFormatter formatter = new BinaryFormatter();
        using (FileStream fs = new FileStream(path, FileMode.Create)) {
            formatter.Serialize(fs, gameData);
        };
        SaveDescription[] descriptions = GetSaveDescriptions(persistentDataPath);
        descriptions[num - 1] = new SaveDescription() {
            isEmpty = false,
            name = gameData.CityName,
            file = $"{num}.loremsave",
            num = num,
            time = DateTimeOffset.UtcNow.ToUnixTimeSeconds()
        };
        UpdateSaveDescriptions(persistentDataPath, descriptions);
    }

    public static SaveDescription?[] GetSaveDescriptions(string persistentDataPath) {
        string descriptionPath = Path.Combine(persistentDataPath, "descriptions.json");
        SaveDescriptionContainer descriptionContainer;
        if (File.Exists(descriptionPath)) {
            using (FileStream fs = new FileStream(descriptionPath, FileMode.Open)) {
                using (StreamReader sr = new StreamReader(fs)) {
                    string serialized = sr.ReadToEnd();
                    descriptionContainer = JsonUtility.FromJson<SaveDescriptionContainer>(serialized);
                    for (int i = 0; i < MAX_SAVE_COUNT; i++) {
                        if (descriptionContainer.saveDescriptions[i]?.isEmpty ?? false) {
                            descriptionContainer.saveDescriptions[i] = null;
                        }
                    }

                    return descriptionContainer.saveDescriptions;
                }
            }
        } else {
            SaveDescription?[] descriptions = new SaveDescription?[MAX_SAVE_COUNT];
            for (int i = 0; i < MAX_SAVE_COUNT; i++) {
                descriptions[i] = null;
            }
            return descriptions;
        }
    }

    private static void UpdateSaveDescriptions(string persistentDataPath, SaveDescription?[] descriptions) {
        string descriptionPath = Path.Combine(persistentDataPath, "descriptions.json");
        SaveDescriptionContainer descriptionContainer = new SaveDescriptionContainer() {
            saveDescriptions = descriptions
        };
        for (int i = 0; i < MAX_SAVE_COUNT; i++) {
            if(descriptionContainer.saveDescriptions[i] == null) {
                descriptionContainer.saveDescriptions[i] = new SaveDescription() {
                    isEmpty = true,
                };
            }
        }
        using (FileStream fs = new FileStream(descriptionPath, FileMode.Create)) {
            using (StreamWriter sr = new StreamWriter(fs)) {
                string serialized = JsonUtility.ToJson(descriptionContainer);
                sr.Write(serialized);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings.Interfaces
{
    public interface ICanBurn
    {
        public bool IsOnFire { get; set; }
        public DateTime? IsOnFireSince { get; set; }
        public bool HasDispatchedTruck { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Utilities;

namespace Backend.Buildings.Interfaces
{
    public interface INeedPower : IPowerBuilding
    {
        public int PowerNeeded { get; }
        public PowerNetwork? Network { get; set; }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Backend.Utilities;
using Backend.GlobalEnums;

namespace Backend.Buildings.Interfaces
{
    public interface IExtraInfo
    {
        public DateResponse BuildDate {get;}
        public ExpenseType MaintenanceExpenseType {get;}
    }
}

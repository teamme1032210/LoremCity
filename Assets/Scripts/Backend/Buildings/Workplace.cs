﻿using Backend.Zones;
using Backend.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings
{
    [Serializable]
    public abstract class Workplace : ZoneBuilding
    {
        internal Workplace(Point location, Zone zone, Variation variation, DateResponse buildDate) : base(location, zone, variation, buildDate) { } 
    }
}

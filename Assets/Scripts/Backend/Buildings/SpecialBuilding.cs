﻿using Backend.Buildings.Interfaces;
using Backend.Utilities;
using Backend.GlobalEnums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings
{
    [Serializable]
    public class SpecialBuilding : Building, IExtraInfo
    {
        public bool IsActive
        {
            get; set;
        }

        public DateResponse BuildDate {get;}

        public ExpenseType MaintenanceExpenseType {get;}

        public SpecialBuilding(Point location, int sizex, int sizey, DateResponse buildDate, ExpenseType expenseType) : base(location, sizex, sizey) 
        { 
            BuildDate = buildDate;
            MaintenanceExpenseType = expenseType;
        }
    }
}

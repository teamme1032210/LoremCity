﻿using Backend.Zones;
using Backend.Utilities.Enums;
using Backend.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Utilities;

namespace Backend.Buildings
{
    [Serializable]
    public class IndustrialWorkplace : Workplace
    {
        public IndustrialWorkplace(Point location, Zone zone, Variation variation, DateResponse buildDate) : base(location, zone, variation, buildDate) { }
    }
}

﻿using Backend.Buildings.Interfaces;
using Backend.Inhabitants;
using Backend.Utilities;
using Backend.Zones;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.GlobalEnums;

namespace Backend.Buildings
{
    [Serializable]
    public abstract class ZoneBuilding : Building, ICanBurn, IExtraInfo
    {
        private Zone _zone;
        private Variation _variation;

        public DateResponse BuildDate {get;}

        public ExpenseType MaintenanceExpenseType {get;}

        public int ActivePoliceStations
        {
            get;
        }

        public int ActiveStadiums
        {
            get;
        }

        public bool HasFireStationNearby
        {
            get;
        }

        public Zone Zone
        {
            get { return _zone; }
            set { _zone = value; }
        }

        public Variation Variation
        {
            get { return _variation; }
        }

        internal ZoneBuilding(Point location, Zone zone, Variation variation, DateResponse buildDate)
            : base(location, variation.SizeX,variation.SizeY)
        {
            BuildDate = buildDate;
            _zone = zone;
            _variation = variation;
        }

        public bool IsOnFire { get; set; }
        public DateTime? IsOnFireSince { get; set; }
        public bool HasDispatchedTruck { get; set; }
    }
}

﻿using Backend.Buildings.Interfaces;
using Backend.Utilities;
using Backend.GlobalEnums;
using Backend.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Backend.Buildings
{
    [Serializable]
    public class FireStation : SpecialBuilding, INeedPower
    {
        private ICanBurn? _dispatchedTo;
        private int _pathIndex;
        private bool _isOnDuty;
        private List<(int,int)?>? _toDestPath;
        private List<(int,int)?>? _fromDestPath;
        private Orientation? _fireTruckOrientation;
        private Orientation _nextOrientation;


        [NonSerialized]
        public EventHandler? FireWasExtinguishedEvent;

        public FireStation(Point location, DateResponse buildDate)
            : base(location, 2, 2, buildDate, ExpenseType.FireStationMaintenance) 
        {
            FireTruckLocation = null;
            _pathIndex = 0;
            _isOnDuty = false;
            _fromDestPath = null;
            _toDestPath = null;
            _dispatchedTo = null;
            FireTruckToBeDisposed = false;
            _fireTruckOrientation = null;
        }

        public ICanBurn? DispatchedTo
        {
            get => _dispatchedTo;
            set { _dispatchedTo = value; }
        }

        public Orientation? FireTruckOrientation
        {
            get { return _fireTruckOrientation; }
            set { _fireTruckOrientation = value; }
        }

        public bool FireTruckToBeDisposed
        {
            get; set;
        }


        public bool IsDispatched
        {
            get
            {
                return FireTruckLocation is not null;
            }
        }

        public bool IsOnDuty
        {
            get
            {
                return _isOnDuty;
            }
        }

        public (int,int)? FireTruckLocation
        {
            get; set;
        }

        public int PowerNeeded { get => 15; }
        public PowerNetwork? Network { get; set; }
        public bool Walked { get; set; } = false;
        public new int GetPointCount() => base.GetPointCount;
        public new Point GetPoint(int index) => base.GetPoint(index);

        public void AbortMission(List<(int,int)?>? pathBack)
        {
            _pathIndex = 0;
            _isOnDuty = false;
            _toDestPath = null;
            _dispatchedTo = null;
            _fromDestPath = pathBack;
        }

        public void MoveOneUnitForward()
        {
            if (FireTruckLocation is null)
            {
                return;
            }
            if (_isOnDuty)
            {
                FireTruckLocation = _toDestPath[_pathIndex];
                _fireTruckOrientation = _nextOrientation;
            }
            else
            {
                FireTruckLocation = _fromDestPath[_pathIndex];
                _fireTruckOrientation = _nextOrientation;
            }
            _pathIndex++;
            if (_fromDestPath is not null && _pathIndex == _fromDestPath.Count && !_isOnDuty)
            {
                _pathIndex = 0;
                _fromDestPath = null;
                FireTruckToBeDisposed = true;
            }
            else if (_fromDestPath is not null && _pathIndex != _fromDestPath.Count && !_isOnDuty)
            {
                DetermineNextOrientation(_fromDestPath[_pathIndex].Value,_fromDestPath[_pathIndex-1].Value);
            }
            else if (_toDestPath is not null && _pathIndex == _toDestPath.Count && _isOnDuty)
            {
                _pathIndex = 0;
                PutOut(_dispatchedTo);
                _isOnDuty = false;
                _toDestPath = null;
                _dispatchedTo.HasDispatchedTruck = false;
                _dispatchedTo = null;
                FireWasExtinguishedEvent?.Invoke(this, EventArgs.Empty);
            }
            else if (_toDestPath is not null && _pathIndex != _toDestPath.Count && _isOnDuty)
            {
                DetermineNextOrientation(_toDestPath[_pathIndex].Value,_toDestPath[_pathIndex-1].Value);
            }
        }

        public void MoveUnitsToDestination(ICanBurn building, List<(int,int)?>? newPath)
        {
            _pathIndex = 0;
            _toDestPath = newPath;
            _fromDestPath = new List<(int,int)?>();
            foreach((int,int)? pair in newPath)
            {
                _fromDestPath.Add(pair);
            }
            _fromDestPath.Reverse();
            _dispatchedTo = building;
            _dispatchedTo.HasDispatchedTruck = true;
            _isOnDuty = true;
            FireTruckLocation = _toDestPath[_pathIndex];
            DetermineInitialOrientation(_toDestPath[0].Value);
        }

        public void MoveUnitsToDestination(ICanBurn building, List<(int,int)?>? newToPath,List<(int,int)?>? newFromPath)
        {
            _pathIndex = 0;
            _toDestPath = newToPath;
            _fromDestPath = newFromPath;
            _dispatchedTo = building;
            _dispatchedTo.HasDispatchedTruck = true;
            _isOnDuty = true;
            FireTruckLocation = _toDestPath[_pathIndex];
            DetermineInitialOrientation(_toDestPath[0].Value);
        }

        private void DetermineInitialOrientation((int,int) firstPoint)
        {
            if (FireTruckLocation is null)
            {
                return;
            }
            if (this.Location.X > firstPoint.Item2)
            {
                _nextOrientation = Orientation.West;
            }
            else if (this.Location.X + this.SizeX - 1 < firstPoint.Item2)
            {
                _nextOrientation = Orientation.East;
            }
            else if (this.Location.Y < firstPoint.Item1)
            {
                _nextOrientation = Orientation.North;
            }
            else
            {
                _nextOrientation = Orientation.South;
            }
        }

        private void DetermineNextOrientation((int,int) nextPoint, (int,int) currentPoint)
        {
            if (currentPoint.Item2 > nextPoint.Item2)
            {
                _nextOrientation = Orientation.West;
            }
            else if (currentPoint.Item2 < nextPoint.Item2)
            {
                _nextOrientation = Orientation.East;
            }
            else if (currentPoint.Item1 < nextPoint.Item1)
            {
                _nextOrientation = Orientation.North;
            }
            else
            {
                _nextOrientation = Orientation.South;
            }
        }
        private void PutOut(ICanBurn building)
        {
            if (building is null)
            {
                //Building has burned down by the time the firetruck got there
                return;
            }
            building.IsOnFire = false;
            building.IsOnFireSince = null;

        }

        public enum Orientation
        {
            North,
            East,
            South,
            West
        }
    }
}

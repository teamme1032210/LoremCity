﻿using Backend.Buildings.Interfaces;
using Backend.GlobalEnums;
using Backend.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings
{
    [Serializable]
    public class PowerPlant : Building, ICanBurn, IExtraInfo, IPowerBuilding
    {
        public DateResponse BuildDate {get;}

        public ExpenseType MaintenanceExpenseType {get;}

        public PowerPlant(Point location, DateResponse buildDate) : base(location, 2, 2) 
        { 
            BuildDate = buildDate;
            MaintenanceExpenseType = ExpenseType.PowerplantMaintenance;
        }

        public int Capacity
        {
            get => 100;
        }

        public bool IsOnFire { get; set; }
        public DateTime? IsOnFireSince { get; set; }
        public bool HasDispatchedTruck { get; set; }
        
        public int PowerNeeded { get; }
        public int PowerConsumption { get; set; }
        public PowerNetwork? Network { get; set; }
        public bool Walked { get; set; } = false;
        public new int GetPointCount() => base.GetPointCount;
        public new Point GetPoint(int index) => base.GetPoint(index);
    }
}

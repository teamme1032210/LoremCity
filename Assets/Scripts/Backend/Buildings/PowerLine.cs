﻿using System;
using Backend.Buildings.Interfaces;
using Backend.GlobalEnums;
using Backend.Utilities;
using System.Drawing;

namespace Backend.Buildings
{
    [Serializable]
    public class PowerLine : Building, IExtraInfo, IPowerBuilding
    {
        public DateResponse BuildDate {get;}
        public ExpenseType MaintenanceExpenseType {get;}

        public PowerNetwork? Network { get; set; }
        public bool Walked { get; set; } = false;
        public new int GetPointCount() => base.GetPointCount;
        public new Point GetPoint(int index) => base.GetPoint(index);

        public PowerLine(Point location,DateResponse buildDate) : base(location, 1, 1)
        {
            BuildDate = buildDate;
            MaintenanceExpenseType = ExpenseType.PowerLineMaintenance;
        }
    }
}

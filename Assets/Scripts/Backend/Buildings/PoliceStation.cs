﻿using Backend.Buildings.Interfaces;
using Backend.Utilities;
using Backend.GlobalEnums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings
{
    [Serializable]
    public class PoliceStation : SpecialBuilding, INeedPower, ICanBurn
    {
        public int PowerNeeded { get => 15; }
        public PowerNetwork? Network { get; set; }
        public bool Walked { get; set; } = false;
        public new int GetPointCount() => base.GetPointCount;
        public new Point GetPoint(int index) => base.GetPoint(index);
        public PoliceStation(Point location, DateResponse buildDate)
            : base(location, 2, 2, buildDate, ExpenseType.PoliceStationMaintenance) {  }

        public bool IsOnFire { get; set; }
        public DateTime? IsOnFireSince { get; set; }
        public bool HasDispatchedTruck { get; set; }
    }
}

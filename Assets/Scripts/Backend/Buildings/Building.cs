﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings
{
    [Serializable]
    public abstract class Building
    {
        private Point _location;
        private int _sizeX;
        private int _sizeY;

        public Point Location
        {
            get { return _location; }
        }

        public int SizeX
        {
            get { return _sizeX; }
        }

        public int SizeY
        {
            get { return _sizeY; }
        }
        
        public int GetPointCount => SizeX * SizeY;

        public Point GetPoint(int index) {
            if (index < 0 || index >= GetPointCount)
                throw new ArgumentOutOfRangeException(index.ToString());

            return new Point(Location.X + (index % SizeX), Location.Y + (index / SizeX));
        }

        internal Building(Point location, int sizex, int sizey)
        {
            _location = location;
            _sizeX = sizex;
            _sizeY = sizey;
        }
    }
}

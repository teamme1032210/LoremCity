﻿using Backend.Buildings.Interfaces;
using Backend.Utilities;
using Backend.GlobalEnums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings
{
    [Serializable]
    public class Forest : Building, IExtraInfo
    {
        private DateResponse _plantDate;
        private int _forestLevel;

        public Forest(Point location, DateResponse plantDate) : base(location,1,1)
        {
            _plantDate = plantDate;
            _forestLevel = 0;
            MaintenanceExpenseType = ExpenseType.ForestMaintenance;
        }

        public DateResponse BuildDate
        {
            get { return _plantDate; }
        }

        public ExpenseType MaintenanceExpenseType {get;}

        public int ForestLevel
        {
            get 
            {
                return _forestLevel;
            }
            set
            {
                _forestLevel = value;
            }
        }
    }
}

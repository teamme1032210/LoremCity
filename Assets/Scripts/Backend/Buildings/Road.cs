﻿using Backend.Buildings.Interfaces;
using Backend.Utilities;
using Backend.GlobalEnums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Buildings
{
    [Serializable]
    public class Road : Building, IExtraInfo
    {
        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public DateResponse BuildDate {get;}
        
        public ExpenseType MaintenanceExpenseType {get;}

        public Road(Point location, DateResponse builDate)
            : base(location, 1, 1) 
            { 
                _isActive = false; 
                BuildDate = builDate;
                MaintenanceExpenseType = ExpenseType.RoadMaintenance;
            }
    }
}

﻿namespace Backend.Zones
{
    public enum ZoneType
    {
        Special = 0,
        Residential = 1,
        Service = 3,
        Industrial = 2,
    }
}

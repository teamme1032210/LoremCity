﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Buildings;
using Backend.Buildings.Interfaces;
using Backend.Utilities;

namespace Backend.Zones
{
    [Serializable]
    public class Zone : INeedPower
    {
        private ZoneType _zoneType;
        private List<Point> _cells;
        private int _currentLevel;
        private bool _isActive;
        private bool _hasFreeSpace;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public int PowerNeeded
        {
            get => _cells.Count;
        }
        public PowerNetwork? Network { get; set; }
        public bool Walked { get; set; } = false;
        public int GetPointCount() => _cells.Count;
        public Point GetPoint(int index) => _cells[index];

        public bool HasFreeSpace
        {
            get { return _hasFreeSpace; }
            set { _hasFreeSpace = value; }
        }

        public Zone(ZoneType zoneType, List<Point> cells, List<ZoneBuilding> buildings)
        {
            _zoneType = zoneType;
            _cells = cells;
            _isActive = false;
            _currentLevel = 1;
            _hasFreeSpace = true;
        }

        public ZoneType ZoneType
        {
            get { return _zoneType; }
            set { _zoneType = value; }
        }
        public List<Point> Cells
        {
            get { return _cells; }
        }

        

        public int CurrentLevel
        {
            get { return _currentLevel; } set { _currentLevel = value; }
        }

        public int Capacity
        {
            get
            {
                return _cells.Count * (_currentLevel) * MapCell.DefaultCapacity;
            }
        }
    }
}

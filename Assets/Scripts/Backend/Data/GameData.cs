﻿using Backend.Utilities;
using Backend.GlobalEnums;
using Backend.Buildings;
using Backend.Inhabitants;
using Backend.Zones;
using Backend.Utilities.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Runtime.Serialization;
using Random = System.Random;
using Backend.Buildings.Interfaces;
using Util;
using Backend.Utilities.Enums;
using UnityEngine;

namespace Backend.Data
{
    [Serializable]
    public class GameData : ISerializable
    {
        
        public const int POWERLINE_REACH = 3;
        private List<Point> _powerLineReachCoords;

        private readonly List<Point> _powerBuildingReachCoords = new List<Point>
        {
            new Point(1, 0),
            new Point(0, 1),
            new Point(-1, 0),
            new Point(0, -1)
        };
        
        private int _salaryDeductionPercentage;
        private string _cityName;
        private int _mapWidth;
        private int _mapHeight;
        private bool _isPaused;
        private GameSpeed _gameSpeed;
        private int _balance;
        private readonly double _secondaryEducationRatio = 0.6;
        private readonly double _higherEducationRatio = 0.2;
        private readonly int _initialInhabitantsCount = 10;
        private DateTime _gameTime;
        [NonSerialized]
        private FactoryLayer _factoryLayer;
        private HWMat<MapCell> _cellData;
        public readonly Point firstPieceOfRoad = new Point(0, 10);
        private readonly int _secondarySchoolStudentCount = 10;
        private readonly int _universityStudentCount = 20;
        private readonly int _industrialDissatisfactionRadius = 5;
        private readonly int _workplaceTaxPerPerson = 100;
        private int _dissatisfactionCounter = 0;
        private int _debtDaysCounter = 0;
        private double _globalsatisfaction = 1;
        private bool _firstGroupOfInhabitantsArrived;
        public static Dictionary<(int, int), List<Variation>> HomeVariationStorage = new Dictionary<(int, int), List<Variation>>()
        {
            { (1,1), new List<Variation>() { new Variation(1, 1, 0), new Variation(1, 1, 1), new Variation(1, 1, 2), } },
            { (1,2), new List<Variation>() { new Variation(1, 2, 3), new Variation(1, 2, 4), new Variation(1, 2, 5), } },
            { (2,1), new List<Variation>() { new Variation(2, 1, 6), new Variation(2, 1, 7), new Variation(2, 1, 8), } },
            { (2,2), new List<Variation>() { new Variation(2, 2, 9), new Variation(2, 2, 10), new Variation(2, 2, 11), }  },
            { (2,3), new List<Variation>() { new Variation(2, 3, 12), new Variation(2, 3, 13), new Variation(2, 3, 14), } },
            { (3,2), new List<Variation>() { new Variation(3, 2, 15), new Variation(3, 2, 16), new Variation(3, 2, 17), } },
            { (3,3), new List<Variation>() { new Variation(3, 3, 18), new Variation(3, 3, 19), new Variation(3, 3, 20), } },
        };

        public static Dictionary<(int, int), List<Variation>> ServiceWorkplaceVariationStorage = new Dictionary<(int, int), List<Variation>>()
        {
            { (1,1), new List<Variation>() { new Variation(1, 1, 0), new Variation(1, 1, 1), new Variation(1, 1, 2), } },
            { (1,2), new List<Variation>() { new Variation(1, 2, 3), new Variation(1, 2, 4), new Variation(1, 2, 5), } },
            { (2,1), new List<Variation>() { new Variation(2, 1, 6), new Variation(2, 1, 7), new Variation(2, 1, 8), } },
            { (2,2), new List<Variation>() { new Variation(2, 2, 9), new Variation(2, 2, 10), new Variation(2, 2, 11), }  },
            { (2,3), new List<Variation>() { new Variation(2, 3, 12), new Variation(2, 3, 13), new Variation(2, 3, 14), } },
            { (3,2), new List<Variation>() { new Variation(3, 2, 15), new Variation(3, 2, 16), new Variation(3, 2, 17), } },
            { (3,3), new List<Variation>() { new Variation(3, 3, 18), new Variation(3, 3, 19), new Variation(3, 3, 20), } },
        };

        public static Dictionary<(int, int), List<Variation>> IndustrialWorkplaceVariationStorage = new Dictionary<(int, int), List<Variation>>()
        {
            { (1,1), new List<Variation>() { new Variation(1, 1, 0), new Variation(1, 1, 1), new Variation(1, 1, 2), } },
            { (1,2), new List<Variation>() { new Variation(1, 2, 3), new Variation(1, 2, 4), new Variation(1, 2, 5), } },
            { (2,1), new List<Variation>() { new Variation(2, 1, 6), new Variation(2, 1, 7), new Variation(2, 1, 8), } },
            { (2,2), new List<Variation>() { new Variation(2, 2, 9), new Variation(2, 2, 10), new Variation(2, 2, 11), }  },
            { (2,3), new List<Variation>() { new Variation(2, 3, 12), new Variation(2, 3, 13), new Variation(2, 3, 14), } },
            { (3,2), new List<Variation>() { new Variation(3, 2, 15), new Variation(3, 2, 16), new Variation(3, 2, 17), } },
            { (3,3), new List<Variation>() { new Variation(3, 3, 18), new Variation(3, 3, 19), new Variation(3, 3, 20), } },
        };

        private List<SecondarySchool> _secondarySchools;
        private List<University> _universities;
        private List<Stadium> _stadiums;
        private List<PoliceStation> _policeStations;
        private List<FireStation> _fireStations;
        private List<Road> _roads;
        private List<Forest> _forests;
        private List<PowerLine> _powerLines;
        private List<PowerPlant> _powerPlants;
        private List<Inhabitant> _inhabitants;
        private List<Zone> _zones;
        private List<ZoneBuilding> _zoneBuildings;
        private List<Inhabitant> _homelessInhabitants;
        private List<Inhabitant> _worklessInhabitants;
        private List<PowerNetwork> _powerNetworks;

        [field: NonSerialized]
        public event EventHandler? MapHasChanged;
        [field: NonSerialized]
        public event EventHandler? NewZoneBuildingWasBuilt;
        [field: NonSerialized]
        public event EventHandler<Point>? BuildingCaughtFire;
        [field: NonSerialized]
        public event EventHandler? BalanceHasChanged;
        [field: NonSerialized]
        public event EventHandler? FireWasExtinguised;
        [field: NonSerialized]
        public event EventHandler<Road>? RoadDeletionConflictEvent;
        [field: NonSerialized]
        public event EventHandler? ZoneBuildingDeletionConflictEvent;
        [field: NonSerialized]
        public event EventHandler? GameOverEvent;

        public float IndustrialServiceRatio
        {
            get; set;
        }

        public List<Inhabitant> HomelessInhabitants
        {
            get { return _homelessInhabitants; }
        }

        public List<Inhabitant> WorklessInhabitants
        {
            get { return _worklessInhabitants; }
        }

        public string CityName
        {
            get { return _cityName; }
        }

        public int MapHeight
        {
            get { return _mapHeight; }
        }
        public int MapWidth
        {
            get { return _mapWidth; }
        }
        public DateTime GameTime
        {
            get { return _gameTime; }
        }

        public bool IsPaused
        {
            get { return _isPaused; }
            set { _isPaused = value; }
        }

        public GameSpeed GameSpeed
        {
            get { return _gameSpeed; }
            set { _gameSpeed = value; }
        }

        public int Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }
        public double SecondaryEducationRatio
        {
            get { return _secondaryEducationRatio; }
        }
        public double HigherEducationRatio
        {
            get { return _higherEducationRatio; }
        }

        public int SalaryDeductionPercentage
        {
            get {return _salaryDeductionPercentage;}
            set
            {
                if (!(value>=0 && value<=100))
                {
                    throw new InvalidOperationException("Invalid percentage!");
                }
                _salaryDeductionPercentage = value;
            }
        }

        public int Satisfaction
        {
            get;
        }

        public int TotalPowerNeeded
        {
            //Depends on the number of buidings that need/use electricity
            get;
        }

        public int TotalPowerCapacity
        {
            //Calculated from the number of powerplants
            get;
        }

        public FactoryLayer FactoryLayer
        {
            get { return _factoryLayer; }
        }

        public List<SecondarySchool> SecondarySchools
        {
            get { return _secondarySchools; }
        }

        public List<University> Universities
        {
            get { return _universities; }
        }

        public List<Stadium> Stadiums
        {
            get { return _stadiums; }
        }

        public List<PoliceStation> PoliceStations
        {
            get { return _policeStations; }
        }

        public List<FireStation> FireStations
        {
            get { return _fireStations; }
        }

        public List<Road> Roads
        {
            get { return _roads; }
        }

        public List<Forest> Forests
        {
            get { return _forests; }
        }

        public List<PowerLine> PowerLines
        {
            get { return _powerLines; }
        }

        public List<PowerPlant> PowerPlants
        {
            get { return _powerPlants; }
        }

        public List<Inhabitant> Inhabitants
        {
            get { return _inhabitants; }
        }

        public List<Zone> Zones
        {
            get { return _zones; }
            set { _zones = value; }
        }

        public List<ZoneBuilding> ZoneBuildings
        {
            get { return _zoneBuildings; }
        }

        public HWMat<MapCell> CellData
        {
            get { return _cellData; }
            set { _cellData = value; }
        }
        
        public List<PowerNetwork> PowerNetworks
        {
            get { return _powerNetworks; }
        }

        public double GlobalSatisfaction
        { 
            get { return _globalsatisfaction; }
            set { _globalsatisfaction = value; }
        }

        public GameData(int mapHeight, int mapWidth, int gameSpeed, DateTime gameTime, string cityName)
        {
            _salaryDeductionPercentage = 27;
            _isPaused = false;
            _balance = 2000;
            _cityName = cityName;
            _mapHeight = mapHeight;
            _mapWidth = mapWidth;
            _gameSpeed = (GameSpeed)gameSpeed;
            _gameTime = gameTime;
            _cellData = new HWMat<MapCell>(mapHeight, mapWidth);
            _stadiums = new List<Stadium>();
            _secondarySchools = new List<SecondarySchool>();
            _universities = new List<University>();
            _policeStations = new List<PoliceStation>();
            _fireStations = new List<FireStation>();
            _roads = new List<Road>();
            _roads.Add(new Road(firstPieceOfRoad,GetGameDate()));
            _forests = new List<Forest>();
            _powerLines = new List<PowerLine>();
            _powerPlants = new List<PowerPlant>();
            _powerNetworks = new List<PowerNetwork>();
            _inhabitants = new List<Inhabitant>();
            _zones = new List<Zone>();
            _zoneBuildings = new List<ZoneBuilding>();
            _factoryLayer = new FactoryLayer(this);
            _factoryLayer.TransferEvent += OnTransferEvent;
            _factoryLayer.IntTransferEvent += OnIntTransferEvent;
            _factoryLayer.FireWasExtingushed += OnFireExtinguished;
            _factoryLayer.RoadDeletionConflictEvent += OnRoadDeletionConflictEvent;
            _factoryLayer.ZoneBuildingDeletionConflictEvent += OnZoneBuildingDeletionConflictEvent;
            _homelessInhabitants = new List<Inhabitant>();
            _worklessInhabitants = new List<Inhabitant>();
            _firstGroupOfInhabitantsArrived = false;
            IndustrialServiceRatio = 1;
            CalculatePowerLineReachCoordinates();
        }

        public DateTime GetDate()
        {
            return _gameTime;
        }

        private void SetIndustrialDissatisfaction(Building building)
        {
            for (int i=building.Location.Y-_industrialDissatisfactionRadius; i<building.Location.Y + building.SizeY + _industrialDissatisfactionRadius; i++)
            {
                for (int j=building.Location.X-_industrialDissatisfactionRadius; j<building.Location.X + building.SizeX + _industrialDissatisfactionRadius; j++)
                {
                    if (i>=0 && i<_mapHeight && j>=0 && j<_mapWidth)
                    {
                          _cellData[i, j].HasIndustrialBuildingNearby = true;
                    }
                }
            }
        }

        public void AdjustSatisfactionAndForestGrowth()
        {
            for (int i=0; i<_mapHeight; i++)
            {
                for (int j=0; j<_mapWidth; j++)
                {
                    _cellData[i, j].CanSeeForest = false;
                    _cellData[i, j].WillSeeForestIfBuildsHere = false;
                    _cellData[i, j].TotalLevelOfForestsInSight = 0;
                    _cellData[i, j].HasIndustrialBuildingNearby = false;
                }
            }
            ZoneBuildings.Where(building => building is IndustrialWorkplace).ToList().ForEach(building => SetIndustrialDissatisfaction(building));
            foreach (Forest forest in _forests)
            {
                if (forest.BuildDate.Year != _gameTime.Year && forest.ForestLevel < 10 && forest.BuildDate.Year == _gameTime.Year && forest.BuildDate.Month == _gameTime.Month && forest.BuildDate.Day == _gameTime.Day)
                {
                    forest.ForestLevel++;
                    _balance -= GetExpense(ExpenseType.ForestMaintenance);
                    BalanceHasChanged!.Invoke(this, EventArgs.Empty);
                }
                int x = forest.Location.X;
                int y = forest.Location.Y;
                StepFunction(forest,x, y, 1, 0);
                StepFunction(forest,x, y, 0, 1);
                StepFunction(forest,x, y, -1, 0);
                StepFunction(forest,x, y, 0, -1);
                ForestNeighbourCheck(x, y);
            }
        }
        private void ForestNeighbourCheck(int x, int y)
        {
            if ((x-1)>=0 && (x+1)>=0 && (x - 1) < _mapWidth && (x + 1) < _mapWidth )
            {
                HandleForestBetweenIndustrialAndResidential(x + 1, y, x - 1, y);
                HandleForestBetweenIndustrialAndResidential(x - 1, y, x + 1, y);
            }
            if ((y - 1) >= 0 && (y + 1) >= 0 && (y - 1) < _mapHeight && (y + 1) < _mapHeight)
            {
                HandleForestBetweenIndustrialAndResidential(x, y - 1, x, y + 1);
                HandleForestBetweenIndustrialAndResidential(x, y + 1, x, y - 1);
            }
        }
        private void HandleForestBetweenIndustrialAndResidential(int x1, int y1, int x2, int y2)
        {
            if (_cellData[y1,x1].Zone.ZoneType==ZoneType.Industrial && _cellData[y1,x1].Building is not null &&_cellData[y2,x2].Zone.ZoneType==ZoneType.Residential)
            {
                _cellData[y2, x2].IndustrialDissatisfactionReducedByForest = true;
            }
        }
        private void StepFunction(Forest forest, int x, int y, int xStep, int yStep)
        {
            x += xStep;
            y += yStep;
            int stepCount = 1;
            while (x >= 0 && x < _mapWidth && y >= 0 && y < _mapHeight && stepCount <= 3)
            {
                if (_cellData[y, x].Building is not null && _cellData[y, x].Zone.ZoneType != ZoneType.Residential)
                {
                    if (!(_cellData[y, x].Zone.ZoneType == ZoneType.Special && _cellData[y,x].Building is Road))
                    {
                        break;
                    }
                }
                else if (_cellData[y, x].Building is not null && _cellData[y, x].Zone.ZoneType == ZoneType.Residential)
                {
                    _cellData[y, x].CanSeeForest = true;
                    _cellData[y, x].TotalLevelOfForestsInSight += (forest.ForestLevel + 1);
                    break;
                }
                else if (_cellData[y,x].Building is null && _cellData[y,x].Zone.ZoneType == ZoneType.Residential)
                {
                    _cellData[y, x].WillSeeForestIfBuildsHere = true;
                }
                x += xStep;
                y += yStep;
                stepCount++;
            }
        }

        private void CalculatePowerLineReachCoordinates()
        {
            _powerLineReachCoords = new List<Point>();
            for (int i = -POWERLINE_REACH; i <= POWERLINE_REACH; i++)
            {
                for(int j = -POWERLINE_REACH; j <= POWERLINE_REACH; j++)
                {
                    if (i == 0 && j == 0)
                    {
                        continue;
                    }

                    if (i * i + j * j <= POWERLINE_REACH * POWERLINE_REACH)
                    {
                        _powerLineReachCoords.Add(new Point(i, j));
                    }
                }
            }
        }

        public void StartNewGame()
        {
            Zone specialZone = new Zone(ZoneType.Special, new List<Point>(), new List<ZoneBuilding>());
            Zones.Add(specialZone);
            List<Point> AllPoints = new List<Point>();
            for (int i = 0; i < _mapHeight; i++)
            {
                for (int j = 0; j < _mapWidth; j++)
                {
                    specialZone.Cells.Add(new Point(j, i));
                    _cellData[i, j] = new MapCell();
                    _cellData[i, j].Zone = specialZone;
                }
            }
            _cellData[firstPieceOfRoad.Y, firstPieceOfRoad.X].Building = _roads[0];
            _roads[0].IsActive = true;

            HashSet<(int, int)> Forests = new HashSet<(int, int)>();
            Random rnd = new Random();
            for (int i = 0; i < _mapWidth * _mapHeight/10; i++)
            {
                int x = rnd.Next(0, _mapWidth);
                int y = rnd.Next(0, _mapHeight);
                if (x != firstPieceOfRoad.X && y != firstPieceOfRoad.Y)
                {
                    Forests.Add((x, y));
                }
            }
            foreach ((int, int) tpl in Forests)
            {
                Forest forest = new Forest(new Point(tpl.Item1, tpl.Item2), new DateResponse(_gameTime.Year-10, _gameTime.Month, _gameTime.Day));
                forest.ForestLevel=10;
                _forests.Add(forest);
                CellData[tpl.Item2, tpl.Item1].Building = forest;
            }

        }

        public void UpdatePowerNetworks()
        {
            HashSet<Building> allBuildings = GetAllBuildings();
            foreach (Building building in allBuildings)
            {
                if (building is IPowerBuilding powerBuilding)
                {
                    PowerHelper.ResetWaked(powerBuilding);
                    if (powerBuilding is INeedPower consumer) {
                        consumer.Network = null;
                    } else if (powerBuilding is PowerLine powerLine)
                    {
                        powerLine.Network = null;
                    } else if (powerBuilding is PowerPlant powerPlant)
                    {
                        powerPlant.Network = null;
                    }
                }
            }

            foreach (Zone zone in _zones) {
                PowerHelper.ResetWaked(zone);
                zone.Network = null;
            }
            
            _powerNetworks.Clear();

            for (int i = 0; i < _powerPlants.Count; i++)
            {
                if (!_powerPlants[i].Walked)
                {
                    PowerNetwork network = new PowerNetwork();
                    WalkPowerNetwork(network, _powerPlants[i]);
                    _powerNetworks.Add(network);
                }
            }
        }

        private void WalkPowerNetwork(PowerNetwork currentNetwork, IPowerBuilding current)
        {
            if (!current.Walked) {
                current.Walked = true;
                if (current is PowerLine line) {
                    line.Network = currentNetwork;
                    currentNetwork.AddLine(line);
                }
                else if (current is PowerPlant plant) {
                    plant.Network = currentNetwork;
                    currentNetwork.AddPlant(plant);
                }
                else if (current is INeedPower consumer) {
                    consumer.Network = currentNetwork;
                    currentNetwork.AddConsumer(consumer);
                }
                else {
                    throw new Exception(
                        "Tervezési hiba");
                }

                for (int i = 0; i < current.GetPointCount(); i++) {
                    Point currentBuildingPoint = current.GetPoint(i);
                    for (int j = 0; j < _powerBuildingReachCoords.Count; j++) {
                        Point currentPoint = new Point(
                            currentBuildingPoint.X + _powerBuildingReachCoords[j].X,
                            currentBuildingPoint.Y + _powerBuildingReachCoords[j].Y
                        );
                        if (currentPoint.X >= 0 && currentPoint.X < _mapWidth &&
                            currentPoint.Y >= 0 && currentPoint.Y < _mapHeight) {
                            if (
                                _cellData[currentPoint.Y, currentPoint.X].Zone.ZoneType != ZoneType.Special &&
                                !_cellData[currentPoint.Y, currentPoint.X].Zone.Walked
                            ) {
                                WalkPowerNetwork(currentNetwork, _cellData[currentPoint.Y, currentPoint.X].Zone);
                            }
                            else if (
                                _cellData[currentPoint.Y, currentPoint.X].Building is IPowerBuilding other &&
                                !other.Walked
                            ) {
                                WalkPowerNetwork(currentNetwork, other);
                            }
                        }
                    }
                }

                if (current is PowerLine currentLine) {
                    for (int i = 0; i < _powerLineReachCoords.Count; i++) {
                        Point currentPoint = new Point(
                            currentLine.Location.X + _powerLineReachCoords[i].X,
                            currentLine.Location.Y + _powerLineReachCoords[i].Y
                        );
                        if (currentPoint.X >= 0 && currentPoint.X < _mapWidth &&
                            currentPoint.Y >= 0 && currentPoint.Y < _mapHeight &&
                            _cellData[currentPoint.Y, currentPoint.X].Building is PowerLine otherLine &&
                            !otherLine.Walked
                        ) {
                            WalkPowerNetwork(currentNetwork, otherLine);
                        }
                    }
                }
            }
        }

        private void OnZoneBuildingDeletionConflictEvent(object sender, EventArgs e)
        {
            ZoneBuildingDeletionConflictEvent?.Invoke(this,EventArgs.Empty);
        }

        private void OnRoadDeletionConflictEvent(object sender, Road e)
        {
            RoadDeletionConflictEvent?.Invoke(this, e);
        }

        private void OnFireExtinguished(object sender, EventArgs e)
        {
            FireWasExtinguised?.Invoke(this, EventArgs.Empty);
        }

        private void OnTransferEvent(object? sender, TransferEventArgs e)
        {
            _balance -= GetExpense(e.ExpenseType);
            BalanceHasChanged?.Invoke(this,EventArgs.Empty);
        }

        private void OnIntTransferEvent(object? sender, int e)
        {
            _balance -= e;
            BalanceHasChanged?.Invoke(this,EventArgs.Empty);
        }

        public int GetExpense(ExpenseType e)
        {
            switch (e)
            {
                case ExpenseType.StadiumIsBuilt:
                    return 500;

                case ExpenseType.StadiumMaintenance:
                    return 100;

                case ExpenseType.StadiumIsDestroyed:
                    return -100;

                case ExpenseType.PoliceStationIsBuilt:
                    return 500;

                case ExpenseType.PoliceStationMaintenance:
                    return 100;

                case ExpenseType.PoliceStationIsDestroyed:
                    return -100;

                case ExpenseType.FireStationIsBuilt:
                    return 500;

                case ExpenseType.FireStationMaintenance:
                    return 100;

                case ExpenseType.FireStationIsDestroyed:
                    return -100;

                case ExpenseType.RoadIsBuilt:
                    return 10;

                case ExpenseType.RoadMaintenance:
                    return 5;

                case ExpenseType.RoadIsDestroyed:
                    return -5;

                case ExpenseType.SchoolIsBuilt:
                    return 500;

                case ExpenseType.SchoolMaintenance:
                    return 100;

                case ExpenseType.SchoolIsDestroyed:
                    return -100;

                case ExpenseType.UniversityIsBuilt:
                    return 700;

                case ExpenseType.UniversityMaintenance:
                    return 150;

                case ExpenseType.UniversityIsDestroyed:
                    return -100;

                case ExpenseType.ForestIsCreated:
                    return 10;

                case ExpenseType.ForestMaintenance:
                    return 5;

                case ExpenseType.ForestIsDestroyed:
                    return -5;

                case ExpenseType.PowerplantIsBuilt:
                    return 1000;

                case ExpenseType.PowerplantIsDestroyed:
                    return -500;

                case ExpenseType.PowerLineIsBuilt:
                    return 10;

                case ExpenseType.PowerLineMaintenance:
                    return 5;

                case ExpenseType.PowerLineIsDestroyed:
                    return -5;

                case ExpenseType.Level1ResidentialFieldIsSet:
                    return 5;

                case ExpenseType.Level1IndustrialFieldIsSet:
                    return 5;

                case ExpenseType.Level1ServiceFieldIsSet:
                    return 5;

                case ExpenseType.Level1ResidentialSetBackToDefault:
                    return 5;

                case ExpenseType.Level1IndustrialSetBackToDefault:
                    return 5;

                case ExpenseType.Level1ServiceSetBackToDefault:
                    return 5;

                case ExpenseType.Level2ResidentialFieldIsSet:
                    return 10;

                case ExpenseType.Level2IndustrialFieldIsSet:
                    return 10;

                case ExpenseType.Level2ServiceFieldIsSet:
                    return 10;

                case ExpenseType.Level2ResidentialSetBackToDefault:
                    return 10;

                case ExpenseType.Level2IndustrialSetBackToDefault:
                    return 10;

                case ExpenseType.Level2ServiceSetBackToDefault:
                    return 15;

                case ExpenseType.Level3ResidentialFieldIsSet:
                    return 15;

                case ExpenseType.Level3IndustrialFieldIsSet:
                    return 15;

                case ExpenseType.Level3ServiceFieldIsSet:
                    return 15;

                case ExpenseType.Level3ResidentialSetBackToDefault:
                    return 15;

                case ExpenseType.Level3IndustrialSetBackToDefault:
                    return 15;

                case ExpenseType.Level3ServiceSetBackToDefault:
                    return 15;

                case ExpenseType.ResidentialUpgradedToLevel2:
                    return 10;
                
                case ExpenseType.ResidentialUpgradedToLevel3:
                    return 10;

                case ExpenseType.IndustrialUpgradedToLevel2:
                    return 10;
                
                case ExpenseType.IndustrialUpgradedToLevel3:
                    return 10;

                case ExpenseType.ServiceUpgradedToLevel2:
                    return 10;
                
                case ExpenseType.ServiceUpgradedToLevel3:
                    return 10;

                case ExpenseType.Compensation:
                    return 100;
                default:
                    return 0;
            }
        }

        public DateResponse Tick()
        {
            if (!_isPaused)
            {
                _gameTime = _gameTime.AddDays(1);
                int month = _gameTime.Month;
                if ((month % 3 == 0) && _gameTime.Day == 1)
                {
                    CollectTax();
                    DeductYearlyMaintenance();
                    BalanceHasChanged?.Invoke(this, EventArgs.Empty);
                }
                if (_gameTime.Month % 4 == 0 && _gameTime.Day % 7 == 0)
                {
                    SetRandomBuildingOnFire();
                }
                if (month == 9 && _gameTime.Day == 1)
                {
                    HandlePopulationAging();
                    HandleFreeSpaceInZones();
                    HandleEducation();
                }
                int serviceCount = 0;
                int industrialCount = 0;
                ZoneBuildings.ForEach(building =>
                {
                    if (building is IndustrialWorkplace)
                    {
                        for (int i=building.Location.Y; i<building.Location.Y + building.SizeY; i++)
                        {
                            for (int j=building.Location.X; j<building.Location.X + building.SizeX; j++)
                            {
                                industrialCount += _cellData[i, j].RelatedInhabitants.Count;
                            }
                        }
                    }
                    else if (building is ServiceWorkplace)
                    {
                        for (int i = building.Location.Y; i < building.Location.Y + building.SizeY; i++)
                        {
                            for (int j = building.Location.X; j < building.Location.X + building.SizeX; j++)
                            {
                                serviceCount += _cellData[i, j].RelatedInhabitants.Count;
                            }
                        }
                    }
                });
                if (serviceCount>0 && industrialCount>0)
                {
                    IndustrialServiceRatio = serviceCount > industrialCount ? (float)industrialCount / (float)serviceCount : (float)serviceCount / (float)industrialCount;
                }
                else
                {
                    if (serviceCount == 0 && industrialCount == 0)
                    {
                        IndustrialServiceRatio = 1;
                    }
                    else
                    {
                        IndustrialServiceRatio = 1 / (serviceCount + industrialCount);
                    }
                }
                HandleBurningBuildings();
                Random rnd = new Random();
                _homelessInhabitants = _homelessInhabitants.OrderBy(x => rnd.Next()).ToList();
                _worklessInhabitants.RemoveAll(inhabitant => inhabitant.IsPensioner);
                _worklessInhabitants = _worklessInhabitants.OrderBy(x => rnd.Next()).ToList();
                FindHome(_homelessInhabitants);
                HandleFreeSpaceInZones();
                FindWorkplace(_worklessInhabitants);
                HandleFreeSpaceInZones();
                if (!_firstGroupOfInhabitantsArrived)
                {
                    for (int i=0; i<_initialInhabitantsCount;i++)
                    {
                        CreateNewInhabitant(rnd.Next(18, 21));
                        HandleFreeSpaceInZones();
                    }
                }
                else
                {
                    CreateNewInhabitant(rnd.Next(18, 61));
                    HandleFreeSpaceInZones();
                }

                _fireStations.ForEach(fs => fs.MoveOneUnitForward());
                RerouteUselessFiretrucks();
                AdjustSatisfactionAndForestGrowth();
                double iwratiomultiplier = (1 - (UnityEngine.Mathf.Round(IndustrialServiceRatio))) / 50;
                double workplaceDistanceMultiplier;
                if (Balance < 0)
                {
                    _debtDaysCounter++;
                }
                else
                {
                    _debtDaysCounter = 0;
                }
                _inhabitants.ForEach(inhabitant =>
                {
                    inhabitant.Satisfaction = 0;
                });
                for (int i = 0; i<_mapHeight;i++)
                {
                    for (int j=0; j<_mapWidth;j++)
                    {
                        if (_cellData[i,j].Zone.ZoneType is ZoneType.Residential)
                        {
                            double electricityPerkScoreDeduction = PowerHelper.HasEnoughPower(_cellData[i, j].Zone) ? 0 : 0.05;
                            double satisfactionScore = Math.Max(0,Math.Min(0.5, 0.3 + CalculateSatisfactionPerHomeCell(_cellData[i, j])) - iwratiomultiplier - ((double)SalaryDeductionPercentage/100) * 0.01 - UnityEngine.Mathf.Round((float)_debtDaysCounter/30) * Math.Abs(Balance) * 0.0008 - HomelessInhabitants.Count * 0.0005 - WorklessInhabitants.Count * 0.0005 - ((double)_cellData[i, j].RelatedInhabitants.Count / (double)_cellData[i,j].Zone.CurrentLevel * MapCell.DefaultCapacity) * 0.0004 - electricityPerkScoreDeduction);
                            _cellData[i, j].RelatedInhabitants.ForEach(inhabitant =>
                            { 
                                if (inhabitant.MadForWorkplaceLossDays>0)
                                {
                                    if (inhabitant.WorkPlace is not null)
                                    {
                                        inhabitant.MadForWorkplaceLossDays--;
                                    }
                                    if (inhabitant.WorkPlace is not null && inhabitant.Home is not null)
                                    {

                                        workplaceDistanceMultiplier = inhabitant.DistanceFromWorkplace <= 5 ? 1 : ((double)inhabitant.DistanceFromWorkplace*0.002);
                                        satisfactionScore = Math.Max(0, satisfactionScore - workplaceDistanceMultiplier);
                                    }
                                    satisfactionScore = Math.Max(0,satisfactionScore-(((double)inhabitant.MadForWorkplaceLossDays)/30)*0.01);
                                }
                                inhabitant.Satisfaction += satisfactionScore;
                            });
                        }
                        if (_cellData[i, j].Zone.ZoneType is ZoneType.Service || _cellData[i,j].Zone.ZoneType is ZoneType.Industrial)
                        {
                            double satisfactionScore = Math.Max(0,Math.Min(0.5, 0.3 + CalculateSatisfactionPerWorkCell(_cellData[i, j]) - iwratiomultiplier - ((double)SalaryDeductionPercentage / 100) * 0.01 - UnityEngine.Mathf.Round((float)_debtDaysCounter / 30) * Math.Abs(Balance) * 0.0008 - HomelessInhabitants.Count * 0.0005 - WorklessInhabitants.Count * 0.0005 - ((double)_cellData[i, j].RelatedInhabitants.Count / (double)_cellData[i, j].Zone.CurrentLevel * MapCell.DefaultCapacity) * 0.0004));
                            _cellData[i, j].RelatedInhabitants.ForEach(inhabitant =>
                            {
                                if (inhabitant.MadForHouseLossDays > 0)
                                {
                                    if (inhabitant.Home is not null)
                                    {
                                        inhabitant.MadForHouseLossDays--;
                                    }
                                    satisfactionScore = Math.Max(0, satisfactionScore - (((double)inhabitant.MadForHouseLossDays) / 30) * 0.01);
                                }
                                inhabitant.Satisfaction += satisfactionScore;
                            });
                        }
                    }
                }
                if (_inhabitants.Count > 0)
                {
                    var voters = _inhabitants.Where(
                        inhabitant => !(inhabitant.Home is null && inhabitant.WorkPlace is null));
                    if (voters.Count() > 0)
                    {
                        GlobalSatisfaction = Math.Min(1, voters.Average(inhabitant => inhabitant.Satisfaction));
                        if (GlobalSatisfaction < 0.15)
                        {
                            _dissatisfactionCounter++;
                        }
                        else
                        {
                            _dissatisfactionCounter = 0;
                        }
                    }
                    else
                    {
                        GlobalSatisfaction = 0;
                    }
                    if (_dissatisfactionCounter == 30)
                    {
                        GameOverEvent?.Invoke(this, EventArgs.Empty);
                    }
                }
                else
                {
                    if (_dissatisfactionCounter == 0)
                    {
                        GlobalSatisfaction = 1;
                    }
                    else
                    {
                        GlobalSatisfaction = 0;
                    }
                }

                var inhabitantsToDelete = new List<Inhabitant>();
                Inhabitants.ForEach(inhabitant =>
                {
                    inhabitant.SetDissatisfiedDays();
                    if (inhabitant.DissatisfiedDays >= 30)
                    {
                        inhabitantsToDelete.Add(inhabitant);
                    }
                });
                inhabitantsToDelete.ForEach(inhabitant => DeleteDissatisfiedInhabitant(inhabitant));
                
            }
            return GetGameDate();

        }
        private double CalculateSatisfactionPerHomeCell(MapCell cell)
        {
            double result = 0;
            if (cell.HasFirestationNearby)
            {
                result += 19;
            }
            if (cell.HasPolicestationNearby)
            {
                result += 19;
            }
            if (cell.HasStadiumNearby)
            {
                result += 29;
            }
            result += cell.TotalLevelOfForestsInSight;

            if (cell.HasIndustrialBuildingNearby)
            {
                result -= 30;
                if (cell.IndustrialDissatisfactionReducedByForest)
                {
                    result += 10;
                }
            }
            return Math.Min(0.2,Math.Max(0, result / 500));
        }
        private double CalculateSatisfactionPerWorkCell(MapCell cell)
        {
            double result = 0;
            if (cell.HasFirestationNearby)
            {
                result += 24;
            }
            if (cell.HasPolicestationNearby)
            {
                result += 30;
            }
            if (cell.HasStadiumNearby)
            {
                result += 46;
            }
            return Math.Min(0.2, Math.Max(0, result / 500));

        }
        private void FindWorkplace(List<Inhabitant> inhabitants)
        {
            Random rnd = new Random();
            if (inhabitants.Count == 0)
            {
                return;
            }
            List<(Zone,Point)> availableWorkplaces = new List<(Zone,Point)>();
            foreach(Zone zone in Zones.Where(zone => zone.HasFreeSpace && zone.IsActive && PowerHelper.HasEnoughPower(zone) && (zone.ZoneType is ZoneType.Service || zone.ZoneType is ZoneType.Industrial)))
            {
                foreach (Point cell in zone.Cells)
                {
                    if (CellData[cell.Y,cell.X].RelatedInhabitants.Count<CellData[cell.Y,cell.X].Zone.CurrentLevel*MapCell.DefaultCapacity)
                    {
                        availableWorkplaces.Add((zone,cell));
                    }   
                }
            }
            availableWorkplaces = availableWorkplaces.OrderBy(x => rnd.Next()).ToList();
            for (int i = 0; i < Math.Min(inhabitants.Count,availableWorkplaces.Count);i++)
            {
                if (_cellData[availableWorkplaces[i].Item2.Y,availableWorkplaces[i].Item2.X].Building is null)
                {
                    _factoryLayer.BuildRandomBuilding(availableWorkplaces[i].Item1,availableWorkplaces[i].Item2);
                }
                CellData[availableWorkplaces[i].Item2.Y,availableWorkplaces[i].Item2.X].RelatedInhabitants.Add(inhabitants.First());
                int sum = CellData[availableWorkplaces[i].Item2.Y, availableWorkplaces[i].Item2.X].Zone.Cells.Sum(x => _cellData[x.Y, x.X].RelatedInhabitants.Count);
                Inhabitant luckyInhabitant = inhabitants.First();
                luckyInhabitant.WorkPlace = availableWorkplaces[i].Item2;
                if (luckyInhabitant.Home is not null)
                {
                    luckyInhabitant.DistanceFromWorkplace = Math.Max(Math.Abs(luckyInhabitant.WorkPlace.Value.X - luckyInhabitant.Home.Value.X), Math.Abs(luckyInhabitant.WorkPlace.Value.Y - luckyInhabitant.Home.Value.Y));
                }
                inhabitants.RemoveAt(0);
            }
            NewZoneBuildingWasBuilt?.Invoke(this, EventArgs.Empty);
        }
        private void FindHome(List<Inhabitant> inhabitants)
        {
            Random rnd = new Random();
            if (inhabitants.Count==0)
            {
                return;
            }
            List<(Zone,Point)> availableHomes = new List<(Zone,Point)>();
            foreach(Zone zone in Zones.Where(zone => zone.HasFreeSpace && zone.IsActive && PowerHelper.HasEnoughPower(zone) && zone.ZoneType is ZoneType.Residential))
            {
                foreach (Point cell in zone.Cells)
                {
                    if (CellData[cell.Y,cell.X].RelatedInhabitants.Count<CellData[cell.Y,cell.X].Zone.CurrentLevel*MapCell.DefaultCapacity)
                    {
                        availableHomes.Add((zone,cell));
                    }   
                }
            }
            availableHomes = availableHomes.OrderBy(x => rnd.Next()).ToList();
            for (int i=0; i<Math.Min(inhabitants.Count,availableHomes.Count);i++)
            {
                if (_cellData[availableHomes[i].Item2.Y,availableHomes[i].Item2.X].Building is null)
                {
                    _factoryLayer.BuildRandomBuilding(availableHomes[i].Item1,availableHomes[i].Item2);
                }
                CellData[availableHomes[i].Item2.Y,availableHomes[i].Item2.X].RelatedInhabitants.Add(inhabitants.First());
                int sum = CellData[availableHomes[i].Item2.Y, availableHomes[i].Item2.X].Zone.Cells.Sum(x => _cellData[x.Y, x.X].RelatedInhabitants.Count);
                Inhabitant luckyInhabitant = inhabitants.First();
                luckyInhabitant.Home = availableHomes[i].Item2;
                if (luckyInhabitant.WorkPlace is not null)
                {
                    luckyInhabitant.DistanceFromWorkplace = Math.Max(Math.Abs(luckyInhabitant.WorkPlace.Value.X - luckyInhabitant.Home.Value.X), Math.Abs(luckyInhabitant.WorkPlace.Value.Y - luckyInhabitant.Home.Value.Y));
                }
                inhabitants.RemoveAt(0);
            }
            NewZoneBuildingWasBuilt?.Invoke(this, EventArgs.Empty);
        }

        private void CreateNewInhabitant(int age)
        {
            List<Zone> availableHomes = new List<Zone>();
            List<Zone> availableIndustrialWorkplaces = new List<Zone>();
            List<Zone> availableServicelWorkplaces = new List<Zone>();
            double workersinService = 0;
            double workersinIndustrial = 0;


            foreach(Zone zone in Zones.Where(zone => zone.HasFreeSpace && zone.IsActive && PowerHelper.HasEnoughPower(zone)))
            {
                
                if (zone.ZoneType is ZoneType.Special)
                {
                    continue;
                }
                else if (zone.ZoneType is ZoneType.Residential)
                {
                    availableHomes.Add(zone);
                }
                else if (zone.ZoneType is ZoneType.Industrial) 
                {
                    availableIndustrialWorkplaces.Add(zone);
                    workersinIndustrial += zone.Cells.Sum(c => CellData[c.Y, c.X].RelatedInhabitants.Count);
                }
                else if (zone.ZoneType is ZoneType.Service)
                {
                    availableServicelWorkplaces.Add(zone);
                    workersinService += zone.Cells.Sum(c => CellData[c.Y, c.X].RelatedInhabitants.Count);
                }
            }
            if (availableHomes.Count > 0 && availableServicelWorkplaces.Count + availableIndustrialWorkplaces.Count > 0)
            {
                _firstGroupOfInhabitantsArrived = true;
                Random rnd = new Random();
                Point homePoint;
                double chance = rnd.NextDouble();
                Point workplacePoint;
                Inhabitant newInhabitant;
                int closestdistance = 0;
                if (availableIndustrialWorkplaces.Count == 0)
                {
                    var res = ChooseOptimalCombo(CalculateDestinations(availableHomes, ZoneType.Service));
                    if (res is null)
                    {
                        return;
                    }
                    (homePoint, closestdistance, workplacePoint) = res.Value;
                }
                else if (availableServicelWorkplaces.Count == 0)
                {
                    var res = ChooseOptimalCombo(CalculateDestinations(availableHomes, ZoneType.Industrial));
                    if (res is null)
                    {
                        return;
                    }
                    (homePoint, closestdistance, workplacePoint) = res.Value;
                }
                else if (workersinIndustrial > workersinService) // I > S
                {
                    double proportion = workersinService / workersinIndustrial;
                    if (proportion > 0.1 ||  chance > proportion)
                    {
                        var res = ChooseOptimalCombo(CalculateDestinations(availableHomes, ZoneType.Service));
                        if (res is null)
                        {
                            return;
                        }
                        (homePoint, closestdistance, workplacePoint) = res.Value;
                    }
                    else
                    {
                        var res = ChooseOptimalCombo(CalculateDestinations(availableHomes, ZoneType.Industrial));
                        if (res is null)
                        {
                            return;
                        }
                        (homePoint, closestdistance, workplacePoint) = res.Value;

                    }
                }
                else // I <= S
                {
                    double proportion = workersinIndustrial / workersinService;
                    if (proportion > 0.1 || chance > proportion)
                    {
                        var res = ChooseOptimalCombo(CalculateDestinations(availableHomes, ZoneType.Industrial));
                        if (res is null)
                        {
                            return;
                        }
                        (homePoint, closestdistance, workplacePoint) = res.Value;
                    }
                    else
                    {
                        var res = ChooseOptimalCombo(CalculateDestinations(availableHomes, ZoneType.Service));
                        if (res is null)
                        {
                            return;
                        }
                        (homePoint, closestdistance, workplacePoint) = res.Value;
                    }
                }
                
                if (_cellData[homePoint.Y,homePoint.X].Building is null)
                {
                    //check probability here
                    double base_chance = 0.6;
                    //add global factor
                    base_chance += GlobalSatisfaction / 4;
                    //add zonedistance factor
                    Random rnd_chance = new Random();
                    if (rnd_chance.NextDouble() < base_chance)
                    {
                        
                        _factoryLayer.BuildRandomBuilding(_cellData[homePoint.Y,homePoint.X].Zone,homePoint);
                        newInhabitant = new Inhabitant(age);
                        Inhabitants.Add(newInhabitant);
                        newInhabitant.DistanceFromWorkplace = closestdistance;
                        CellData[homePoint.Y, homePoint.X].RelatedInhabitants.Add(newInhabitant);
                        int sum = CellData[homePoint.Y, homePoint.X].Zone.Cells.Sum(x => _cellData[x.Y, x.X].RelatedInhabitants.Count);
                        newInhabitant.Home = homePoint;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {

                    //check probability here
                    double base_chance = 0.6;
                    //add global factor
                    base_chance += GlobalSatisfaction / 4;
                    //add zonedistance factor
                    Random rnd_chance = new Random();
                    if (rnd_chance.NextDouble() < base_chance)
                    {
                        newInhabitant = new Inhabitant(age);
                        Inhabitants.Add(newInhabitant);
                        newInhabitant.DistanceFromWorkplace = closestdistance;
                        CellData[homePoint.Y, homePoint.X].RelatedInhabitants.Add(newInhabitant);
                        int sum = CellData[homePoint.Y, homePoint.X].Zone.Cells.Sum(x => _cellData[x.Y, x.X].RelatedInhabitants.Count);
                        newInhabitant.Home = homePoint;
                    }
                    else
                    {
                        return;
                    }
                }
                if (_cellData[workplacePoint.Y,workplacePoint.X].Building is null)
                {
                    _factoryLayer.BuildRandomBuilding(_cellData[workplacePoint.Y,workplacePoint.X].Zone,workplacePoint);
                    CellData[workplacePoint.Y, workplacePoint.X].RelatedInhabitants.Add(newInhabitant);

                    int sum = CellData[workplacePoint.Y, workplacePoint.X].Zone.Cells.Sum(x => _cellData[x.Y, x.X].RelatedInhabitants.Count);
                    newInhabitant.WorkPlace = workplacePoint;
                }
                else
                {
                    CellData[workplacePoint.Y, workplacePoint.X].RelatedInhabitants.Add(newInhabitant);
                    int sum = CellData[workplacePoint.Y, workplacePoint.X].Zone.Cells.Sum(x => _cellData[x.Y, x.X].RelatedInhabitants.Count);
                    newInhabitant.WorkPlace = workplacePoint;
                }
                NewZoneBuildingWasBuilt?.Invoke(this, EventArgs.Empty);
            }

        }

        private (Point, int, Point)? ChooseOptimalCombo(List<(Point, int, Point)> distances)
        {
            if (distances.Count==0)
            {
                return null;
            }

            //randomize distances
            Random rnd = new Random();
            distances = distances.OrderBy(x => rnd.Next()).ToList();

            double maxScore = Int32.MinValue;
            //Home, distance, Workplace
            (Point, int, Point)? optimalCombo = null!;
            distances.ForEach(distance =>
            {
                double currentScore = 0;
                if (CellData[distance.Item1.Y, distance.Item1.X].HasIndustrialBuildingNearby)
                {
                    currentScore -= 0.3;
                }
                if (distance.Item2 > 10)
                {
                    currentScore += 0;
                }
                else if (distance.Item2 > 5)
                {
                    currentScore += 1;
                }
                else if (distance.Item2 <= 5)
                {
                    currentScore += 2;
                }
                if (CellData[distance.Item1.Y, distance.Item1.X].Building is null && CellData[distance.Item1.Y, distance.Item1.X].WillSeeForestIfBuildsHere)
                {
                    currentScore += 0.2;
                }
                else if (CellData[distance.Item1.Y, distance.Item1.X].Building is not null && CellData[distance.Item1.Y, distance.Item1.X].CanSeeForest)
                {
                    currentScore += 0.2;
                }
                if (currentScore>maxScore)
                {
                    maxScore = currentScore;
                    optimalCombo = distance;
                }

            });
            return optimalCombo.Value;
        }

        private List<(Point, int, Point)> CalculateDestinations(List<Zone> availableHomes, ZoneType destinationZoneType)
        {
            List<(Point, int, Point)> distances = new();
            availableHomes.ForEach(home =>
            {
                home.Cells.Where(cell => _cellData[cell.Y,cell.X].RelatedInhabitants.Count < _cellData[cell.Y, cell.X].Zone.CurrentLevel*MapCell.DefaultCapacity).ToList().ForEach(cell =>
                {
                    distances.AddRange(ClosestWorkplaceDistanceNorm(cell, destinationZoneType));
                });
            });
            return distances;
        }

        private List<(Point,int,Point)> ClosestWorkplaceDistanceNorm(Point homePoint, ZoneType desiredWPType)
        {
            int distance = 1;
            List<(Point,int,Point)> closestWorkplaces = new();
            while (distance <= 10)
            {
                for (int i = homePoint.Y - distance; i <= homePoint.Y + distance; i++)
                {
                    for (int j = homePoint.X - distance; j <= homePoint.X + distance; j++)
                    {
                        if (!(i >= 0 && j >= 0 && i < _mapHeight && j < _mapWidth))
                        {
                            continue;
                        }
                        if (_cellData[i,j].Zone.ZoneType == desiredWPType)
                        {
                            if (_cellData[i, j].RelatedInhabitants.Count < _cellData[i, j].Zone.CurrentLevel * MapCell.DefaultCapacity)
                            {
                                closestWorkplaces.Add((homePoint, distance, new Point(j,i)));
                            }
                        }
                    }
                }
                if (closestWorkplaces.Count > 0)
                {
                    return closestWorkplaces;
                }
                distance++;
            }
            return new();
        }

        private void HandleFreeSpaceInZones()
        {
            foreach(Zone zone in _zones)
            {
                if (zone.Cells.Any(cell => _cellData[cell.Y,cell.X].RelatedInhabitants.Count<zone.CurrentLevel*MapCell.DefaultCapacity))
                {
                    zone.HasFreeSpace=true;
                }
                else
                {
                    zone.HasFreeSpace=false;
                }
            }
        }


        public void DeleteDisposableFireTrucks()
        {
            _fireStations.Where(fs => fs.FireTruckToBeDisposed).ToList().ForEach(fs =>
            {
                fs.FireTruckToBeDisposed = false;
                fs.FireTruckLocation = null;
                fs.FireTruckOrientation = null;
            });
        }

        public List<SpecialBuilding> GetSpecialBuildings()
        {
            List<SpecialBuilding> result = new List<SpecialBuilding>();
            result.AddRange(_secondarySchools);
            result.AddRange(_universities);
            result.AddRange(_fireStations);
            result.AddRange(_policeStations);
            result.AddRange(_stadiums);
            return result;
        } 

        public DateResponse GetGameDate()
        {
            return new DateResponse(_gameTime.Year, _gameTime.Month, _gameTime.Day);
        }

        private void CollectTax()
        {
            double tocollect = 0;
            double toDeduct = 0;

            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    foreach(Inhabitant inhabitant in CellData[i,j].RelatedInhabitants)
                    {
                        if (!inhabitant.IsPensioner && CellData[i,j].Zone.ZoneType is ZoneType.Residential)
                        {
                            tocollect += inhabitant.CurrentIncome * (_salaryDeductionPercentage * 0.01);
                        }
                        else if (inhabitant.IsPensioner && CellData[i, j].Zone.ZoneType is ZoneType.Residential)
                        {
                            toDeduct += inhabitant.Pension.Value;
                        }
                        else if (CellData[i,j].Zone.ZoneType is not ZoneType.Residential)
                        {

                            double electricityMultiplier = PowerHelper.HasEnoughPower(CellData[i, j].Zone) ? 1 : 0.5;
                            tocollect += (_salaryDeductionPercentage * 0.01) * (_workplaceTaxPerPerson) * electricityMultiplier;
                        }
                    }
                }
            }
            Balance += (int)(tocollect-toDeduct);

        }

        private int GetFinancialQuarterYear(DateTime desiredDate)
        {
            if (desiredDate.Month==12 || desiredDate.Month<=2)
            {
                return 1;
            }
            else if (desiredDate.Month >= 3 && desiredDate.Month <= 5)
            {
                return 2;
            }
            else if (desiredDate.Month >= 6 && desiredDate.Month <= 8)
            {
                return 3;
            }
            else 
            {
                return 4;
            }
        }

        private void DeductYearlyMaintenance()
        {
            int toDeduct = 0;
            List<IExtraInfo> buildings = GetSpecialBuildings().Select(building => (IExtraInfo)building).ToList();
            buildings.AddRange(Forests.Select(building => (IExtraInfo)building).ToList());
            buildings.AddRange(Roads.Select(building => (IExtraInfo)building).ToList());
            buildings.AddRange(PowerLines.Select(building => (IExtraInfo)building).ToList());
            foreach (IExtraInfo building in buildings)
            {
                DateTime buildDateTime = GetDateTimeFromDateResponse(building.BuildDate);
                int quarterYear = GetFinancialQuarterYear(_gameTime.AddDays(-1));
                _gameTime.AddDays(1);
                DateTime intervalStartDate = new DateTime((quarterYear * 3) - 3 == 0 ? Math.Max(0, _gameTime.Year - 1) : _gameTime.Year, (quarterYear * 3) - 3 == 0 ? 12 : (quarterYear * 3) - 3, 1);
                if (buildDateTime>=intervalStartDate)
                {
                    TimeSpan entireQuarterYear = _gameTime - intervalStartDate;
                    TimeSpan timeElapseSinceBuild = _gameTime - buildDateTime;

                    toDeduct += Convert.ToInt32(GetExpense(building.MaintenanceExpenseType) * (timeElapseSinceBuild.TotalDays/entireQuarterYear.TotalDays));
                }
                else
                {
                    toDeduct += GetExpense(building.MaintenanceExpenseType);
                }
            }
            Balance -= toDeduct;
        }

        public void HandleFiretruck(Building building)
        {
            if (!(building is ICanBurn))
            {
                throw new Exception("Cannot dispatch firetruck to building that cannot get on fire!");
            }
            if (((ICanBurn)building).HasDispatchedTruck)
            {
                return;
            }
            FireStation? selectedFireStation = null;
            List<(int,int)?>? fireTruckPath = null;
            List<(int,int)?>? pathBackToStation = null;
            bool willNotComeFromStation = true;
            foreach(FireStation firestation in _fireStations.Where(fs => fs.IsActive && !fs.IsOnDuty && PowerHelper.HasEnoughPower(fs)))
            {
                if (firestation.FireTruckLocation is null)
                {
                    List<(int,int)> activeRoadsAround = GetSurroundingRoadCoorinates(firestation);
                    var test = DateTimeOffset.Now;
                    foreach((int,int) coordPair in activeRoadsAround)
                    {
                        List<(int,int)?>? currentPath = FindShortestPath(coordPair.Item1,coordPair.Item2,_cellData,building);
                        if (fireTruckPath is null && currentPath is not null)
                        {
                            fireTruckPath = currentPath;
                            selectedFireStation = firestation;
                            willNotComeFromStation=false;
                        }
                        else if (fireTruckPath is not null && currentPath is not null)
                        {
                            if (currentPath.Count<fireTruckPath.Count)
                            {
                                fireTruckPath=currentPath;
                                selectedFireStation = firestation;
                                willNotComeFromStation=false;
                            }
                        }
                    }   
                }
                else
                {
                    var pathFromCurrentFireTruckPosition = FindShortestPath(firestation.FireTruckLocation.Value.Item1,firestation.FireTruckLocation.Value.Item2,_cellData,building);
                    if (pathFromCurrentFireTruckPosition is not null)
                    {
                        pathBackToStation = FindShortestPath(pathFromCurrentFireTruckPosition.Last().Value.Item1,pathFromCurrentFireTruckPosition.Last().Value.Item2,_cellData,firestation);
                    }
                    if (fireTruckPath is null && pathFromCurrentFireTruckPosition is not null)
                    {
                        fireTruckPath=pathFromCurrentFireTruckPosition;
                        selectedFireStation=firestation;
                        willNotComeFromStation=true;
                    }
                    else if (fireTruckPath is not null && pathFromCurrentFireTruckPosition is not null && pathFromCurrentFireTruckPosition.Count<fireTruckPath.Count)
                    {
                        fireTruckPath=pathFromCurrentFireTruckPosition;
                        selectedFireStation=firestation;
                        willNotComeFromStation=true;
                    }
                }
            }
            if (selectedFireStation is null)
            {
                return;
            }
            else
            {
                if (willNotComeFromStation)
                {
                    selectedFireStation.MoveUnitsToDestination((ICanBurn)building,fireTruckPath,pathBackToStation);
                }
                else
                {
                    selectedFireStation.MoveUnitsToDestination((ICanBurn)building,fireTruckPath);
                }
            }
        }

        private void RerouteUselessFiretrucks()
        {
            foreach(FireStation fs in FireStations.Where(fs => fs.IsOnDuty && fs.DispatchedTo is null && fs.FireTruckLocation is not null))
            {
                fs.AbortMission(FindShortestPath(fs.FireTruckLocation.Value.Item1, fs.FireTruckLocation.Value.Item2, _cellData, fs));
            }
        }

        private List<(int,int)> GetSurroundingRoadCoorinates(Building building)
        {
            if (building.SizeY * building.SizeX != 4)
            {
                throw new Exception("Fire station is not 2x2");
            }
            List<(int,int)> results = new List<(int,int)>();
            List<(int,int)> areaPoints = new List<(int,int)>();
            for (int i=building.Location.Y;i<building.Location.Y+building.SizeY;i++)
            {
                for (int j=building.Location.X;j<building.Location.X+building.SizeX;j++)
                {
                    areaPoints.Add((i,j));
                }
            }
            
            (int, int) topleftcorner = (building.Location.Y - 1, building.Location.X - 1);
            (int, int) botleftcorner = (building.Location.Y + 2,building.Location.X - 1);
            (int, int) toprightcorner = (building.Location.Y - 1,building.Location.X + 2);
            (int, int) botrightcorner = (building.Location.Y + 2,building.Location.X + 2);
            if (topleftcorner.Item1 >= 0 && topleftcorner.Item2 >= 0 && topleftcorner.Item1 < _mapHeight && topleftcorner.Item2 < _mapWidth)
            {
                areaPoints.Add(topleftcorner);
            }
            if (botleftcorner.Item1 >= 0 && botleftcorner.Item2 >= 0 && botleftcorner.Item1 < _mapHeight && botleftcorner.Item2 < _mapWidth)
            {
                areaPoints.Add(botleftcorner);
            }
            if (toprightcorner.Item1 >= 0 && toprightcorner.Item2 >= 0 && toprightcorner.Item1 < _mapHeight && toprightcorner.Item2 < _mapWidth)
            {
                areaPoints.Add(toprightcorner);
            }
            if (botrightcorner.Item1 >= 0 && botrightcorner.Item2 >= 0 && botrightcorner.Item1 < _mapHeight && botrightcorner.Item2 < _mapWidth)
            {
                areaPoints.Add(botrightcorner);
            }
            for (int i=building.Location.Y-1;i<=building.Location.Y+building.SizeY;i++)
            {
                for (int j=building.Location.X-1;j<=building.Location.X+building.SizeX;j++)
                {
                    if (i>=0 && i<_mapHeight && j>=0 && j<_mapWidth && !areaPoints.Contains((i,j)) && _cellData[i,j].Building is Road && ((Road)_cellData[i,j].Building).IsActive) 
                    {
                        results.Add((i,j));
                    }
                }
            }
            return results;
        }

        private void HandleBurningBuildings()
        {
            HashSet<Building> buildings = GetAllBuildings();
            foreach(Building building in buildings.Where(building => building is ICanBurn && ((ICanBurn)building).IsOnFire))
            {
                double spreadInterval = ((TimeSpan)(_gameTime-((ICanBurn)building).IsOnFireSince)).TotalDays;
                if(spreadInterval>=40 && spreadInterval<=50)
                {
                    //Spread to neighboring buildings
                    for (int i=building.Location.Y-1;i<=building.Location.Y+building.SizeY;i++)
                    {
                        for (int j=building.Location.X-1;j<=building.Location.X+building.SizeX;j++)
                        {
                            if(i>=0 && i<_mapHeight && j>=0 && j<_mapWidth && _cellData[i,j].Building is not null && _cellData[i,j].Building != building && _cellData[i,j].Building is ICanBurn && _cellData[i,j].Building is IExtraInfo && !((ICanBurn)_cellData[i,j].Building).IsOnFire && ((TimeSpan)(_gameTime-(GetDateTimeFromDateResponse(((IExtraInfo)_cellData[i,j].Building).BuildDate)))).TotalDays>=10)
                            {
                                ((ICanBurn)_cellData[i,j].Building).IsOnFire = true;
                                ((ICanBurn)_cellData[i,j].Building).IsOnFireSince = _gameTime;
                                BuildingCaughtFire?.Invoke(this,new Point(j,i));
                            }
                        }
                    }
                }
                else if (spreadInterval>50)
                {
                    int compensationCount = 0;
                    for (int i=building.Location.Y; i<building.Location.Y + building.SizeY; i++)
                    {
                        for (int j=building.Location.X; j<building.Location.X + building.SizeX;j++)
                        {
                            if (_cellData[i,j].Building is ZoneBuilding)
                            {
                                if (building is Home)
                                {
                                    _cellData[i,j].RelatedInhabitants.ForEach(inhabitant => 
                                    {
                                        _homelessInhabitants.Add(inhabitant);
                                        inhabitant.Home = null;
                                        inhabitant.MadForHouseLossDays = 30;
                                        compensationCount++;
                                    });
                                    _cellData[i,j].RelatedInhabitants.Clear();
                                }
                                else if (building is Workplace)
                                {
                                    _cellData[i,j].RelatedInhabitants.ForEach(inhabitant => 
                                    {
                                        _worklessInhabitants.Add(inhabitant);
                                        inhabitant.WorkPlace = null;
                                        inhabitant.MadForWorkplaceLossDays = 30;
                                        compensationCount++;
                                    });
                                    _cellData[i,j].RelatedInhabitants.Clear();
                                }
                            }
                            _cellData[i,j].Building=null;
                            foreach(FireStation  fs in FireStations.Where(fs => fs.DispatchedTo==(ICanBurn)building))
                            {
                                fs.DispatchedTo = null;
                            }
                        }
                    }
                    Balance -= compensationCount * GetExpense(ExpenseType.Compensation);
                    BalanceHasChanged?.Invoke(this,EventArgs.Empty);
                    if (building is ZoneBuilding)
                    {
                        _zoneBuildings.Remove((ZoneBuilding)building);
                    }
                    else if (building is Stadium)
                    {
                        ((Stadium)building).IsOnFire = false;
                        _factoryLayer.DestoryStadium((Stadium)building);   
                    }
                    else if (building is University)
                    {
                        ((University)building).IsOnFire = false;
                        _factoryLayer.DestroyUniversity((University)building);
                    }
                    else if (building is SecondarySchool)
                    {
                        ((SecondarySchool)building).IsOnFire = false;
                        _factoryLayer.DestroySecondarySchool((SecondarySchool)building);
                    }
                    else if (building is PoliceStation)
                    {
                        ((PoliceStation)building).IsOnFire = false;
                        _factoryLayer.DestroyPoliceStation((PoliceStation)building);
                    }
                    else if (building is PowerPlant)
                    {
                        ((PowerPlant)building).IsOnFire = false;
                        _factoryLayer.DestroyPowerPlant((PowerPlant)building);
                    }
                    MapHasChanged?.Invoke(this,EventArgs.Empty);
                }
                
            }
        }

        private DateTime GetDateTimeFromDateResponse(DateResponse dr)
        {
            return new DateTime(dr.Year,dr.Month,dr.Day);
        }

        private void SetRandomBuildingOnFire()
        {
            List<(Zone,Building,bool)> zonesAndBuildings = new List<(Zone,Building,bool)>();
            for (int i=0; i<_mapHeight; i++)
            {
                for (int j=0; j<_mapWidth;j++)
                {
                    if (CellData[i,j].Building is not null && CellData[i,j].Building is ICanBurn && !((ICanBurn)CellData[i,j].Building).IsOnFire)
                    {
                        if (zonesAndBuildings.Any(tri => tri.Item1 == CellData[i,j].Zone && tri.Item2 == CellData[i,j].Building))
                        {
                            var match = zonesAndBuildings.Where(tri => tri.Item1 == CellData[i,j].Zone && tri.Item2 == CellData[i,j].Building).First();
                            if (CellData[i,j].HasFirestationNearby)
                            {
                                match.Item3=true;
                            }
                        }
                        else
                        {
                            zonesAndBuildings.Add((CellData[i,j].Zone,CellData[i,j].Building,CellData[i,j].HasFirestationNearby));
                        }
                    }
                }
            }

            Dictionary<int,List<(Zone,Building,bool)>> fireProbabilityCategories = new Dictionary<int,List<(Zone,Building,bool)>>
            {
                {1,zonesAndBuildings.Where(tri => tri.Item1.ZoneType is ZoneType.Industrial && !tri.Item3).ToList()},
                {2,zonesAndBuildings.Where(tri => tri.Item1.ZoneType is not ZoneType.Industrial && !tri.Item3).ToList()},
                {3,zonesAndBuildings.Where(tri => tri.Item1.ZoneType is ZoneType.Industrial && tri.Item3).ToList()},
                {4,zonesAndBuildings.Where(tri => tri.Item1.ZoneType is not ZoneType.Industrial && tri.Item3).ToList()},
            };
            Random rnd = new Random();
            int choice = rnd.Next(1,16);
            if (choice <= 8)
            {
                if (fireProbabilityCategories[1].Count>0)
                {
                    var building = fireProbabilityCategories[1][rnd.Next(0,fireProbabilityCategories[1].Count)].Item2;
                    ((ICanBurn)building).IsOnFire = true;
                    ((ICanBurn)building).IsOnFireSince = _gameTime;
                    BuildingCaughtFire?.Invoke(this,building.Location);
                }
            }
            else if (choice >= 9 && choice <= 12)
            {
                if (fireProbabilityCategories[2].Count>0)
                {
                    var building = fireProbabilityCategories[2][rnd.Next(0,fireProbabilityCategories[2].Count)].Item2;
                    ((ICanBurn)building).IsOnFire = true;
                    ((ICanBurn)building).IsOnFireSince = _gameTime;
                    BuildingCaughtFire?.Invoke(this,building.Location);
                }
            }
            else if (choice == 13 || choice == 14)
            {
                if (fireProbabilityCategories[3].Count>0)
                {
                    var building = fireProbabilityCategories[3][rnd.Next(0,fireProbabilityCategories[3].Count)].Item2;
                    ((ICanBurn)building).IsOnFire = true;
                    ((ICanBurn)building).IsOnFireSince = _gameTime;
                    BuildingCaughtFire?.Invoke(this,building.Location);
                }
            }
            else if (choice == 15)
            {
                if (fireProbabilityCategories[4].Count>0)
                {
                    var building = fireProbabilityCategories[4][rnd.Next(0,fireProbabilityCategories[4].Count)].Item2;
                    ((ICanBurn)building).IsOnFire = true;
                    ((ICanBurn)building).IsOnFireSince = _gameTime;
                    BuildingCaughtFire?.Invoke(this,building.Location);

                }
            }

        }

        private void HandleEducation()
        {
            List<Inhabitant> inhabitantsWithPrimaryEducation = new List<Inhabitant>();
            List<Inhabitant> inhabitantsWithSecondaryEducation = new List<Inhabitant>();
            List<Inhabitant> inhabitantsWithHigherEducation = new List<Inhabitant>();
            _inhabitants.ForEach(inhabitant =>
            {
                if (inhabitant.Qualification == Qualification.SecondaryEducation)
                {
                    inhabitantsWithSecondaryEducation.Add(inhabitant);
                }
                else if (inhabitant.Qualification == Qualification.HigherEducation)
                {
                    inhabitantsWithHigherEducation.Add(inhabitant);
                    inhabitantsWithSecondaryEducation.Add(inhabitant);
                }
                else
                {
                    if (!inhabitant.IsPensioner)
                    {
                        inhabitantsWithPrimaryEducation.Add(inhabitant);
                    }
                }
            });
            int maxSecondary = (int)((_inhabitants.Count)*_secondaryEducationRatio);
            int maxHigher = (int)((_inhabitants.Count)*_higherEducationRatio);
            List<Inhabitant> inhabitantsWithSecondaryEducationWithoutHigherEducation = inhabitantsWithSecondaryEducation.Except(inhabitantsWithHigherEducation).ToList();
            int n = Math.Min(maxHigher-inhabitantsWithHigherEducation.Count,Math.Min(inhabitantsWithSecondaryEducationWithoutHigherEducation.Count,Math.Min(maxHigher,_universities.Where(university => university.IsActive && !university.IsOnFire && PowerHelper.HasEnoughPower(university)).Count()*_universityStudentCount)));
            if (n>0)
            {
                var rnd = new Random();
                var selected = inhabitantsWithSecondaryEducationWithoutHigherEducation.OrderBy(x => rnd.Next()).Take(n).ToList();
                foreach(Inhabitant inhabitant in selected)
                {
                    inhabitant.Qualification = Qualification.HigherEducation;
                }
            }
            n = Math.Min(maxSecondary-inhabitantsWithSecondaryEducation.Count,Math.Min(inhabitantsWithPrimaryEducation.Count,Math.Min(maxSecondary,_secondarySchools.Where(secondarySchool => secondarySchool.IsActive && !secondarySchool.IsOnFire && PowerHelper.HasEnoughPower(secondarySchool)).Count()*_secondarySchoolStudentCount)));
            if (n>0)
            {
                var rnd = new Random();
                var selected = inhabitantsWithPrimaryEducation.OrderBy(x => rnd.Next()).Take(n).ToList();
                foreach(Inhabitant inhabitant in selected)
                {
                    inhabitant.Qualification = Qualification.SecondaryEducation;
                }
            }
        }

        private void DeleteInhabitant(Inhabitant inhabitant)
        {
            if (inhabitant.Home is not null)
            {
                _cellData[inhabitant.Home.Value.Y,inhabitant.Home.Value.X].RelatedInhabitants.Remove(inhabitant);
                inhabitant.Home = null;
            }
            if (inhabitant.WorkPlace is not null)
            {
                _cellData[inhabitant.WorkPlace.Value.Y,inhabitant.WorkPlace.Value.X].RelatedInhabitants.Remove(inhabitant);
                inhabitant.WorkPlace = null;
            }
            _inhabitants.Remove(inhabitant);
            _worklessInhabitants.Remove(inhabitant);
            _homelessInhabitants.Remove(inhabitant);
            CreateNewInhabitant(18);
        }

        private void DeleteDissatisfiedInhabitant(Inhabitant inhabitant)
        {
            if (inhabitant.IsPensioner)
            {
                return;
            }
            if (inhabitant.Home is not null)
            {
                _cellData[inhabitant.Home.Value.Y, inhabitant.Home.Value.X].RelatedInhabitants.Remove(inhabitant);
                inhabitant.Home = null;
            }
            if (inhabitant.WorkPlace is not null)
            {
                _cellData[inhabitant.WorkPlace.Value.Y, inhabitant.WorkPlace.Value.X].RelatedInhabitants.Remove(inhabitant);
                inhabitant.WorkPlace = null;
            }
            _inhabitants.Remove(inhabitant);
            _worklessInhabitants.Remove(inhabitant);
            _homelessInhabitants.Remove(inhabitant);
        }

        private void HandlePopulationAging()
        {
            Random rnd = new Random();
            int deathAge = rnd.Next(70,90);
            List<Inhabitant> toKill = new List<Inhabitant>();
            _inhabitants.ForEach(inhabitant => 
            {
                inhabitant.Age++;
                if (inhabitant.Age>=deathAge)
                {
                    toKill.Add(inhabitant);
                }
                else if (inhabitant.Age>=65 && inhabitant.Age<=deathAge && !inhabitant.IsPensioner)
                {
                    if (inhabitant.WorkPlace is not null)
                    {
                        _cellData[inhabitant.WorkPlace.Value.Y,inhabitant.WorkPlace.Value.X].RelatedInhabitants.Remove(inhabitant);
                    }
                    inhabitant.TransformIntoPensioner();
                }
                else
                {
                    if (!inhabitant.IsPensioner)
                    {
                        inhabitant.Salaries.Add(inhabitant.CurrentIncome);
                    }
                }
            });
            toKill.ForEach(inhabitant => DeleteInhabitant(inhabitant));
        }

        private HashSet<Building> GetAllBuildings()
        {
            HashSet<Building> result = new HashSet<Building>();
            for (int i=0; i<_mapHeight;i++)
            {
                for (int j=0; j<_mapWidth;j++)
                {
                    result.Add(CellData[i,j].Building);
                }
            }
            return result;
        }

        public List<(int,int)?>? FindShortestPath(int row, int col, HWMat<MapCell> matrix,Building building)
        {
            if (!(building is SpecialBuilding || building is ZoneBuilding || building is PowerPlant))
            {
                return null;
            }
            int[] dr = { -1, 0, 1, 0 };
            int[] dc = { 0, 1, 0, -1 };

            bool[,] visited = new bool[_mapHeight, _mapWidth];
            Dictionary<(int,int)?, (int,int)?> previous = new Dictionary<(int,int)?, (int, int)?>();
            Queue<(int,int)> queue = new Queue<(int,int)>();

            visited[row, col] = true;
            queue.Enqueue((row,col));

            (int,int)? destination = null;

            while (queue.Count > 0)
            {
                (int,int) current = queue.Dequeue();

                for (int i = 0; i < 4; i++)
                {
                    int newRow = current.Item1 + dr[i];
                    int newCol = current.Item2 + dc[i];

                    if (newRow >= 0 && newRow < _mapHeight && newCol >= 0 && newCol < _mapWidth &&
                        !visited[newRow, newCol] && matrix[newRow, newCol].Building is not null && matrix[newRow, newCol].Building is Road && ((Road)matrix[newRow, newCol].Building).IsActive)
                    {
                        visited[newRow, newCol] = true;
                        previous[(newRow,newCol)] = current;
                        queue.Enqueue((newRow, newCol));

                        bool test = FieldIsDestination(newRow, newCol, matrix, building);
                        if (test)
                        {
                            destination = (newRow, newCol);
                            break;
                        }
                    }
                }

                if (destination != null)
                {
                    break;
                }
            }

            if (destination == null)
            {
                return null; // No path found
            }

            List<(int,int)?> path = new List<(int, int)?>();
            (int,int)? currentPathCell = destination;

            while (currentPathCell != null)
            {
                path.Add(currentPathCell);
                (int,int)? nextPathCell = null;
                previous.TryGetValue(currentPathCell, out nextPathCell);
                currentPathCell = nextPathCell;
            }

            path.Reverse();
            return path;
        }

        private bool FieldIsDestination(int row, int col, HWMat<MapCell> matrix, Building building)
        {
            int[] dr = { -1, 0, 1, 0 };
            int[] dc = { 0, 1, 0, -1 };

            for (int i = 0; i < 4; i++)
            {
                int newRow = row + dr[i];
                int newCol = col + dc[i];
                if (newRow >= 0 && newRow < _mapHeight && newCol >= 0 && newCol < _mapWidth)
                {
                    if (building is SpecialBuilding || building is PowerPlant)
                    {
                        if (matrix[newRow, newCol].Building == building)
                        {
                            return true;
                        }
                    }
                    else if (building is ZoneBuilding)
                    {
                        
                        if (((ZoneBuilding)building).Zone==matrix[newRow,newCol].Zone)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
        public int GetQuarterExpenseOfCity()
        {
            int toDeduct = 0;
            List<IExtraInfo> buildings = GetSpecialBuildings().Select(building => (IExtraInfo)building).ToList();
            buildings.AddRange(Forests.Select(building => (IExtraInfo)building).ToList());
            buildings.AddRange(Roads.Select(building => (IExtraInfo)building).ToList());
            buildings.AddRange(PowerLines.Select(building => (IExtraInfo)building).ToList());
            foreach (IExtraInfo building in buildings)
            {
                DateTime buildDateTime = GetDateTimeFromDateResponse(building.BuildDate);
                int quarterYear = GetFinancialQuarterYear(_gameTime.AddDays(-1));
                _gameTime.AddDays(1);
                DateTime intervalStartDate = new DateTime((quarterYear * 3) - 3 == 0 ? Math.Max(0, _gameTime.Year - 1) : _gameTime.Year, (quarterYear * 3) - 3 == 0 ? 12 : (quarterYear * 3) - 3, 1);
                if (buildDateTime >= intervalStartDate)
                {
                    TimeSpan entireQuarterYear = _gameTime - intervalStartDate;
                    TimeSpan timeElapseSinceBuild = _gameTime - buildDateTime;

                    toDeduct += Convert.ToInt32(GetExpense(building.MaintenanceExpenseType) * (timeElapseSinceBuild.TotalDays / entireQuarterYear.TotalDays));
                }
                else
                {
                    toDeduct += GetExpense(building.MaintenanceExpenseType);
                }
            }
            toDeduct += Convert.ToInt32(Inhabitants.Where(inhabitant => inhabitant.IsPensioner).Sum(x => x.Pension));
            return toDeduct;
        }
          
        public int GetQuarterIncome()
        {
            double tocollect = 0;
            double toDeduct = 0;

            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    foreach (Inhabitant inhabitant in CellData[i, j].RelatedInhabitants)
                    {
                        if (!inhabitant.IsPensioner && CellData[i, j].Zone.ZoneType is ZoneType.Residential)
                        {
                            tocollect += inhabitant.CurrentIncome * (_salaryDeductionPercentage * 0.01);
                        }
                        else if (inhabitant.IsPensioner && CellData[i, j].Zone.ZoneType is ZoneType.Residential)
                        {
                            toDeduct += inhabitant.Pension.Value;
                        }
                        else if (CellData[i, j].Zone.ZoneType is not ZoneType.Residential)
                        {

                            double electricityMultiplier = PowerHelper.HasEnoughPower(CellData[i, j].Zone) ? 1 : 0.5;
                            tocollect += (_salaryDeductionPercentage * 0.01) * (_workplaceTaxPerPerson) * electricityMultiplier;
                        }
                    }
                }
            }
            return Convert.ToInt32(tocollect - toDeduct);
        }

        private void UpdateHomelessInhabitants() {
            _homelessInhabitants = _inhabitants.Where(i => i.Home == null).ToList();
        }

        private void UpdateWorklessInhabitants() {
            _worklessInhabitants = _inhabitants.Where(i => i.WorkPlace == null).ToList();
        }

        private void UpdateMapCells() {
            _zones.ForEach(z => z.Cells.ForEach(c => _cellData[c.Y, c.X].Zone = z));
            _secondarySchools.ForEach(UpdateBuildingPoints);
            _universities.ForEach(UpdateBuildingPoints);
            _stadiums.ForEach(UpdateBuildingPoints);
            _policeStations.ForEach(UpdateBuildingPoints);
            _fireStations.ForEach(UpdateBuildingPoints);
            _roads.ForEach(UpdateBuildingPoints);
            _forests.ForEach(UpdateBuildingPoints);
            _powerLines.ForEach(UpdateBuildingPoints);
            _powerPlants.ForEach(UpdateBuildingPoints);
            _zoneBuildings.ForEach(UpdateBuildingPoints);
            _inhabitants
                .ForEach(i => {
                    if (i.Home != null) {
                        _cellData[i.Home.Value.Y, i.Home.Value.X].RelatedInhabitants.Add(i);
                    }

                    if (i.WorkPlace != null) {
                        _cellData[i.WorkPlace.Value.Y, i.WorkPlace.Value.X].RelatedInhabitants.Add(i);
                    }
                });
            AdjustSatisfactionAndForestGrowth();
        }

        private void UpdateBuildingPoints(Building building) {
            for (int i = 0; i < building.GetPointCount; i++) {
                int x = building.GetPoint(i).X;
                int y = building.GetPoint(i).Y;
                _cellData[y, x].Building = building;
            }
        }

        private void SubscribeToFireStationEvents() {
            _fireStations.ForEach(s => _factoryLayer.SubscribeToBuildindEvents(s));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("CityName", _cityName);
            info.AddValue("MapWidth", _mapWidth);
            info.AddValue("MapHeight", _mapHeight);
            info.AddValue("Balance", _balance);
            info.AddValue("GameTime", _gameTime);
            info.AddValue("DissatisfactionCounter", _dissatisfactionCounter);
            info.AddValue("DebtDaysCounter", _debtDaysCounter);
            info.AddValue("GlobalSatisfaction", _globalsatisfaction);
            info.AddValue("Zones", _zones);
            info.AddValue("ZoneBuildings", _zoneBuildings);
            
            info.AddValue("SecondarySchools", _secondarySchools);
            info.AddValue("Universities", _universities);
            info.AddValue("Stadiums", _stadiums);
            info.AddValue("PoliceStations", _policeStations);
            info.AddValue("FireStations", _fireStations);
            info.AddValue("Roads", _roads);
            info.AddValue("Forests", _forests);
            info.AddValue("PowerLines", _powerLines);
            info.AddValue("PowerPlants", _powerPlants);
            info.AddValue("Inhabitants", _inhabitants);
            info.AddValue("SalaryDeductionPercentage", _salaryDeductionPercentage);
        }

        public GameData(SerializationInfo info, StreamingContext context) {
            _cityName = info.GetString("CityName");
            _mapWidth = info.GetInt32("MapWidth");
            _mapHeight = info.GetInt32("MapHeight");
            _balance = info.GetInt32("Balance");
            _gameTime = info.GetDateTime("GameTime");
            _dissatisfactionCounter = info.GetInt32("DissatisfactionCounter");
            _debtDaysCounter = info.GetInt32("DebtDaysCounter");
            _globalsatisfaction = info.GetInt32("GlobalSatisfaction");
            _zones = (List<Zone>) info.GetValue("Zones", typeof(List<Zone>));
            _zoneBuildings = (List<ZoneBuilding>)info.GetValue("ZoneBuildings", typeof(List<ZoneBuilding>));
            
            _secondarySchools = (List<SecondarySchool>)info.GetValue("SecondarySchools", typeof(List<SecondarySchool>));
            _universities = (List<University>)info.GetValue("Universities", typeof(List<University>));
            _stadiums = (List<Stadium>)info.GetValue("Stadiums", typeof(List<Stadium>));
            _policeStations = (List<PoliceStation>)info.GetValue("PoliceStations", typeof(List<PoliceStation>));
            _fireStations = (List<FireStation>)info.GetValue("FireStations", typeof(List<FireStation>));
            _roads = (List<Road>)info.GetValue("Roads", typeof(List<Road>));
            _forests = (List<Forest>)info.GetValue("Forests", typeof(List<Forest>));
            _powerLines = (List<PowerLine>)info.GetValue("PowerLines", typeof(List<PowerLine>));
            _powerPlants = (List<PowerPlant>)info.GetValue("PowerPlants", typeof(List<PowerPlant>));
            _inhabitants = (List<Inhabitant>)info.GetValue("Inhabitants", typeof(List<Inhabitant>));
            _salaryDeductionPercentage = info.GetInt32("SalaryDeductionPercentage");
            _isPaused = true;
            _gameSpeed = GameSpeed.Normal;
            _factoryLayer = new FactoryLayer(this);
            _factoryLayer.TransferEvent += OnTransferEvent;
            _factoryLayer.IntTransferEvent += OnIntTransferEvent;
            _factoryLayer.FireWasExtingushed += OnFireExtinguished;
            _factoryLayer.RoadDeletionConflictEvent += OnRoadDeletionConflictEvent;
            _factoryLayer.ZoneBuildingDeletionConflictEvent += OnZoneBuildingDeletionConflictEvent;
            _cellData = new HWMat<MapCell>(_mapHeight, _mapWidth);
            _powerNetworks = new List<PowerNetwork>();
            for(int i = 0; i < _mapHeight; i++) {
                for(int j = 0; j < _mapWidth; j++) {
                    _cellData[i, j] = new MapCell();
                }
            }
            UpdateHomelessInhabitants();
            UpdateWorklessInhabitants();
            UpdateMapCells();
            CalculatePowerLineReachCoordinates();
            UpdatePowerNetworks();
            _factoryLayer.SpecialBuildingSatisfactionFunction();
            SubscribeToFireStationEvents();
        }

        public override bool Equals(object other) {
            return
                other is GameData otherData &&
                _gameTime.Equals(otherData._gameTime) &&
                _cityName.Equals(otherData._cityName) &&
                _mapWidth == otherData._mapWidth &&
                _mapHeight == otherData._mapHeight &&
                _balance == otherData._balance &&
                _dissatisfactionCounter == otherData._dissatisfactionCounter &&
                _debtDaysCounter == otherData._debtDaysCounter &&
                _zones.Equals(otherData._zones) &&
                _zoneBuildings.Equals(otherData._zoneBuildings) &&
                _secondarySchools.Equals(otherData._secondarySchools) &&
                _universities.Equals(otherData._universities) &&
                _stadiums.Equals(otherData._stadiums) &&
                _policeStations.Equals(otherData._policeStations) &&
                _fireStations.Equals(otherData._fireStations) &&
                _roads.Equals(otherData._roads) &&
                _forests.Equals(otherData._forests) &&
                _powerLines.Equals(otherData._powerLines) &&
                _powerPlants.Equals(otherData._powerPlants) &&
                _inhabitants.Equals(otherData._inhabitants);
        }

    }
}

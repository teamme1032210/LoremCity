﻿using Backend.Buildings;
using Backend.Zones;
using Backend.Utilities.CustomEventArgs;
using Backend.Utilities;
using Backend.GlobalEnums;
using Backend.Utilities.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.XR;
using Util;

namespace Backend.Data
{
    public class FactoryLayer
    {
        private GameData _dataLayer;
        private static readonly int _maxBuildingAreaX = 3;
        private static readonly int _maxBuildingAreaY = 3;

        private static readonly int _fireSafetyRadius = 5;
        private static readonly int _policeSafetyRadius = 5;
        private static readonly int _stadiumSatisfactionRadius = 5;

        public event EventHandler<TransferEventArgs>? TransferEvent;
        public event EventHandler<int>? IntTransferEvent;
        public event EventHandler? FireWasExtingushed;
        public event EventHandler<Road>? RoadDeletionConflictEvent;
        public event EventHandler? ZoneBuildingDeletionConflictEvent;

        public FactoryLayer(GameData gameData)
        {
            _dataLayer = gameData;
        }
        

        public (bool,int,List<Point>,List<Point>) ValidateZoneArrangement(HWMat<int> cellMatrix)
        {
            var validationResult = ArrangeZones(cellMatrix);
            return (validationResult.Item1,validationResult.Item2,validationResult.Item5,validationResult.Item6);
        }

        public void RearrangeZoneLayout(HWMat<int> cellMatrix,List<Home> homesToDestroy, List<Workplace> workplacesToDestroy)
        {
            var validationResult = ArrangeZones(cellMatrix);
            int compensationCount = 0;
            bool isValid = validationResult.Item1;
            if (!isValid)
            {
                throw new Exception("Cannot rearrange zone layout as the provided layout is not valid!");
            }
            _dataLayer.CellData= validationResult.Item4;
            foreach (Home home in homesToDestroy)
            {
                for (int i=home.Location.Y; i<home.Location.Y+home.SizeY;i++)
                {
                    for (int j= home.Location.X; j<home.Location.X + home.SizeX;j++)
                    {
                        _dataLayer.CellData[i,j].Building = null;
                        _dataLayer.CellData[i,j].RelatedInhabitants.ForEach(inhabitant =>
                        {
                            _dataLayer.HomelessInhabitants.Add(inhabitant);
                            inhabitant.Home = null;
                            inhabitant.MadForHouseLossDays = 30;
                            compensationCount++;
                        });
                        _dataLayer.CellData[i, j].RelatedInhabitants.Clear();
                    }
                }
                _dataLayer.ZoneBuildings.Remove(home);
            }
            foreach(Workplace workplace in workplacesToDestroy)
            {
                for (int i=workplace.Location.Y; i < workplace.Location.Y+workplace.SizeY;i++)
                {
                    for (int j = workplace.Location.X; j<workplace.Location.X + workplace.SizeX;j++)
                    {
                        _dataLayer.CellData[i,j].Building = null;
                        _dataLayer.CellData[i,j].RelatedInhabitants.ForEach(inhabitant =>
                        {
                            _dataLayer.WorklessInhabitants.Add(inhabitant);
                            inhabitant.WorkPlace = null;
                            inhabitant.MadForWorkplaceLossDays = 30;
                            compensationCount++;
                        });
                        _dataLayer.CellData[i, j].RelatedInhabitants.Clear();
                    }
                }
                _dataLayer.ZoneBuildings.Remove(workplace);
            }
            _dataLayer.Zones = validationResult.Item3;
            ActivatorFunction(false,null,null);
            IntTransferEvent?.Invoke(this, validationResult.Item2+compensationCount*_dataLayer.GetExpense(ExpenseType.Compensation));
            _dataLayer.UpdatePowerNetworks();
        }

        //Cell info from the frontend
        private (bool,int,List<Zone>?,HWMat<MapCell>?,List<Point>,List<Point>) ArrangeZones(HWMat<int> cellMatrix)
        {
            
            if(cellMatrix.Width != _dataLayer.MapWidth || cellMatrix.Height != _dataLayer.MapHeight)
            {
                throw new Exception("Cell matrix size does not match map size!");
            }

            int totalCost = 0;
            int minCells = 4;
            List<Zone> newZoneLayout = new List<Zone>();
            Zone specialZone = new Zone(ZoneType.Special, new List<Point>(), new List<ZoneBuilding>());
            newZoneLayout.Add(specialZone);
            HWMat<MapCell> newCellData = new HWMat<MapCell>(_dataLayer.MapHeight, _dataLayer.MapWidth);
            for (int i = 0; i<_dataLayer.MapHeight;i++)
            {
                for (int j = 0; j<_dataLayer.MapWidth;j++)
                {
                    newCellData[i,j] = new MapCell();
                }
            }

            bool[,] boolMatrix = new bool[cellMatrix.Height, cellMatrix.Width];
            bool isValid = true;
            List<Point> warningCells = new List<Point>();
            for (int i=0; i<cellMatrix.Height; i++)
            {
                for (int j=0; j<cellMatrix.Width; j++)
                {
                    if (!boolMatrix[i, j] && cellMatrix[i, j] != 0)
                    {
                        List<Point> points = new List<Point>();
                        int cellCount = recursiveExploration(cellMatrix, ref boolMatrix, cellMatrix[i, j], _dataLayer.CellData[i,j].Zone.CurrentLevel, i, j, ref points);
                        if (cellCount<minCells)
                        {
                            isValid = false;
                            warningCells.AddRange(points);
                        }
                        
                        else
                        {
                            switch (cellMatrix[i, j]) 
                            {
                                case 1:
                                    newZoneLayout.Add(new Zone(ZoneType.Residential, points, new List<ZoneBuilding>()));
                                    break;
                                case 2:
                                    newZoneLayout.Add(new Zone(ZoneType.Industrial, points, new List<ZoneBuilding>()));
                                    break;
                                case 3:
                                    newZoneLayout.Add(new Zone(ZoneType.Service, points, new List<ZoneBuilding>()));
                                    break;
                            }
                        }
                    }
                    else if (cellMatrix[i,j] == 0)
                    {
                        specialZone.Cells.Add(new Point(j, i));
                    }
                }
            }

            List<Point> invalidZoneTransitions = new List<Point>();

            HWMat<int> currentMatrix = ConvertCurrentStanding();

            for (int i = 0; i < cellMatrix.Height; i++)
            {
                for (int j=0; j<cellMatrix.Width; j++)
                {
                    if (currentMatrix[i, j] != cellMatrix[i,j])
                    {
                        if (_dataLayer.CellData[i,j].Building!=null)
                        {
                            if (_dataLayer.CellData[i,j].Building is not ZoneBuilding)
                            {
                                isValid = false;
                                HandleErrorPrecedence(warningCells, invalidZoneTransitions,i,j);
                                continue;
                            }
                        }
                        (int,int) transformationTuple = (currentMatrix[i, j], cellMatrix[i,j]);

                        switch (transformationTuple) 
                        {
                            case (0, 1):
                                totalCost+=_dataLayer.GetExpense(ExpenseType.Level1ResidentialFieldIsSet);
                                break;
                            case (0, 2):
                                totalCost+=_dataLayer.GetExpense(ExpenseType.Level1IndustrialFieldIsSet);
                                break;
                            case (0, 3):
                                totalCost+=_dataLayer.GetExpense(ExpenseType.Level1ServiceFieldIsSet);
                                break;
                            case (1, 0):
                                switch (_dataLayer.CellData[i,j].Zone.CurrentLevel)
                                {
                                    case 1:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level1ResidentialSetBackToDefault);
                                        break;
                                    case 2:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level2ResidentialSetBackToDefault);
                                        break;
                                    case 3:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level3ResidentialSetBackToDefault);
                                        break;
                                }
                                break;
                            case (1, 2):
                                HandleErrorPrecedence(warningCells, invalidZoneTransitions,i,j);
                                isValid = false;
                                continue;
                            case (1, 3):
                                HandleErrorPrecedence(warningCells, invalidZoneTransitions,i,j);
                                isValid = false;
                                continue;
                            case (2, 0):
                                switch (_dataLayer.CellData[i,j].Zone.CurrentLevel)
                                {
                                    case 1:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level1IndustrialSetBackToDefault);
                                        break;
                                    case 2:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level2IndustrialSetBackToDefault);
                                        break;
                                    case 3:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level3IndustrialSetBackToDefault);
                                        break;
                                }
                                break;
                            case (2, 1):
                                HandleErrorPrecedence(warningCells, invalidZoneTransitions,i,j);
                                isValid = false;
                                continue;
                            case (2, 3):
                                HandleErrorPrecedence(warningCells, invalidZoneTransitions,i,j);
                                isValid = false;
                                continue;
                            case (3, 0):
                                switch (_dataLayer.CellData[i,j].Zone.CurrentLevel)
                                {
                                    case 1:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level1ServiceSetBackToDefault);
                                        break;
                                    case 2:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level2ServiceSetBackToDefault);
                                        break;
                                    case 3:
                                        totalCost+=_dataLayer.GetExpense(ExpenseType.Level3ServiceSetBackToDefault);
                                        break;
                                }
                                break;
                            case (3, 1):
                                HandleErrorPrecedence(warningCells, invalidZoneTransitions,i,j);
                                isValid = false;
                                continue;
                            case (3, 2):
                                HandleErrorPrecedence(warningCells, invalidZoneTransitions,i,j);
                                isValid = false;
                                continue;
                        }
                    }
                    
                }
            }
            foreach (Zone zone in newZoneLayout)
            {
                List<Point> validCells = new List<Point>();
                int invalidTransitionCount = 0;
                foreach (Point point in zone.Cells)
                {
                    if (invalidZoneTransitions.Any(invalidpoint => invalidpoint.X == point.X && invalidpoint.Y == point.Y))
                    {
                        invalidTransitionCount++;
                    }
                    else
                    {
                        validCells.Add(point);
                    }
                    newCellData[point.Y,point.X].Zone=zone;
                    newCellData[point.Y,point.X].Building = _dataLayer.CellData[point.Y,point.X].Building;
                    newCellData[point.Y,point.X].RelatedInhabitants = _dataLayer.CellData[point.Y,point.X].RelatedInhabitants;
                    newCellData[point.Y,point.X].HasFirestationNearby = _dataLayer.CellData[point.Y,point.X].HasFirestationNearby;
                    int currentLevel;
                    if (zone.ZoneType is ZoneType.Special)
                    {
                        currentLevel=1;
                    }
                    else
                    {
                        if (_dataLayer.CellData[point.Y,point.X].Building is not null && _dataLayer.CellData[point.Y,point.X].Zone.ZoneType != ZoneType.Special)
                        {
                            var zoneBuilding = (ZoneBuilding)_dataLayer.CellData[point.Y,point.X].Building;
                            zoneBuilding.Zone=zone;
                        }
                        currentLevel=_dataLayer.CellData[point.Y,point.X].Zone.CurrentLevel;
                    }
                    newCellData[point.Y,point.X].Zone.CurrentLevel = currentLevel;
                }
                if (invalidTransitionCount>0 && zone.ZoneType != ZoneType.Special)
                {
                    warningCells.AddRange(validCells);
                }
            }
            if (isValid)
            {
                return (true,totalCost,newZoneLayout,newCellData,new List<Point>(),new List<Point>());
            }
            else
            {
                return (false,0,null,null,warningCells,invalidZoneTransitions);
            }
            
        }

        private void HandleErrorPrecedence(List<Point> warningCells, List<Point> invalidZoneTransitions, int i, int j)
        {
            Point invalidTransition = new Point(j,i);
            if (warningCells.Any(point => point.X==invalidTransition.X && point.Y==invalidTransition.Y))
            {
                Point alreadyExistingPoint = warningCells.Where(point => point.X==invalidTransition.X && point.Y==invalidTransition.Y).First();
                warningCells.Remove(alreadyExistingPoint);
            }
            invalidZoneTransitions.Add(invalidTransition);
        }
        
        private HWMat<int> ConvertCurrentStanding()
        {
            HWMat<int> toReturn = new HWMat<int>(_dataLayer.MapHeight, _dataLayer.MapWidth);
            for (int i=0; i<_dataLayer.MapHeight;i++) 
            {
                for (int j=0; j<_dataLayer.MapWidth;j++) 
                {
                    switch (_dataLayer.CellData[i,j].Zone.ZoneType)
                    {
                        case ZoneType.Special:
                            toReturn[i, j] = 0;
                            break;
                        case ZoneType.Residential:
                            toReturn[i, j] = 1;
                            break;
                        case ZoneType.Industrial:
                            toReturn[i, j] = 2;
                            break;
                        case ZoneType.Service:
                            toReturn[i, j] = 3;
                            break;
                    }
                }
            }
            return toReturn;
        }
        private int recursiveExploration(HWMat<int> cellMatrix, ref bool[,] boolMatrix, int zoneToFind, int zoneLevel, int row, int col, ref List<Point> points)
        {

            if (boolMatrix[row, col])
            {
                return 0;
            }
            if (!(cellMatrix[row, col] == zoneToFind && _dataLayer.CellData[row,col].Zone.CurrentLevel==zoneLevel))
            {
                return 0;
            }
            boolMatrix[row, col] = true;
            int count = 1;
            points.Add(new Point(col, row));

            if (row - 1 >= 0)
            {
                count += recursiveExploration(cellMatrix, ref boolMatrix, zoneToFind, zoneLevel, row - 1, col, ref points);
            }
            if (row + 1 < cellMatrix.Height)
            {
                count += recursiveExploration(cellMatrix, ref boolMatrix, zoneToFind, zoneLevel, row + 1, col, ref points);

            }
            if (col - 1 >= 0)
            {
                count += recursiveExploration(cellMatrix, ref boolMatrix, zoneToFind, zoneLevel, row, col - 1, ref points);

            }
            if (col + 1 < cellMatrix.Width)
            {
                count += recursiveExploration(cellMatrix, ref boolMatrix, zoneToFind, zoneLevel, row, col + 1, ref points);

            }
            return count;
        }
        public void BuildStadium(Point location)
        {
            Stadium toAdd = new Stadium(location,_dataLayer.GetGameDate());
            int baseArea= toAdd.SizeX*toAdd.SizeY;
            int availableFields = 0;
            for (int i=location.Y; i<Math.Min(_dataLayer.MapHeight,location.Y+toAdd.SizeY); i++)
            {
                for (int j=location.X; j<Math.Min(_dataLayer.MapWidth,location.X+toAdd.SizeX); j++)
                {
                    if (_dataLayer.CellData[i,j].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[i,j].Building is null)
                    {
                        availableFields++;
                    }
                }
            }
            if (!(availableFields==baseArea))
            {
                //Todo:Exception
                return;
            }

            for (int i=location.Y; i<location.Y+toAdd.SizeY;i++)
            {
                for (int j=location.X; j<location.X+toAdd.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building=toAdd;
                }
            }
            _dataLayer.Stadiums.Add(toAdd);

            ActivatorFunction(false,null,null);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.StadiumIsBuilt));

            _dataLayer.UpdatePowerNetworks();
        }

        public void DestoryStadium(Stadium stadium)
        {
            if (stadium.IsOnFire) { return; }
            for (int i=stadium.Location.Y; i<stadium.Location.Y+stadium.SizeY;i++)
            {
                for (int j=stadium.Location.X; j<stadium.Location.X+stadium.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building=null;
                }
            }
            _dataLayer.Stadiums.Remove(stadium);

            SpecialBuildingSatisfactionFunction();

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.StadiumIsDestroyed));

            _dataLayer.UpdatePowerNetworks();
        }

        public void BuildFireStation(Point location)
        {
            FireStation toAdd = new FireStation(location,_dataLayer.GetGameDate());
            int baseArea = toAdd.SizeX * toAdd.SizeY;
            int availableFields = 0;
            for (int i = location.Y; i < Math.Min(_dataLayer.MapHeight, location.Y + toAdd.SizeY); i++)
            {
                for (int j = location.X; j < Math.Min(_dataLayer.MapWidth, location.X + toAdd.SizeX); j++)
                {
                    if (_dataLayer.CellData[i, j].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[i, j].Building is null)
                    {
                        availableFields++;
                    }
                }
            }
            if (!(availableFields == baseArea))
            {
                //Todo:Exception
                return;
            }

            for (int i = location.Y; i < location.Y + toAdd.SizeY; i++)
            {
                for (int j = location.X; j < location.X + toAdd.SizeX; j++)
                {
                    _dataLayer.CellData[i, j].Building = toAdd;
                }
            }
            _dataLayer.FireStations.Add(toAdd);

            toAdd.FireWasExtinguishedEvent += OnFireExtinguished;

            ActivatorFunction(false,null,null);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.FireStationIsBuilt));

            _dataLayer.UpdatePowerNetworks();
        }

        public void SpecialBuildingSatisfactionFunction()
        {
            for (int i=0; i<_dataLayer.MapHeight;i++)
            {
                for (int j=0; j<_dataLayer.MapWidth;j++)
                {
                    _dataLayer.CellData[i,j].HasFirestationNearby=false;
                    _dataLayer.CellData[i,j].HasPolicestationNearby=false;
                    _dataLayer.CellData[i,j].HasStadiumNearby=false;
                }
            }
            foreach(FireStation fs in _dataLayer.FireStations.Where(fs => fs.IsActive && PowerHelper.HasEnoughPower(fs)))
            {
                for (int i=fs.Location.Y-_fireSafetyRadius; i< fs.Location.Y + fs.SizeY + _fireSafetyRadius; i++)
                {
                    for (int j=fs.Location.X-_fireSafetyRadius; j< fs.Location.X + fs.SizeX + _fireSafetyRadius; j++)
                    {
                        if (i>=0 && i<_dataLayer.MapHeight && j>=0 && j<_dataLayer.MapWidth)
                        {
                            _dataLayer.CellData[i,j].HasFirestationNearby=true;
                        }
                    }
                }
            }
            foreach(PoliceStation ps in _dataLayer.PoliceStations.Where(ps => ps.IsActive && PowerHelper.HasEnoughPower(ps)))
            {
                for (int i=ps.Location.Y-_policeSafetyRadius; i< ps.Location.Y + ps.SizeY + _policeSafetyRadius; i++)
                {
                    for (int j=ps.Location.X-_policeSafetyRadius; j< ps.Location.X + ps.SizeX + _policeSafetyRadius; j++)
                    {
                        if (i>=0 && i<_dataLayer.MapHeight && j>=0 && j<_dataLayer.MapWidth)
                        {
                            _dataLayer.CellData[i,j].HasPolicestationNearby=true;
                        }
                    }
                }
            }
            foreach(Stadium stadium in _dataLayer.Stadiums.Where(stadium => stadium.IsActive && PowerHelper.HasEnoughPower(stadium)))
            {
                for (int i=stadium.Location.Y-_stadiumSatisfactionRadius; i< stadium.Location.Y + stadium.SizeY + _stadiumSatisfactionRadius; i++)
                {
                    for (int j=stadium.Location.X-_stadiumSatisfactionRadius; j< stadium.Location.X + stadium.SizeX + _stadiumSatisfactionRadius; j++)
                    {
                        if (i>=0 && i<_dataLayer.MapHeight && j>=0 && j<_dataLayer.MapWidth)
                        {
                            _dataLayer.CellData[i,j].HasStadiumNearby=true;
                        }
                    }
                }
            }
        }

        public void DestroyFireStation(FireStation fireStation)
        {
            for (int i = fireStation.Location.Y; i < fireStation.Location.Y + fireStation.SizeY; i++)
            {
                for (int j = fireStation.Location.X; j < fireStation.Location.X + fireStation.SizeX; j++)
                {
                    _dataLayer.CellData[i, j].Building = null;
                }
            }
            fireStation.FireWasExtinguishedEvent -= OnFireExtinguished;
            _dataLayer.FireStations.Remove(fireStation);

            SpecialBuildingSatisfactionFunction();

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.FireStationIsDestroyed));

            _dataLayer.UpdatePowerNetworks();
        }

        public void BuildForest(Point location)
        {
            Forest toAdd = new Forest(location, _dataLayer.GetGameDate());

            if (_dataLayer.CellData[location.Y, location.X].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[location.Y, location.X].Building is null)
            {
                _dataLayer.CellData[location.Y, location.X].Building = toAdd;
                _dataLayer.Forests.Add(toAdd);
            }
            else
            {
                return;
            }

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.ForestIsCreated));
        }

        public void DestroyForest(Forest forest)
        {

            _dataLayer.CellData[forest.Location.Y, forest.Location.X].Building = null;

            _dataLayer.Forests.Remove(forest);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.ForestIsDestroyed));
        }

        public void BuildPoliceStation(Point location)
        {
            PoliceStation toAdd = new PoliceStation(location,_dataLayer.GetGameDate());
            int baseArea = toAdd.SizeX * toAdd.SizeY;
            int availableFields = 0;
            for (int i = location.Y; i < Math.Min(_dataLayer.MapHeight, location.Y + toAdd.SizeY); i++)
            {
                for (int j = location.X; j < Math.Min(_dataLayer.MapWidth, location.X + toAdd.SizeX); j++)
                {
                    if (_dataLayer.CellData[i, j].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[i, j].Building is null)
                    {
                        availableFields++;
                    }
                }
            }
            if (!(availableFields == baseArea))
            {
                //Todo:Exception
                return;
            }

            for (int i = location.Y; i < location.Y + toAdd.SizeY; i++)
            {
                for (int j = location.X; j < location.X + toAdd.SizeX; j++)
                {
                    _dataLayer.CellData[i, j].Building = toAdd;
                }
            }
            _dataLayer.PoliceStations.Add(toAdd);

            ActivatorFunction(false,null,null);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.PoliceStationIsBuilt));

            //Event that the data has changed
            _dataLayer.UpdatePowerNetworks();
        }

        public void DestroyPoliceStation(PoliceStation policeStation)
        {
            if (policeStation.IsOnFire) { return; }
            for (int i = policeStation.Location.Y; i < policeStation.Location.Y + policeStation.SizeY; i++)
            {
                for (int j = policeStation.Location.X; j < policeStation.Location.X + policeStation.SizeX; j++)
                {
                    _dataLayer.CellData[i, j].Building = null;
                }
            }
            _dataLayer.PoliceStations.Remove(policeStation);

            SpecialBuildingSatisfactionFunction();

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.PoliceStationIsDestroyed));

            //Event that the data has changed
            _dataLayer.UpdatePowerNetworks();
        }

        public void BuildPowerLine(Point location)
        {
            PowerLine toAdd = new PowerLine(location,_dataLayer.GetGameDate());

            if (_dataLayer.CellData[location.Y, location.X].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[location.Y, location.X].Building is null)
            {
                _dataLayer.CellData[location.Y, location.X].Building = toAdd;
                _dataLayer.PowerLines.Add(toAdd);
            }
            else
            {
                return;
            }

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.PowerLineIsBuilt));
            _dataLayer.UpdatePowerNetworks();
        }

        public void DestroyPowerLine(PowerLine powerLine)
        {
            _dataLayer.CellData[powerLine.Location.Y, powerLine.Location.X].Building = null;

            _dataLayer.PowerLines.Remove(powerLine);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.PowerLineIsDestroyed));
            _dataLayer.UpdatePowerNetworks();
        }

        public void BuildPowerPlant(Point location)
        {
            PowerPlant toAdd = new PowerPlant(location,_dataLayer.GetGameDate());
            int baseArea= toAdd.SizeX*toAdd.SizeY;
            int availableFields = 0;
            for (int i=location.Y; i<Math.Min(_dataLayer.MapHeight,location.Y+toAdd.SizeY); i++)
            {
                for (int j=location.X; j<Math.Min(_dataLayer.MapWidth,location.X+toAdd.SizeX); j++)
                {
                    if (_dataLayer.CellData[i,j].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[i,j].Building is null)
                    {
                        availableFields++;
                    }
                }
            }
            if (!(availableFields==baseArea))
            {   
                // DON'T THROW AN EXCEPTION HERE
                //Todo:Exception
                return;
            }

            for (int i=location.Y; i<location.Y+toAdd.SizeY;i++)
            {
                for (int j=location.X; j<location.X+toAdd.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building=toAdd;
                }
            }
            _dataLayer.PowerPlants.Add(toAdd);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.PowerplantIsBuilt));
            _dataLayer.UpdatePowerNetworks();
        }

        public void DestroyPowerPlant(PowerPlant powerplant)
        {
            if (powerplant.IsOnFire) { return; }
            for (int i=powerplant.Location.Y; i<powerplant.Location.Y+powerplant.SizeY;i++)
            {
                for (int j=powerplant.Location.X; j<powerplant.Location.X+powerplant.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building=null;
                }
            }
        
            _dataLayer.PowerPlants.Remove(powerplant);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.PowerplantIsDestroyed));
            _dataLayer.UpdatePowerNetworks();
        }
        private void ActivatorFunction(bool isTame, int? destRow, int? destCol)
        {
            bool hasError = false;
            ActivatorFunction(isTame, destRow, destCol, out hasError);
        }

        private void ActivatorFunction(bool isTame, int? destRow, int? destCol, out bool hasError)
        {
            hasError = false;
            bool[,] boolMatrix = new bool[_dataLayer.MapHeight, _dataLayer.MapWidth];
            List<Road> activeRoads = new List<Road>();
            HashSet<Zone> activeZones = new HashSet<Zone>();
            HashSet<SpecialBuilding> activeSpecialBuildings = new HashSet<SpecialBuilding>();
            HandleActiveRoads(_dataLayer.firstPieceOfRoad.Y, _dataLayer.firstPieceOfRoad.X, ref boolMatrix, ref activeRoads, ref activeZones, ref activeSpecialBuildings,destRow,destCol);
            if (_dataLayer.Zones.Any(zone => zone.IsActive && !activeZones.Contains(zone)))
            {
                hasError = true;
                if (destRow != null && destCol != null)
                {
                    RoadDeletionConflictEvent?.Invoke(this, (Road)(_dataLayer.CellData[destRow.Value,destCol.Value].Building));
                }
            }
            if ((!hasError && isTame) || !isTame)
            {
                foreach (Road road in _dataLayer.Roads) 
                {
                    if (activeRoads.Contains(road))
                    {
                        road.IsActive= true;
                    }
                    else
                    {
                        road.IsActive = false;
                    }
                }
                foreach (Zone zone in _dataLayer.Zones)
                {
                    if (activeZones.Contains(zone))
                    {
                        zone.IsActive = true;
                    }
                    else
                    {
                        zone.IsActive = false;
                    }
                }
                foreach (SpecialBuilding specialBuilding in _dataLayer.GetSpecialBuildings())
                {
                    if (activeSpecialBuildings.Contains(specialBuilding))
                    {
                        specialBuilding.IsActive = true;
                    }
                    else
                    {
                        specialBuilding.IsActive = false;
                    }
                }
                SpecialBuildingSatisfactionFunction();
            }
        }

        public void BuildRoad(Point location)
        {
            Road toAdd = new Road(location,_dataLayer.GetGameDate());

            if (_dataLayer.CellData[location.Y,location.X].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[location.Y, location.X].Building is null)
            {
                _dataLayer.CellData[location.Y, location.X].Building = toAdd;
                _dataLayer.Roads.Add(toAdd);
            }
            else
            {
                return;
            }
            ActivatorFunction(false,null,null);
            //TODO: Notifiy surrounding buildings


            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.RoadIsBuilt));
        }

        private void HandleActiveRoads(int row, int col, ref bool[,] boolMatrix, ref List<Road> activeRoads, ref HashSet<Zone> activeZones, ref HashSet<SpecialBuilding> activeSpecialBuildings,int? destRow, int? destCol)
        {
            if (destRow is not null && destCol is not null && row==destRow.Value && col == destCol.Value)
            {
                return;
            }
            if (boolMatrix[row,col] || _dataLayer.CellData[row,col].Building is null || _dataLayer.CellData[row,col].Building is not Road)
            {
                return;
            }
            ((Road)_dataLayer.CellData[row, col].Building).IsActive = true;
            activeRoads.Add(((Road)_dataLayer.CellData[row, col].Building));
            boolMatrix[row, col] = true;
            if (row - 1 >= 0)
            {
                //TODO: Collect active special buildings
                if (_dataLayer.CellData[row-1,col].Zone.ZoneType is not ZoneType.Special)
                {
                    activeZones.Add(_dataLayer.CellData[row - 1, col].Zone);
                }
                else
                {
                    if (_dataLayer.CellData[row-1,col].Building is not null && _dataLayer.CellData[row-1,col].Building is SpecialBuilding)
                    {
                        activeSpecialBuildings.Add((SpecialBuilding)_dataLayer.CellData[row-1,col].Building);
                    }
                }
                HandleActiveRoads(row - 1, col, ref boolMatrix, ref activeRoads, ref activeZones, ref activeSpecialBuildings,destRow,destCol);
            }
            if (row + 1 < _dataLayer.MapHeight)
            {
                if (_dataLayer.CellData[row + 1, col].Zone.ZoneType is not ZoneType.Special)
                {
                    activeZones.Add(_dataLayer.CellData[row + 1, col].Zone);
                }
                else
                {
                    if (_dataLayer.CellData[row+1,col].Building is not null && _dataLayer.CellData[row+1,col].Building is SpecialBuilding)
                    {
                        activeSpecialBuildings.Add((SpecialBuilding)_dataLayer.CellData[row+1,col].Building);
                    }
                }
                HandleActiveRoads(row+1,col, ref boolMatrix, ref activeRoads, ref activeZones, ref activeSpecialBuildings,destRow,destCol);

            }
            if (col - 1 >= 0)
            {
                if (_dataLayer.CellData[row, col - 1].Zone.ZoneType is not ZoneType.Special)
                {
                    activeZones.Add(_dataLayer.CellData[row, col -1].Zone);
                }
                else
                {
                    if (_dataLayer.CellData[row,col-1].Building is not null && _dataLayer.CellData[row,col-1].Building is SpecialBuilding)
                    {
                        activeSpecialBuildings.Add((SpecialBuilding)_dataLayer.CellData[row,col-1].Building);
                    }
                }
                HandleActiveRoads(row, col-1, ref boolMatrix, ref activeRoads, ref activeZones, ref activeSpecialBuildings,destRow,destCol);
            }
            if (col + 1 < _dataLayer.MapWidth)
            {
                if (_dataLayer.CellData[row, col + 1].Zone.ZoneType is not ZoneType.Special)
                {
                    activeZones.Add(_dataLayer.CellData[row, col + 1].Zone);
                }
                else
                {
                    if (_dataLayer.CellData[row,col+1].Building is not null && _dataLayer.CellData[row,col+1].Building is SpecialBuilding)
                    {
                        activeSpecialBuildings.Add((SpecialBuilding)_dataLayer.CellData[row,col+1].Building);
                    }
                }
                HandleActiveRoads(row, col + 1, ref boolMatrix, ref activeRoads, ref activeZones, ref activeSpecialBuildings,destRow,destCol);

            }
            return;
        }

        public void DestroyRoad(Road road, bool isTame)
        {
            if ( _dataLayer.FireStations.Any( f => f.FireTruckLocation != null))
            {
                return;
            }
            bool hasError = false;
            if (road.Location.X == _dataLayer.firstPieceOfRoad.X && road.Location.Y == _dataLayer.firstPieceOfRoad.Y)
            {
                //TODO: Throw some nasty exception
                return;
            }
            if (isTame)
            {
                ActivatorFunction(true,road.Location.Y,road.Location.X, out hasError);
            }
            if ((!hasError && isTame) || !isTame)
            {
                _dataLayer.CellData[road.Location.Y, road.Location.X].Building = null;
                _dataLayer.Roads.Remove(road);

                UnityEngine.Debug.Log("is not tame");
                ActivatorFunction(false,null,null);


                TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.RoadIsDestroyed));
            }
            
        }

        public void BuildSecondarySchool(Point location)
        {
            SecondarySchool toAdd = new SecondarySchool(location,_dataLayer.GetGameDate());
            int baseArea= toAdd.SizeX*toAdd.SizeY;
            int availableFields = 0;
            for (int i=location.Y; i < Math.Min(_dataLayer.MapHeight,location.Y + toAdd.SizeY); i++)
            {
                for (int j=location.X; j < Math.Min(_dataLayer.MapWidth,location.X + toAdd.SizeX); j++)
                {
                    if (_dataLayer.CellData[i,j].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[i,j].Building is null)
                    {
                        availableFields++;
                    }
                }
            }
            if (!(availableFields == baseArea))
            {
                //Todo:Exception
                return;
            }

            for (int i=location.Y; i<location.Y+toAdd.SizeY;i++)
            {
                for (int j=location.X; j<location.X+toAdd.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building = toAdd;
                }
            }

            _dataLayer.SecondarySchools.Add(toAdd);
            
            ActivatorFunction(false,null,null);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.SchoolIsBuilt));
            _dataLayer.UpdatePowerNetworks();
        }

        public void DestroySecondarySchool(SecondarySchool secondarySchool)
        {
            if (secondarySchool.IsOnFire) { return; }
            for (int i=secondarySchool.Location.Y; i<secondarySchool.Location.Y+secondarySchool.SizeY;i++)
            {
                for (int j=secondarySchool.Location.X; j<secondarySchool.Location.X+secondarySchool.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building=null;
                }
            }
            _dataLayer.SecondarySchools.Remove(secondarySchool);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.SchoolIsDestroyed));
            _dataLayer.UpdatePowerNetworks();
        }

        public void BuildUniversity(Point location)
        {
            University toAdd = new University(location,_dataLayer.GetGameDate());
            int baseArea= toAdd.SizeX*toAdd.SizeY;
            int availableFields = 0;
            for (int i=location.Y; i<Math.Min(_dataLayer.MapHeight,location.Y+toAdd.SizeY); i++)
            {
                for (int j=location.X; j<Math.Min(_dataLayer.MapWidth,location.X+toAdd.SizeX); j++)
                {
                    if (_dataLayer.CellData[i,j].Zone.ZoneType is ZoneType.Special && _dataLayer.CellData[i,j].Building is null)
                    {
                        availableFields++;
                    }
                }
            }
            if (!(availableFields==baseArea))
            {
                //Todo:Exception
                return;
            }

            for (int i=location.Y; i<location.Y+toAdd.SizeY;i++)
            {
                for (int j=location.X; j<location.X+toAdd.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building=toAdd;
                }
            }
            
            _dataLayer.Universities.Add(toAdd);

            ActivatorFunction(false,null,null);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.UniversityIsBuilt));
            _dataLayer.UpdatePowerNetworks();
        }

        public void DestroyUniversity(University university)
        {
            if (university.IsOnFire) { return; }
            for (int i=university.Location.Y; i<university.Location.Y+university.SizeY;i++)
            {
                for (int j=university.Location.X; j<university.Location.X+university.SizeX;j++)
                {
                    _dataLayer.CellData[i,j].Building=null;
                }
            }

            _dataLayer.Universities.Remove(university);

            TransferEvent!.Invoke(this, new TransferEventArgs(ExpenseType.UniversityIsDestroyed));
            _dataLayer.UpdatePowerNetworks();
        }

        public void UpgradeZone(Zone zone)
        {
            //TODO: Notify the frontend that the Zone has been upgraded
            if (!((zone.CurrentLevel == 1 || zone.CurrentLevel==2) && zone.ZoneType !=ZoneType.Special ))
            {
                return;
            }

            switch (zone.CurrentLevel)
            {
                case 1:
                    if (zone.ZoneType is ZoneType.Residential)
                    {
                        IntTransferEvent?.Invoke(this, zone.Cells.Count*_dataLayer.GetExpense(ExpenseType.ResidentialUpgradedToLevel2));
                        
                    }
                    else if (zone.ZoneType is ZoneType.Service)
                    {
                        IntTransferEvent?.Invoke(this, zone.Cells.Count*_dataLayer.GetExpense(ExpenseType.ServiceUpgradedToLevel2));
                    }
                    else if (zone.ZoneType is ZoneType.Industrial)
                    {
                        IntTransferEvent?.Invoke(this, zone.Cells.Count*_dataLayer.GetExpense(ExpenseType.IndustrialUpgradedToLevel2));
                    }
                    break;
                case 2:
                    if (zone.ZoneType is ZoneType.Residential)
                    {
                        IntTransferEvent?.Invoke(this, zone.Cells.Count*_dataLayer.GetExpense(ExpenseType.ResidentialUpgradedToLevel3));
                    }
                    else if (zone.ZoneType is ZoneType.Service)
                    {
                        
                        IntTransferEvent?.Invoke(this, zone.Cells.Count*_dataLayer.GetExpense(ExpenseType.ServiceUpgradedToLevel3));
                    }
                    else if (zone.ZoneType is ZoneType.Industrial)
                    {
                       
                        IntTransferEvent?.Invoke(this, zone.Cells.Count*_dataLayer.GetExpense(ExpenseType.IndustrialUpgradedToLevel3));
                    }
                    break;
            }
            zone.HasFreeSpace = true;
            zone.CurrentLevel++;
            RearrangeZoneLayout(ConvertCurrentStanding(),new List<Home>(),new List<Workplace>());
        }

        public Building? BuildRandomBuilding(Zone zone, Point chosenPoint)
        {
            Random rnd = new Random();
            
            if (!(zone.IsActive && zone.HasFreeSpace && PowerHelper.HasEnoughPower(zone)))
            {
                return null;
            }
            
            int maxX = _maxBuildingAreaX;
            int maxY = _maxBuildingAreaY;
            bool foundFittingBuilding = false;
            while (!foundFittingBuilding)
            {
                bool foundHere = Fits(maxY,maxX,zone,chosenPoint);
                if (foundHere)
                {
                    break;
                }
                foundHere = Fits(maxY,maxX-1,zone,chosenPoint);
                if (foundHere)
                {
                    maxX--;
                    break;
                }
                foundHere = Fits(maxY-1,maxX,zone,chosenPoint);
                if (foundHere)
                {
                    maxY--;
                    break;
                }
                maxX--;
                maxY--;
            }

            switch (zone.ZoneType)
            {
                case ZoneType.Residential:
                    Home home = new Home(chosenPoint, zone, GameData.HomeVariationStorage[(maxX, maxY)][rnd.Next(0,GameData.HomeVariationStorage[(maxX,maxY)].Count)], _dataLayer.GetGameDate());
                    AddNewZoneBuilding(home, chosenPoint, zone);
                    return home;
                case ZoneType.Industrial:
                    IndustrialWorkplace industrialWorkplace = new IndustrialWorkplace(chosenPoint, zone, GameData.IndustrialWorkplaceVariationStorage[(maxX, maxY)][rnd.Next(0, GameData.IndustrialWorkplaceVariationStorage[(maxX, maxY)].Count)], _dataLayer.GetGameDate());
                    AddNewZoneBuilding(industrialWorkplace, chosenPoint, zone);
                    return industrialWorkplace;
                case ZoneType.Service:
                    ServiceWorkplace serviceWorkplace = new ServiceWorkplace(chosenPoint, zone, GameData.ServiceWorkplaceVariationStorage[(maxX, maxY)][rnd.Next(0, GameData.ServiceWorkplaceVariationStorage[(maxX, maxY)].Count)], _dataLayer.GetGameDate());
                    AddNewZoneBuilding(serviceWorkplace, chosenPoint, zone);
                    return serviceWorkplace;
            } 
            return null;
        }

        private void AddNewZoneBuilding(ZoneBuilding building, Point chosenPoint, Zone zone)
        {
            for (int i=chosenPoint.Y; i<chosenPoint.Y+building.SizeY; i++) 
            {
                for (int j=chosenPoint.X; j<chosenPoint.X+building.SizeX; j++) 
                {
                    _dataLayer.CellData[i, j].Building = building;
                }
            }
            _dataLayer.ZoneBuildings.Add(building);
        }
        //y is the index of the row, x is the index of the column
        private bool Fits(int y, int x, Zone zone, Point point)
        {
            for (int i = y-1; i>=0; i--)
            {
                for (int j = x-1; j>=0; j--)
                {
                    if (i+point.Y<0 || i+point.Y>=_dataLayer.MapHeight || j+point.X<0 || j+point.X>=_dataLayer.MapWidth  || (!(_dataLayer.CellData[i+point.Y,j+point.X].Building is null && _dataLayer.CellData[i+point.Y,j+point.X].Zone == zone)))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        private void OnFireExtinguished(object sender, EventArgs e)
        {
            FireWasExtingushed?.Invoke(this, EventArgs.Empty);
        }

        public void SubscribeToBuildindEvents(FireStation station) {
            station.FireWasExtinguishedEvent += OnFireExtinguished;
        }
    }
}
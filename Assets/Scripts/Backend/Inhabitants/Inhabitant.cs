﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Backend.Inhabitants
{
    [Serializable]
    public class Inhabitant
    {
        private Point? _home;
        private Point? _workPlace;
        private double _satisfaction;
        private Qualification _qualification;
        private string _name;
        private int _age;
        private int _distanceFromWorkPlace;
        private List<int>? _salaries;
        private double? _pension;
        private bool _isPensioner;
        private int _madForWorkplaceLossDays;
        private int _madForHouseLossDays;
        private int _dissatisfiedDays;

        public int DissatisfiedDays
        {
            get { return _dissatisfiedDays;}
        }

        public int DistanceFromWorkplace
        {
            get
            {
                if (_workPlace is null || _home is null)
                {
                    return Int32.MaxValue;
                }
                else
                {
                    return _distanceFromWorkPlace;
                }
            }
            set
            {
                _distanceFromWorkPlace = value;
            }
        }

        public string Name
        {
            get { return _name; }
        }

        public Point? Home
        {
            get 
            { 
                if (_home is not null)
                {
                    return _home.Value;
                }
                else
                {
                    return null;
                }
            }

            set { _home = value; }
        }

        public Point? WorkPlace
        {
            get 
            { 
                if (_workPlace is not null)
                {
                    return _workPlace.Value;
                }
                else
                {
                    return null;
                }
            }
            set { _workPlace = value; }
        }

        public double Satisfaction
        {
            get { return _satisfaction; }
            set
            {
                _satisfaction = value;
            }
        }

        public Qualification Qualification
        {
            get { return _qualification; }
            set { _qualification = value; }
        }

        public int MadForWorkplaceLossDays
        {
            get { return _madForWorkplaceLossDays; }
            set { _madForWorkplaceLossDays = value; }
        }

        public int MadForHouseLossDays
        {
            get { return _madForHouseLossDays; }
            set { _madForHouseLossDays = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public int CurrentIncome
        {
            get { 
                    if (WorkPlace == null)
                    {
                        return 0;
                    }
                    return ((int)_qualification) * 150; 
            }
        }

        public bool IsPensioner
        {
            get { return _isPensioner; }
        }

        public double? Pension 
        {
            get { return _pension; }
        }

        public List<int> Salaries
        {
            get { return _salaries; }
        }

        public Inhabitant(int age)
        {

            Random rnd = new Random();
            _name = NameData.FirstNames[rnd.Next(0,NameData.FirstNames.Count)] + " " + NameData.LastNames[rnd.Next(0,NameData.LastNames.Count)];
            _home = null;
            _workPlace = null;
            _age = age;
            _qualification = Qualification.PrimaryEducation;
            _satisfaction = 0.5;
            _salaries = new List<int>();
            _pension = null;
            _isPensioner = false;
            _madForWorkplaceLossDays = 0;
            _madForHouseLossDays = 0;
            _distanceFromWorkPlace = Int32.MaxValue;
        }

        public void TransformIntoPensioner()
        {
           //MAKE SURE THAT THE SALARIES LIST CANNOT BE EMPTY AT THIS POINT (aging BEFORE moving in new inhabitants)
            _workPlace = null;
            _qualification = Qualification.PrimaryEducation;
            var salariesToBeAveraged = _salaries.Skip(Math.Max(0, _salaries.Count() - 20)).ToList();
            _pension = Math.Floor((double)(salariesToBeAveraged.Sum(x => x)/salariesToBeAveraged.Count) * 0.5);
            _salaries.Clear();
            _salaries = null;
            _isPensioner = true;
        }
        public void SetDissatisfiedDays()
        {
            if ((WorkPlace is null) && (Home is null))
            {
                _dissatisfiedDays++;
            }
            else if (Satisfaction <= 0.15)
            {
                _dissatisfiedDays++;
            }
            else
            {
                _dissatisfiedDays = 0;
            }
        }
    }
}

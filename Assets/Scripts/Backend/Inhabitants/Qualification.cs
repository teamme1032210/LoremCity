﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Inhabitants
{
    [Serializable]
    public enum Qualification
    {
        PrimaryEducation = 1,
        SecondaryEducation = 2,
        HigherEducation = 3,
    }
}

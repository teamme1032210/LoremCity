using System.Collections.Generic;

namespace Backend.Inhabitants
{
    public static class NameData
    {
        public static List<string> FirstNames = new List<string>()
        {
        "Ruby", "Oscar", "Aurora", "Elijah", "Ava", "Carter", "Penelope", "Xavier", "Layla", "Micah",
        "Sadie", "Hunter", "Grace", "Leo", "Mia", "Gideon", "Chloe", "Wyatt", "Lily", "Mason",
        "Scarlett", "Isaac", "Hannah", "Jasper", "Hazel", "Samuel", "Nova", "Silas", "Maya", "Felix",
        "Abigail", "Levi", "Zoe", "Kai", "Eleanor", "Oliver", "Amelia", "Logan", "Adelaide", "Jude",
        "Ruby", "Eliana", "Asher", "Aria", "Sebastian", "Ivy", "Gabriel", "Aurora", "Jonah", "Alice",
        "Ezra", "Stella", "Isaiah", "Willow", "Ezra", "Grace", "Elias", "Emma", "Ezekiel", "Ellie",
        "Xavier", "Luna", "Nolan", "Lucy", "Wyatt", "Sophie", "Abraham", "Violet", "Lucas", "Audrey",
        "Caleb", "Elsie", "Andrew", "Hazel", "Grayson", "Clara", "Elijah", "Piper", "Levi", "Rose", "Oliver", "Cora", "Miles", "Eleanor", "Nathan",
        "Sadie", "Roman", "Aurora", "Isaac", "Iris", "Lincoln", "Ava", "Joshua", "Julia", "Samuel",
        "Lila", "Luke", "Mia", "Caleb", "Nora"
        };

        public static List<string> LastNames = new List<string>()
        {
            "Reynolds", "Bridges", "Kim", "Thornton", "Perez",
            "Green", "Warner", "Vasquez", "Woods", "Castro",
            "Kramer", "Duncan", "Cross", "Macias", "Torres",
            "Vasquez", "Peters", "Burton", "Poole", "Leach",
            "Holland", "Lowe", "Pearson", "Clayton", "Gomez",
            "Willis", "Austin", "Williams", "Cummings", "Black",
            "Stokes", "Mendoza", "Rhodes", "Ingram", "Cortez",
            "Tran", "Simon", "Lambert", "Mcintosh", "Hartman",
            "Brady", "Sanders", "Mcbride", "Mack", "Ritter",
            "Gregory", "Moody", "Wilkerson", "Carey", "Dickson",
            "Potts", "Peck", "Miranda", "Byrd", "Higgins",
            "Sloan", "Hunter", "Charles", "Knox", "Chaney",
            "Harrell", "Ellison", "Pitts", "Yoder", "Huerta",
            "Blackwell", "Gaines", "Dickerson", "Benson", "Gallegos", "Pham",
            "Chan", "Soto", "West", "Rubio", "Faulkner",
            "Cummings", "Kim", "Puckett", "Reyes", "Ortiz",
            "Case", "Velazquez", "Bradshaw", "Boyle", "Shea",
            "Meza", "Crane", "Mcknight", "Mcclain", "Hutchinson",
            "Caldwell", "Mcgee", "Callahan", "Cherry", "Costa",
            "Davenport", "Mathis", "Pittman", "Wolf"
        };
    }
}

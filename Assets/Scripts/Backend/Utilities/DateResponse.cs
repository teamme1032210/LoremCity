﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Utilities
{
    [Serializable]
    public class DateResponse
    {
        private int _year;
        private int _month;
        private int _day;

        public DateResponse(int year, int month, int day)
        {
            _year = year;
            _month = month;
            _day = day;
        }

        public int Year
        {
            get { return _year; }
        }
        public int Month
        {
            get { return _month; }
        }
        public int Day
        {
            get { return _day; }
        }
    }
}

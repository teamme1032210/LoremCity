using Backend.Buildings.Interfaces;
using Backend.Utilities;

namespace Util {
    public static class PowerHelper {
        public static int GetPowerConsumption(INeedPower consumer) {
            if (consumer.Network == null) return 0;
            return consumer.Network.AvailablePower >= 0 ? consumer.PowerNeeded : 0;
        }
        public static bool HasEnoughPower(INeedPower consumer) {
            if (consumer.Network == null) return false;
            return consumer.Network.AvailablePower >= 0;
        }

        public static void ResetWaked(IPowerBuilding building) {
            building.Walked = false;
        }

        public static bool IsUnderPower(INeedPower consumer){
            return consumer.Network != null;
        }
    }
}
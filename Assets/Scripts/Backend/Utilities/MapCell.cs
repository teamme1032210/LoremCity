﻿using Backend.Buildings;
using Backend.Zones;
using Backend.Inhabitants;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Backend.Utilities
{
    public class MapCell
    {
        public static readonly int DefaultCapacity = 5;
        public MapCell()
        {
            HasFirestationNearby = false;
            HasStadiumNearby = false;
            HasPolicestationNearby = false;
            CanSeeForest = false;
            WillSeeForestIfBuildsHere = false;
            IndustrialDissatisfactionReducedByForest = false;
            TotalLevelOfForestsInSight = 0;

            RelatedInhabitants = new List<Inhabitant>();
        }

        public Zone Zone
        { get; set; }

        public Building? Building
        { get; set; }

        public List<Inhabitant> RelatedInhabitants
        {
            get; set;
        }

        public bool HasFirestationNearby
        {
            get; set;
        }

        public bool HasPolicestationNearby
        {
            get; set;
        }

        public bool HasStadiumNearby
        {
            get; set;
        }
        public bool CanSeeForest
        {
            get; set;
        }
        public bool WillSeeForestIfBuildsHere
        {
            get; set;
        }
        public bool IndustrialDissatisfactionReducedByForest
        {
            get; set;
        }
        public bool HasIndustrialBuildingNearby
        {
            get; set;
        }
        public int TotalLevelOfForestsInSight
        {
            get; set;
        }
    }
}

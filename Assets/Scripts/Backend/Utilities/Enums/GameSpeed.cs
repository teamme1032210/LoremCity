using System;

namespace Backend.Utilities.Enums
{
    [Serializable]
    public enum GameSpeed
    {
        Normal = 1,
        Fast = 2,
        Insane = 3,
    }
}
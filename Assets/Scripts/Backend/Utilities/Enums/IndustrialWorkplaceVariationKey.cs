﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Utilities.Enums
{
    public enum IndustrialWorkplaceVariationKey
    {
        ResidentialAreaIsCreated,
        ServiceAreaIsCreated,
        IndustrialAreaIsCreated,
        ZoneTypeSetBackToDefault,
        StadiumIsBuilt,
        StadiumMaintenance,
        StadiumIsDestroyed,
        PoliceStationIsBuilt,
        PoliceStationMaintenance,
        PoliceStationIsDestroyed,
        FireStationIsBuilt,
        FireStationMaintenance,
        FireStationIsDestroyed,
        RoadIsBuilt,
        RoadMaintenance,
        RoadIsDestroyed,
        SchoolIsBuilt,
        SchoolMaintenance,
        SchoolIsDestroyed,
        UniversityIsBuilt,
        UniversityMaintenance,
        UniversityIsDestroyed,
        ServiceUpgradedToLevel2,
        ServiceUpgradedToLevel3,
        IndustrialUpgradedToLevel2,
        IndustrialUpgradedToLevel3,
        ResidentialUpgradedToLevel2,
        ResidentialUpgradedToLevel3,
        Compensation,
        ForestIsCreated,
        ForestMaintenance,
        ForestIsDestroyed,
    }
}

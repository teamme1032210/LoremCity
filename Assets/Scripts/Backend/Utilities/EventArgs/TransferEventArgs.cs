﻿using Backend.GlobalEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Utilities.CustomEventArgs
{
    public class TransferEventArgs : EventArgs
    {
        private ExpenseType _expenseType;

        public ExpenseType ExpenseType
        {
            get { return _expenseType; }
        }

        public TransferEventArgs(ExpenseType expenseType)
        {
            _expenseType = expenseType;
        }
    }
}

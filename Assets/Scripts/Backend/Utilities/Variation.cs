﻿using System;

namespace Backend.Utilities
{
    [Serializable]
    public class Variation
    {
        private int _sizeX;
        private int _sizeY;
        private int _textureId;

        public int SizeX
        {
            get { return _sizeX; }
        }

        public int SizeY
        {
            get { return _sizeY; }
        }

        public int TextureId
        {
            get { return _textureId; }
        }


        public Variation(int sizeX, int sizeY, int textureId)
        {
            _sizeX = sizeX;
            _sizeY = sizeY;
            _textureId = textureId;
        }
    }
}

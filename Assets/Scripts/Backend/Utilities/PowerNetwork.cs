using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Buildings;
using Backend.Buildings.Interfaces;

namespace Backend.Utilities
{
    [Serializable]
    public class PowerNetwork
    {
        [NonSerialized]
        List<PowerLine> _powerLines;
        [NonSerialized]
        List<PowerPlant> _powerPlants;
        [NonSerialized]
        List<INeedPower> _powerConsumers;

        public PowerNetwork()
        {
            _powerLines = new List<PowerLine>();
            _powerPlants = new List<PowerPlant>();
            _powerConsumers = new List<INeedPower>();
        }
        
        public int ProducedPower
        {
            get => _powerPlants.Aggregate(0, (sum, plant) => sum + plant.Capacity);
        }
        
        public int NecessaryPower
        {
            get => _powerConsumers.Aggregate(0, (sum, consumer) => sum + consumer.PowerNeeded);
        }
        
        public int AvailablePower
        {
            get => ProducedPower - NecessaryPower;
        }
        
        public List<PowerPlant> PowerPlants
        {
            get => _powerPlants;
        }
        
        public List<PowerLine> PowerLines
        {
            get => _powerLines;
        }
        
        public List<INeedPower> PowerConsumers
        {
            get => _powerConsumers;
        }

        public void AddLine(PowerLine line)
        {
            _powerLines.Add(line);
        }

        public void AddPlant(PowerPlant plant)
        {
            _powerPlants.Add(plant);
        }
        
        public void AddConsumer(INeedPower consumer)
        {
            _powerConsumers.Add(consumer);
        }
    }
}
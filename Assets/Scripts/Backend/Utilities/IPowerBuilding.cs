using System.Collections.Generic;
using System.Drawing;

namespace Backend.Utilities
{
    public interface IPowerBuilding {
        public bool Walked { get; set; }

        public int GetPointCount();
        public Point GetPoint(int index);
    }
}
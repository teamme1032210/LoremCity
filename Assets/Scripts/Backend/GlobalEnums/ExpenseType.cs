﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.GlobalEnums
{
    public enum ExpenseType
    {
        Level1ResidentialFieldIsSet,
        Level1ServiceFieldIsSet,
        Level1IndustrialFieldIsSet,
        Level1ResidentialSetBackToDefault,
        Level1ServiceSetBackToDefault,
        Level1IndustrialSetBackToDefault,
        Level2ResidentialFieldIsSet,
        Level2ServiceFieldIsSet,
        Level2IndustrialFieldIsSet,
        Level2ResidentialSetBackToDefault,
        Level2ServiceSetBackToDefault,
        Level2IndustrialSetBackToDefault,
        Level3ResidentialFieldIsSet,
        Level3ServiceFieldIsSet,
        Level3IndustrialFieldIsSet,
        Level3ResidentialSetBackToDefault,
        Level3ServiceSetBackToDefault,
        Level3IndustrialSetBackToDefault,
        StadiumIsBuilt,
        StadiumMaintenance,
        StadiumIsDestroyed,
        PoliceStationIsBuilt,
        PoliceStationMaintenance,
        PoliceStationIsDestroyed,
        FireStationIsBuilt,
        FireStationMaintenance,
        FireStationIsDestroyed,
        RoadIsBuilt,
        RoadMaintenance,
        RoadIsDestroyed,
        SchoolIsBuilt,
        SchoolMaintenance,
        SchoolIsDestroyed,
        UniversityIsBuilt,
        UniversityMaintenance,
        UniversityIsDestroyed,
        ServiceUpgradedToLevel2,
        ServiceUpgradedToLevel3,
        IndustrialUpgradedToLevel2,
        IndustrialUpgradedToLevel3,
        ResidentialUpgradedToLevel2,
        ResidentialUpgradedToLevel3,
        Compensation,
        ForestIsCreated,
        ForestMaintenance,
        ForestIsDestroyed,
        PowerplantIsBuilt,
        PowerplantMaintenance,
        PowerplantIsDestroyed,
        PowerLineIsBuilt,
        PowerLineMaintenance,
        PowerLineIsDestroyed,

    }
}

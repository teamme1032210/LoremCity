using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Backend.Buildings;
using Backend.Data;
using Backend.Zones;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Util;

public class ElectricityTest
{
    [Test]
    public void SinglePowerPlantTest()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
    }
    
    [Test]
    public void MultiplePowerPlantTest()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerPlant(new Point(5, 5));
        Assert.AreEqual(gameData.PowerNetworks.Count, 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[1].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[1].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[1].PowerConsumers.Count, 0);
    }

    [Test]
    public void SinglePowerPlantTestWithPolesTooFar()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerLine(new Point(5, 5));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
    }
    
    [Test]
    public void SinglePowerPlantTestWithPolesCloseEnough()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerLine(new Point(2, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
    }
    
    [Test]
    public void SinglePowerPlantTestWithMultiplePolesCloseEnough()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerLine(new Point(2, 0));
        gameData.FactoryLayer.BuildPowerLine(new Point(0, 2));
        gameData.FactoryLayer.BuildPowerLine(new Point(2, 2));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 3);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
    }
    
    [Test]
    public void MultiplePowerPlantTestCloseEnough()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerPlant(new Point(2, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
    }
    
    [Test]
    public void MultiplePowerPlantTestConnectedWithPowerLines()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerPlant(new Point(3, 0));
        gameData.FactoryLayer.BuildPowerLine(new Point(2, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
    }
    
    [Test]
    public void MultiplePowerPlantTestConnectedWithZone()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerPlant(new Point(3, 0));
        HWMat<int> zone = new HWMat<int>(11, 26);
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 26; j++) {
                zone[i, j] = 0;
            }
        }
        zone[2, 0] = 1;
        zone[2, 1] = 1;
        zone[2, 2] = 1;
        zone[2, 3] = 1;
        gameData.FactoryLayer.RearrangeZoneLayout(zone,new List<Home>(), new List<Workplace>());
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 1);
    }
    
    [Test]
    public void MultiplePowerPlantTestConnectedWithManyLines()
    {
        GameData gameData = new GameData(25, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        gameData.FactoryLayer.BuildPowerPlant(new Point(23, 23));
        int count = 0;
        for (int i = 2; i < 23; i += 2) {
            gameData.FactoryLayer.BuildPowerLine(new Point(i, i));
            count++;
        }
        gameData.FactoryLayer.BuildPowerLine(new Point(0, 2));
        gameData.FactoryLayer.BuildPowerLine(new Point(22, 23));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, count + 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 0);
    }
    
    [Test] 
    public void SinglePowerPlantTestWithConnectedZones()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        HWMat<int> zone = new HWMat<int>(11, 26);
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 26; j++) {
                zone[i, j] = 0;
            }
        }
        zone[2, 0] = 1;
        zone[2, 1] = 1;
        zone[2, 2] = 1;
        zone[2, 3] = 1;
        zone[3, 0] = 2;
        zone[3, 1] = 2;
        zone[3, 2] = 2;
        zone[3, 3] = 2;
        gameData.FactoryLayer.RearrangeZoneLayout(zone,new List<Home>(), new List<Workplace>());
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 2);
        for (int i = 0; i < gameData.Zones.Count; i++) {
            if (gameData.Zones[i].ZoneType != ZoneType.Special) {
                Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Zones[i]));
            }
        }
    }
    
    [Test] 
    public void SinglePowerPlantTestWithConnectedZonesBySinglePowerLine()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        HWMat<int> zone = new HWMat<int>(11, 26);
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 26; j++) {
                zone[i, j] = 0;
            }
        }
        zone[2, 0] = 1;
        zone[2, 1] = 1;
        zone[2, 2] = 1;
        zone[2, 3] = 1;
        zone[4, 0] = 2;
        zone[4, 1] = 2;
        zone[4, 2] = 2;
        zone[4, 3] = 2;
        gameData.FactoryLayer.RearrangeZoneLayout(zone,new List<Home>(), new List<Workplace>());
        gameData.FactoryLayer.BuildPowerLine(new Point(0, 3));
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 2);
        for (int i = 0; i < gameData.Zones.Count; i++) {
            if (gameData.Zones[i].ZoneType != ZoneType.Special) {
                Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Zones[i]));
            }
        }
    }
    
    [Test] 
    public void SinglePowerPlantTestWithConnectedZonesByMultiplePowerLines()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        HWMat<int> zone = new HWMat<int>(11, 26);
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 26; j++) {
                zone[i, j] = 0;
            }
        }
        zone[2, 0] = 1;
        zone[2, 1] = 1;
        zone[2, 2] = 1;
        zone[2, 3] = 1;
        zone[6, 0] = 2;
        zone[6, 1] = 2;
        zone[6, 2] = 2;
        zone[6, 3] = 2;
        gameData.FactoryLayer.RearrangeZoneLayout(zone,new List<Home>(), new List<Workplace>());
        gameData.FactoryLayer.BuildPowerLine(new Point(0, 3));
        gameData.FactoryLayer.BuildPowerLine(new Point(0, 5));
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 2);
        for (int i = 0; i < gameData.Zones.Count; i++) {
            if (gameData.Zones[i].ZoneType != ZoneType.Special) {
                Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Zones[i]));
            }
        }
    }
    
    [Test] 
    public void SinglePowerPlantTestWithConnectedZonesByMultiplePowerLinesThatAreTakenAway()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        HWMat<int> zone = new HWMat<int>(11, 26);
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 26; j++) {
                zone[i, j] = 0;
            }
        }
        zone[2, 0] = 1;
        zone[2, 1] = 1;
        zone[2, 2] = 1;
        zone[2, 3] = 1;
        zone[6, 0] = 2;
        zone[6, 1] = 2;
        zone[6, 2] = 2;
        zone[6, 3] = 2;
        gameData.FactoryLayer.RearrangeZoneLayout(zone,new List<Home>(), new List<Workplace>());
        gameData.FactoryLayer.BuildPowerLine(new Point(0, 3));
        gameData.FactoryLayer.BuildPowerLine(new Point(0, 5));
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 2);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 2);
        for (int i = 0; i < gameData.Zones.Count; i++) {
            if (gameData.Zones[i].ZoneType != ZoneType.Special) {
                Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Zones[i]));
            }
        }
        gameData.FactoryLayer.DestroyPowerLine(gameData.CellData[5, 0].Building as PowerLine);
        Assert.IsFalse(PowerHelper.IsUnderPower(gameData.CellData[6, 0].Zone));
    }
    
    [Test] 
    public void SinglePowerPlantTestWithConnectedZoneAndSpecialBuilding()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        HWMat<int> zone = new HWMat<int>(11, 26);
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 26; j++) {
                zone[i, j] = 0;
            }
        }
        zone[2, 0] = 1;
        zone[2, 1] = 1;
        zone[2, 2] = 1;
        zone[2, 3] = 1;
        gameData.FactoryLayer.RearrangeZoneLayout(zone,new List<Home>(), new List<Workplace>());
        gameData.FactoryLayer.BuildStadium(new Point(3, 0));
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 2);
        for (int i = 0; i < gameData.Zones.Count; i++) {
            if (gameData.Zones[i].ZoneType != ZoneType.Special) {
                Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Zones[i]));
            }
        }
        Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Stadiums[0]));
    }
    
    [Test] 
    public void SinglePowerPlantTestWithConnectedSpecialBuildings()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        RemoveAllTrees(gameData);
        gameData.FactoryLayer.BuildStadium(new Point(2, 0));
        gameData.FactoryLayer.BuildStadium(new Point(4, 0));
        gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
        Assert.AreEqual(gameData.PowerNetworks.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 0);
        Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 2);
        Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Stadiums[0]));
        Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Stadiums[1]));
    }
    
    [Test]
    public void SinglePowerPlantTestWithMultipleConnectedZonesByMultiplePowerLinesThatAreTakenAway()
        {
            GameData gameData = new GameData(50, 62, 1, System.DateTime.Now, "Test");
            gameData.StartNewGame();
            RemoveAllTrees(gameData);
            HWMat<int> zone = CreateEmptyZoneArray(50, 62);
            for (int i = 0; i < 6; i++) {
                zone[i * 4 + 2, 1] = 1;
                zone[i * 4 + 2, 2] = 1;
                zone[i * 4 + 2, 3] = 1;
                zone[i * 4 + 2, 4] = 1;
                gameData.FactoryLayer.BuildPowerLine(new Point(5, i*4 + 2));
                gameData.FactoryLayer.BuildPowerLine(new Point(5, i*4 + 4));
            }
            gameData.FactoryLayer.RearrangeZoneLayout(zone,new List<Home>(), new List<Workplace>());
            gameData.FactoryLayer.BuildPowerPlant(new Point(0, 0));
            Assert.AreEqual(gameData.PowerNetworks.Count, 1);
            Assert.AreEqual(gameData.PowerNetworks[0].PowerPlants.Count, 1);
            Assert.AreEqual(gameData.PowerNetworks[0].PowerLines.Count, 12);
            Assert.AreEqual(gameData.PowerNetworks[0].PowerConsumers.Count, 6);
            for (int i = 0; i < gameData.Zones.Count; i++) {
                if (gameData.Zones[i].ZoneType != ZoneType.Special) {
                    Assert.IsTrue(PowerHelper.IsUnderPower(gameData.Zones[i]));
                }
            }
            gameData.FactoryLayer.DestroyPowerLine(gameData.CellData[10, 5].Building as PowerLine);
            for (int i = 0; i < 6; i++) {
                if (gameData.Zones[i].ZoneType != ZoneType.Special) {
                    Assert.IsTrue(PowerHelper.IsUnderPower(gameData.CellData[i*4 + 2, 1].Zone) == i < 2);
                }
            }
        }
    
    private void RemoveAllTrees(GameData gameData)
    {
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }
    }

    private HWMat<int> CreateEmptyZoneArray(int h, int w) {
        HWMat<int> zone = new HWMat<int>(h, w);
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                zone[i, j] = 0;
            }
        }
        return zone;
    }

}

using Backend.Buildings;
using System;
using Backend.Data;
using Backend.GlobalEnums;
using Backend.Inhabitants;
using Backend.Utilities;
using Backend.Zones;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Policy;
using Unity.Collections.LowLevel.Unsafe;
using Util;
using Backend.Utilities.Enums;
using UnityEditor.VersionControl;
using UnityEngine;
using NUnit.Framework.Constraints;

public class GameDataTests
{
    // A Test behaves as an ordinary method
    [Test]
    public void GameDataTestsSimplePasses()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        Assert.AreEqual(gameData.MapHeight, 11);
        Assert.AreEqual(gameData.MapWidth, 26);
        Assert.AreEqual(gameData.Roads[0],gameData.CellData[gameData.firstPieceOfRoad.Y,gameData.firstPieceOfRoad.X].Building);
        Assert.IsNotNull(MapCell.DefaultCapacity);
        Assert.AreEqual(MapCell.DefaultCapacity, 5);
        Assert.AreEqual(gameData.CityName, "Test");
        gameData.IsPaused = true;
        Assert.AreEqual(gameData.IsPaused, true);
        gameData.GameSpeed = GameSpeed.Normal;
        Assert.AreEqual(gameData.GameSpeed, GameSpeed.Normal);
        Assert.IsNotNull(gameData.SalaryDeductionPercentage);
        Assert.IsNotNull(gameData.Satisfaction);
        Assert.IsNotNull(gameData.TotalPowerNeeded);
        Assert.IsNotNull(gameData.TotalPowerCapacity);
        Assert.IsEmpty(gameData.WorklessInhabitants);
        Assert.IsEmpty(gameData.HomelessInhabitants);
        gameData.SalaryDeductionPercentage = 20;
        Assert.AreEqual(gameData.SalaryDeductionPercentage, 20);
        Assert.Throws<InvalidOperationException>(() => gameData.SalaryDeductionPercentage = 110);
        Assert.IsNotNull(NameData.LastNames);
        Assert.IsNotNull(NameData.FirstNames);
        Assert.IsNotNull(gameData.GetDate());
        Assert.IsNotNull(gameData.GetQuarterExpenseOfCity());
        Assert.IsNotNull(gameData.GetQuarterExpenseOfCity());
    }

    [Test]
    public void RoadTests()
    {
        GameData gameData = new GameData(11, 26, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }
        Assert.AreEqual(0, gameData.Forests.Count);
        Assert.AreEqual(gameData.Roads.Count, 1);
        Assert.AreEqual(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X].Building, gameData.Roads[0]);
        gameData.FactoryLayer.BuildRoad(new System.Drawing.Point(gameData.firstPieceOfRoad.X+2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(gameData.Roads.Count, 2);
        Assert.IsTrue(gameData.Roads[0].IsActive);
        Assert.IsFalse(gameData.Roads[1].IsActive);
        Assert.AreEqual(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X+2].Building, gameData.Roads[1]);
        gameData.FactoryLayer.BuildRoad(new System.Drawing.Point(gameData.firstPieceOfRoad.X+1, gameData.firstPieceOfRoad.Y));
        Assert.IsTrue(gameData.Roads[2].IsActive);
        Assert.IsTrue(gameData.Roads[1].IsActive);
        Assert.AreEqual(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 1].Building, gameData.Roads[2]);
        gameData.FactoryLayer.DestroyRoad(gameData.Roads[2],false);
        Assert.AreEqual(gameData.Roads.Count, 2);
        Assert.IsFalse(gameData.Roads[1].IsActive);
        Assert.AreEqual(gameData.GetExpense(gameData.Roads[1].MaintenanceExpenseType), gameData.GetExpense(ExpenseType.RoadMaintenance));
        foreach (var road in gameData.Roads)
        {
            Assert.IsNotNull(road.BuildDate);
        }
    }

    [Test]
    public void PowerLineTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count>0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }
        Assert.AreEqual(0,gameData.Forests.Count);
        gameData.FactoryLayer.BuildPowerLine(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        foreach (var powerLine in gameData.PowerLines)
        {
            Assert.AreEqual(powerLine.MaintenanceExpenseType, ExpenseType.PowerLineMaintenance);
            Assert.IsNotNull(gameData.GetExpense(powerLine.MaintenanceExpenseType));
        }
        Assert.AreEqual(gameData.PowerLines[0].BuildDate.Year,gameData.GetGameDate().Year);
        Assert.AreEqual(gameData.PowerLines[0].BuildDate.Month, gameData.GetGameDate().Month);
        Assert.AreEqual(gameData.PowerLines[0].BuildDate.Day, gameData.GetGameDate().Day);
    }

    [Test]
    public void ForestTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
   
        foreach (Forest forest in gameData.Forests)
        {
            Assert.AreEqual(forest.ForestLevel, 10);
        }

        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }
        Assert.AreEqual(0, gameData.Forests.Count);
        gameData.FactoryLayer.BuildForest(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(0, gameData.Forests[0].ForestLevel);
        foreach (var forest in gameData.Forests)
        {
            Assert.AreEqual(forest.MaintenanceExpenseType, ExpenseType.ForestMaintenance);
            Assert.IsNotNull(gameData.GetExpense(forest.MaintenanceExpenseType));
        }
    }

    [Test]
    public void StadiumTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }

        Assert.AreEqual(0, gameData.Stadiums.Count);
        gameData.FactoryLayer.BuildStadium(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(1, gameData.Stadiums.Count);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        DateResponse built = new DateResponse(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.FactoryLayer.BuildStadium(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        gameData.FactoryLayer.DestoryStadium(gameData.Stadiums[0]);
        Assert.AreEqual(1,gameData.Stadiums.Count);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 5].Building);
        Assert.AreEqual(gameData.Stadiums[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 5].Building);
        List<Point> cells = new List<Point>();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y + 1));
        gameData.Zones.Add(new Backend.Zones.Zone(ZoneType.Residential, cells, new List<ZoneBuilding>()));
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Zone.ZoneType);
        cells.Clear();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y + 1));
        HWMat<int> matrix = new HWMat<int>(25,25);
        for (int i = 0; i < 25; i++)
        {
            for (int j = 0; j < 25; j++)
            {
                matrix[i,j] = 0;
            }
        }
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3] = 1;
        Assert.IsNotNull(gameData.FactoryLayer.ValidateZoneArrangement(matrix));
        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Zone.ZoneType);
        foreach (var stadium in gameData.Stadiums)
        {
            Assert.AreEqual(PowerHelper.GetPowerConsumption(stadium), 0);
            Assert.False(stadium.IsOnFire);
            Assert.IsNotNull(stadium.PowerNeeded);
        }
        Assert.False(gameData.Stadiums[0].IsOnFireSince.HasValue);
        gameData.Stadiums[0].IsOnFire = true;
        DateTime since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.Stadiums[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        for (int i = 0; i < 10; i++)
        {
            gameData.Tick();
        }
        Assert.AreEqual(since, gameData.Stadiums[0].IsOnFireSince);
        gameData.Stadiums[0].HasDispatchedTruck = true;
        Assert.IsTrue(gameData.Stadiums[0].HasDispatchedTruck);
        Assert.IsNotNull(gameData.GetExpense(gameData.Stadiums[0].MaintenanceExpenseType));
        Assert.AreEqual(gameData.Stadiums[0].MaintenanceExpenseType,ExpenseType.StadiumMaintenance);
        Assert.AreEqual(gameData.Stadiums[0].BuildDate.Year,built.Year);
        Assert.AreEqual(gameData.Stadiums[0].BuildDate.Month, built.Month);
        Assert.AreEqual(gameData.Stadiums[0].BuildDate.Day, built.Day);
    }

    [Test]
    public void ZoneTests()
    {
        GameData gameData = new GameData(50, 50, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }
        Assert.AreEqual(50,gameData.MapHeight);
        Assert.AreEqual(50,gameData.MapWidth);
        Assert.AreEqual(gameData.Roads[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X].Building);
        Assert.AreEqual(1, gameData.Roads.Count);
        Assert.AreEqual(0, gameData.Forests.Count);
        Assert.AreEqual(1, gameData.Zones.Count);
        Assert.AreEqual(ZoneType.Special, gameData.Zones[0].ZoneType);

        HWMat<int> matrix = new HWMat<int>(50, 50);
        for (int i = 0; i < 50; i++)
        {
            for (int j = 0; j < 50; j++)
            {
                if (i > gameData.firstPieceOfRoad.Y)
                {
                    matrix[i, j] = 1;
                }
                else if (i < gameData.firstPieceOfRoad.Y)
                {
                    matrix[i, j] = 2;
                }
                else
                {
                    matrix[i, j] = 0;
                }
            }
        }
        matrix[matrix.Height - 1, matrix.Width - 1] = 3;
        matrix[matrix.Height - 2, matrix.Width - 1] = 3;
        matrix[matrix.Height - 1, matrix.Width - 2] = 3;
        matrix[matrix.Height - 2, matrix.Width - 2] = 3;
        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y,5].Zone.ZoneType);
        Assert.AreEqual(4, gameData.Zones.Count);
        Assert.AreEqual(ZoneType.Industrial, gameData.CellData[gameData.firstPieceOfRoad.Y - 2, 5].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 2, 5].Zone.ZoneType);
        Assert.IsTrue(gameData.Zones[1].IsActive);
        Assert.IsTrue(gameData.Zones[2].IsActive);
        Assert.AreEqual(ZoneType.Service,gameData.CellData[gameData.MapHeight-1,gameData.MapWidth-1].Zone.ZoneType);
        Assert.IsFalse(gameData.CellData[gameData.MapHeight - 1, gameData.MapWidth - 1].Zone.IsActive);
        matrix[0, 0] = 1;
        try
        {
            gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
            Assert.Fail();
        }
        catch (Exception)
        {
            
        }
        Assert.AreEqual(ZoneType.Industrial, gameData.CellData[0, 0].Zone.ZoneType);
        Assert.AreEqual(1, gameData.Zones[1].CurrentLevel);
    }

    [Test]
    public void InhabitantsAndZoneBuildingsTests()
    {
        GameData gameData = new GameData(50, 50, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }
        Assert.AreEqual(50, gameData.MapHeight);
        Assert.AreEqual(50, gameData.MapWidth);
        Assert.AreEqual(gameData.Roads[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X].Building);
        Assert.AreEqual(1, gameData.Roads.Count);
        Assert.AreEqual(0, gameData.Forests.Count);
        Assert.AreEqual(1, gameData.Zones.Count);
        Assert.AreEqual(ZoneType.Special, gameData.Zones[0].ZoneType);

        HWMat<int> matrix = new HWMat<int>(50, 50);
        for (int i = 0; i < 50; i++)
        {
            for (int j = 0; j < 50; j++)
            {
                if (i > gameData.firstPieceOfRoad.Y && i < gameData.firstPieceOfRoad.Y + 10 && j >= gameData.firstPieceOfRoad.X && j < gameData.firstPieceOfRoad.X + 10)
                {
                    matrix[i, j] = 1;
                }
                else if (i < gameData.firstPieceOfRoad.Y && i > gameData.firstPieceOfRoad.Y - 10 && j >= gameData.firstPieceOfRoad.X && j < gameData.firstPieceOfRoad.X + 10)
                {
                    matrix[i, j] = 2;
                }
                else
                {
                    matrix[i, j] = 0;
                }
            }
        }
        for (int j = 1; j < 5; j++)
        {
            matrix[gameData.firstPieceOfRoad.Y, j] = 3;
        }

        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        gameData.FactoryLayer.BuildPowerPlant(new Point(gameData.firstPieceOfRoad.X+10, gameData.firstPieceOfRoad.Y+2));
        gameData.FactoryLayer.BuildPowerPlant(new Point(gameData.firstPieceOfRoad.X+10, gameData.firstPieceOfRoad.Y-2));
        Assert.AreEqual(4,gameData.Zones.Count);
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y, 5].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Industrial, gameData.CellData[gameData.firstPieceOfRoad.Y - 2, gameData.firstPieceOfRoad.X].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 2, gameData.firstPieceOfRoad.X].Zone.ZoneType);

        Assert.IsTrue(gameData.Zones[1].IsActive);
        Assert.IsTrue(gameData.Zones[2].IsActive);
        Assert.IsTrue(PowerHelper.HasEnoughPower(gameData.Zones[1]));
        Assert.IsTrue(PowerHelper.HasEnoughPower(gameData.Zones[2]));
        Assert.AreEqual(ZoneType.Residential,gameData.Zones[3].ZoneType);
        int inhabcount = gameData.Inhabitants.Count;

        for (int i = 0; i < 100; i++)
        {
            gameData.Tick();
            gameData.ZoneBuildings[0].IsOnFireSince = null;
            gameData.ZoneBuildings[0].IsOnFire = false;
            gameData.PowerPlants[1].IsOnFireSince = null;
            gameData.PowerPlants[1].IsOnFire = false;
            gameData.PowerPlants[0].IsOnFireSince = null;
            gameData.PowerPlants[0].IsOnFire = false;
        }
        Assert.Greater(gameData.Inhabitants.Count,inhabcount);
        Assert.Throws<ArgumentOutOfRangeException>(() => gameData.ZoneBuildings[0].GetPoint(gameData.ZoneBuildings[0].GetPointCount + 1));

        foreach (var inhabitant in gameData.Inhabitants)
        {
            Assert.Less(inhabitant.Age, 91);
            Assert.Greater(inhabitant.Age, 17);
            if (inhabitant.Age > 65)
            {
                Assert.True(inhabitant.IsPensioner);
            }
            if(inhabitant.IsPensioner)
            {
                Assert.IsNotNull(inhabitant.Pension);
            }
            if( inhabitant.WorkPlace != null && !inhabitant.IsPensioner)
            {
                Assert.Greater(inhabitant.CurrentIncome, 0);
            }
            if(inhabitant.WorkPlace is not null && inhabitant.Home is not null)
            {
                Assert.IsNotNull(inhabitant.DistanceFromWorkplace);
            }
        }
        foreach (var building in gameData.ZoneBuildings)
        {
            Assert.AreEqual(0,building.ActivePoliceStations);
            Assert.AreEqual(0, building.ActiveStadiums);
            Assert.False(building.HasFireStationNearby);
            Assert.True(gameData.Zones[1] == building.Zone || gameData.Zones[2] == building.Zone || gameData.Zones[3] == building.Zone);
        }
        gameData.ZoneBuildings[0].IsOnFireSince = null;
        gameData.ZoneBuildings[0].IsOnFire = false;
        gameData.ZoneBuildings[0].IsOnFire = true;
        gameData.ZoneBuildings[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        DateTime since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        
        for (int i = 0; i < 49; i++)
        {
            gameData.Tick();
        }
        Assert.AreEqual(since.Year, gameData.ZoneBuildings[0].IsOnFireSince.Value.Year);
        Assert.AreEqual(since.Month, gameData.ZoneBuildings[0].IsOnFireSince.Value.Month);
        Assert.AreEqual(since.Day,gameData.ZoneBuildings[0].IsOnFireSince.Value.Day);

        gameData.ZoneBuildings[0].IsOnFireSince = null;
        gameData.ZoneBuildings[0].IsOnFire = false;
        gameData.PowerPlants[1].IsOnFireSince = null;
        gameData.PowerPlants[1].IsOnFire = false;
        gameData.PowerPlants[0].IsOnFireSince = null;
        gameData.PowerPlants[0].IsOnFire = false;
        int f = 0;
        while (gameData.ZoneBuildings[f].Zone.ZoneType!=ZoneType.Residential)
        {
            f++;
        }
        Assert.True(gameData.ZoneBuildings[f].Zone.ZoneType == ZoneType.Residential);
        Assert.False(((Home)gameData.ZoneBuildings[f]).HasForestNearby);
        Assert.False(gameData.ZoneBuildings[f].HasFireStationNearby);
        gameData.ZoneBuildings[0].HasDispatchedTruck= true;
        Assert.True(gameData.ZoneBuildings[0].HasDispatchedTruck);
        Assert.IsNotNull(gameData.ZoneBuildings[0].Variation.TextureId);
        Assert.IsNotNull(gameData.Zones[0].Capacity);
        Assert.AreEqual(NameData.FirstNames[1], "Oscar");
        Assert.AreEqual(NameData.LastNames[2], "Kim");
        for (int i = 0; i < 49; i++)
        {
            gameData.Tick();
        }
        Assert.IsNotNull(gameData.Inhabitants[0].Name);
        Assert.IsNotNull(gameData.Inhabitants[0].Home);
        Assert.IsNotNull(gameData.Inhabitants[0].DistanceFromWorkplace);
        gameData.Inhabitants[0].Qualification = Qualification.PrimaryEducation;
        Assert.AreEqual(gameData.Inhabitants[0].Qualification, Qualification.PrimaryEducation);
        Assert.NotNull(gameData.ZoneBuildings[0].Variation);
        Assert.AreEqual(gameData.ZoneBuildings[0].MaintenanceExpenseType,ExpenseType.Level1ResidentialFieldIsSet);
        List<Point> cells = new List<Point>();
        cells.Add(gameData.ZoneBuildings[0].Location);
        List<Backend.Buildings.ZoneBuilding> buildings = new List<Backend.Buildings.ZoneBuilding>();
        buildings.Add(gameData.ZoneBuildings[0]);
        gameData.ZoneBuildings[0].Zone = new Backend.Zones.Zone(ZoneType.Residential,cells, buildings);
        Assert.AreNotEqual(gameData.ZoneBuildings[0].Zone, gameData.Zones[3]);
        Inhabitant testinhab = new Inhabitant(20);
        testinhab.WorkPlace = null;
        testinhab.Home = null;
        testinhab.MadForHouseLossDays = 1;
        testinhab.MadForWorkplaceLossDays = 0;
        Assert.AreEqual(1, testinhab.MadForHouseLossDays);
        Assert.AreEqual(testinhab.CurrentIncome , 0);
        Assert.IsNull(testinhab.Home);
        Assert.AreEqual(Int32.MaxValue,testinhab.DistanceFromWorkplace);
        gameData.Zones[2].ZoneType = ZoneType.Residential;
        Assert.AreEqual(gameData.Zones[0].ZoneType,ZoneType.Special);
        gameData.FactoryLayer.UpgradeZone(gameData.Zones[1]);
        gameData.FactoryLayer.UpgradeZone(gameData.Zones[2]);
        gameData.FactoryLayer.UpgradeZone(gameData.Zones[0]);
        gameData.FactoryLayer.UpgradeZone(gameData.Zones[1]);
        gameData.FactoryLayer.UpgradeZone(gameData.Zones[2]);
        gameData.FactoryLayer.UpgradeZone(gameData.Zones[0]);
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level1ResidentialSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level1IndustrialSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level1ServiceSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level2ResidentialFieldIsSet));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level2IndustrialFieldIsSet));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level2ServiceFieldIsSet));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level2ResidentialSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level2IndustrialSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level2ServiceSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level3ResidentialFieldIsSet));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level3IndustrialFieldIsSet));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level3ServiceFieldIsSet));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level3ResidentialSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level3IndustrialSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.Level3ServiceSetBackToDefault));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.ResidentialUpgradedToLevel2));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.ResidentialUpgradedToLevel3));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.IndustrialUpgradedToLevel2));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.IndustrialUpgradedToLevel3));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.ServiceUpgradedToLevel2));
        Assert.IsNotNull(gameData.GetExpense(ExpenseType.ServiceUpgradedToLevel3));

        gameData.Inhabitants[0].TransformIntoPensioner();
        Assert.IsNotNull(gameData.Inhabitants[0].Pension);

        GameData prevGameData = gameData;
        Assert.Throws<Exception>(() => DataPersistenceManager.SaveGame(4, Application.persistentDataPath, gameData));
        Assert.Throws<Exception>(() => DataPersistenceManager.LoadGame(4, Application.persistentDataPath));
        DataPersistenceManager.SaveGame(1,Application.persistentDataPath,gameData);
        gameData = DataPersistenceManager.LoadGame(1, Application.persistentDataPath);
        
        gameData.GameTime.Equals(prevGameData.GameTime);
        gameData.CityName.Equals(prevGameData.CityName);
        gameData.MapWidth.Equals(prevGameData.MapWidth);
        gameData.MapHeight.Equals(prevGameData.MapHeight);
        gameData.Balance.Equals(prevGameData.Balance);
        gameData.Zones.Equals(prevGameData.Zones);
        gameData.ZoneBuildings.Equals(prevGameData.ZoneBuildings);
        gameData.SecondarySchools.Equals(prevGameData.SecondarySchools);
        gameData.Universities.Equals(prevGameData.Universities);
        gameData.Stadiums.Equals(prevGameData.Stadiums);
        gameData.PoliceStations.Equals(prevGameData.PoliceStations);
        gameData.FireStations.Equals(prevGameData.FireStations);
        gameData.Roads.Equals(prevGameData.Roads);
        gameData.Forests.Equals(prevGameData.Forests);
        gameData.PowerLines.Equals(prevGameData.PowerLines);
        gameData.PowerPlants.Equals(prevGameData.PowerPlants);
        gameData.Inhabitants.Equals(prevGameData.Inhabitants);
        
    }
    [Test]

    public void BalanceTests()
    {
        GameData gameData = new GameData(50, 50, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        gameData.Balance = 40000;
        Assert.AreEqual(40000, gameData.Balance);
    }

    [Test]
    public void TickTests()
    {
        GameData gameData = new GameData(50, 50, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        DateResponse startingDate=gameData.GetGameDate();
        gameData.Tick();
        if(gameData.GetGameDate().Day!=1)
        {
            Assert.Greater(gameData.GetGameDate().Day, startingDate.Day);
            Assert.AreEqual(gameData.GetGameDate().Month, startingDate.Month);
            Assert.AreEqual(gameData.GetGameDate().Year, startingDate.Year);
        }
        else if(gameData.GetGameDate().Month!=12)
        {
            Assert.Less(gameData.GetGameDate().Day, startingDate.Day);
            Assert.Greater(gameData.GetGameDate().Month, startingDate.Month);
            Assert.AreEqual(gameData.GetGameDate().Year, startingDate.Year);
        }
        else
        {
            Assert.Less(gameData.GetGameDate().Day, startingDate.Day);
            Assert.Less(gameData.GetGameDate().Month, startingDate.Month);
            Assert.Greater(gameData.GetGameDate().Year, startingDate.Year);
        }
        
        
        
        for (int i = 0; i < 35; i++)
        {
            gameData.Tick();
        }
        Assert.Greater(gameData.GetGameDate().Month, startingDate.Month);
    }
    [Test]
    public void FireTests()
    {
        GameData gameData = new GameData(50, 50, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }

        for (int j = 1; j < 4; j++)
        {
            gameData.FactoryLayer.BuildRoad(new Point(gameData.firstPieceOfRoad.X + j, gameData.firstPieceOfRoad.Y));
        }
        for (int j = 1; j < 3; j++)
        {
            gameData.FactoryLayer.BuildRoad(new Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y + j));
        }
        for (int j = 4; j < 6; j++)
        {
            gameData.FactoryLayer.BuildRoad(new Point(gameData.firstPieceOfRoad.X + j, gameData.firstPieceOfRoad.Y + 2));
        }
        for (int j = 1; j < 5; j++)
        {
            gameData.FactoryLayer.BuildRoad(new Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y + 2 - j));
        }
        for (int j = 6; j < 10; j++)
        {
            gameData.FactoryLayer.BuildRoad(new Point(gameData.firstPieceOfRoad.X + j, gameData.firstPieceOfRoad.Y-2));
        }

        gameData.FactoryLayer.BuildRoad(new Point(gameData.firstPieceOfRoad.X + 9, gameData.firstPieceOfRoad.Y - 1));
        gameData.FactoryLayer.BuildRoad(new Point(gameData.firstPieceOfRoad.X + 9, gameData.firstPieceOfRoad.Y));
        Assert.True(gameData.Roads[gameData.Roads.Count-1].IsActive);
        gameData.FactoryLayer.BuildFireStation(new Point(gameData.firstPieceOfRoad.X, gameData.firstPieceOfRoad.Y+1));
        gameData.FactoryLayer.BuildPowerPlant(new Point(gameData.firstPieceOfRoad.X, gameData.firstPieceOfRoad.Y + 3));
        Assert.True(gameData.FireStations[0].IsActive);
        gameData.FactoryLayer.BuildStadium(new Point(gameData.firstPieceOfRoad.X + 10, gameData.firstPieceOfRoad.Y));
        Assert.True(gameData.Stadiums[0].IsActive);
        Assert.IsNull(gameData.FireStations[0].FireTruckOrientation);
        Assert.False(gameData.FireStations[0].IsDispatched);
        Assert.False(gameData.FireStations[0].IsOnDuty);
        Assert.IsNotNull(gameData.FireStations[0].PowerNeeded);
        gameData.Stadiums[0].IsOnFire = true;
        gameData.Stadiums[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        DateTime since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.Tick();
        gameData.HandleFiretruck(gameData.Stadiums[0]);
        gameData.Tick();
        Assert.True(gameData.FireStations[0].IsDispatched);
        Assert.True(gameData.FireStations[0].IsOnDuty);
        for (int i = 0; i < 40; i++)
        {
            gameData.PowerPlants[0].IsOnFire = false;
            gameData.PowerPlants[0].IsOnFireSince = null;
            gameData.Tick();
            gameData.DeleteDisposableFireTrucks();
        }
        if(gameData.Stadiums[0].IsOnFire)
        {
            Assert.AreNotEqual(gameData.Stadiums[0].IsOnFireSince.Value.Day, since.Day);
        }
        else
        {
            Assert.IsNull(gameData.Stadiums[0].IsOnFireSince);
        }
        gameData.Stadiums[0].IsOnFire = true;
        gameData.Stadiums[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.Tick();
        gameData.HandleFiretruck(gameData.Stadiums[0]);
        for (int i = 0; i < 20; i++)
        {
            gameData.PowerPlants[0].IsOnFire = false;
            gameData.PowerPlants[0].IsOnFireSince = null;
            gameData.Tick();
            gameData.DeleteDisposableFireTrucks();
        }
        if (gameData.Stadiums[0].IsOnFire)
        {
            Assert.AreNotEqual(gameData.Stadiums[0].IsOnFireSince.Value.Day, since.Day);
        }
        else
        {
            Assert.IsNull(gameData.Stadiums[0].IsOnFireSince);
        }
        for (int i = 0; i < 20; i++)
        {
            gameData.PowerPlants[0].IsOnFire = false;
            gameData.PowerPlants[0].IsOnFireSince = null;
            gameData.Tick();
            gameData.DeleteDisposableFireTrucks();
        }
        gameData.FactoryLayer.BuildSecondarySchool(new Point(6, gameData.firstPieceOfRoad.Y));
        Assert.True(gameData.SecondarySchools[0].IsActive);
        gameData.SecondarySchools[0].IsOnFire = true;
        gameData.SecondarySchools[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        for (int i = 0; i < 45; i++)
        {
            gameData.PowerPlants[0].IsOnFire = false;
            gameData.PowerPlants[0].IsOnFireSince = null;
            gameData.DeleteDisposableFireTrucks();
            gameData.Tick();
        }
        gameData.HandleFiretruck(gameData.SecondarySchools[0]);
        for (int i = 0; i < 8; i++)
        {
            gameData.PowerPlants[0].IsOnFire = false;
            gameData.PowerPlants[0].IsOnFireSince = null;
            gameData.DeleteDisposableFireTrucks();
            gameData.Tick();
        }
        Assert.IsTrue(PowerHelper.HasEnoughPower(gameData.FireStations[0]));

        Assert.IsTrue(gameData.FireStations[0].IsActive);
        Assert.AreEqual(gameData.SecondarySchools.Count, 0);
        Assert.True(gameData.FireStations[0].IsDispatched);
        Assert.False(gameData.FireStations[0].IsOnDuty);

        gameData.Tick();
        gameData.FactoryLayer.BuildSecondarySchool(new Point(6, gameData.firstPieceOfRoad.Y));
        Assert.True(gameData.SecondarySchools[0].IsActive);
        gameData.SecondarySchools[0].IsOnFire = true;
        gameData.SecondarySchools[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.HandleFiretruck(gameData.SecondarySchools[0]);
        for (int i = 0; i < 30; i++)
        {
            gameData.DeleteDisposableFireTrucks();
            gameData.Tick();
        }

        Assert.IsNotNull(PowerHelper.GetPowerConsumption(gameData.FireStations[0]));
    }

    [Test]
    public void PoliceStationTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }

        Assert.AreEqual(0, gameData.PoliceStations.Count);
        gameData.FactoryLayer.BuildPoliceStation(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(1, gameData.PoliceStations.Count);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        DateResponse built = new DateResponse(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.FactoryLayer.BuildPoliceStation(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        gameData.FactoryLayer.DestroyPoliceStation(gameData.PoliceStations[0]);
        Assert.AreEqual(1, gameData.PoliceStations.Count);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 5].Building);
        Assert.AreEqual(gameData.PoliceStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 5].Building);
        List<Point> cells = new List<Point>();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y + 1));
        gameData.Zones.Add(new Backend.Zones.Zone(ZoneType.Residential, cells, new List<ZoneBuilding>()));
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Zone.ZoneType);
        cells.Clear();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y + 1));
        HWMat<int> matrix = new HWMat<int>(25, 25);
        for (int i = 0; i < 25; i++)
        {
            for (int j = 0; j < 25; j++)
            {
                matrix[i, j] = 0;
            }
        }
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3] = 1;
        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Zone.ZoneType);
        foreach (var policeStation in gameData.PoliceStations)
        {
            Assert.AreEqual(PowerHelper.GetPowerConsumption(policeStation), 0);
            Assert.False(policeStation.IsOnFire);
            Assert.IsNotNull(policeStation.PowerNeeded);
        }
        Assert.False(gameData.PoliceStations[0].IsOnFireSince.HasValue);
        gameData.PoliceStations[0].IsOnFire = true;
        DateTime since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.PoliceStations[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        for (int i = 0; i < 10; i++)
        {
            gameData.Tick();
        }
        Assert.AreEqual(since, gameData.PoliceStations[0].IsOnFireSince);
        gameData.PoliceStations[0].HasDispatchedTruck = true;
        Assert.IsTrue(gameData.PoliceStations[0].HasDispatchedTruck);
        Assert.IsNotNull(gameData.GetExpense(gameData.PoliceStations[0].MaintenanceExpenseType));
        Assert.AreEqual(gameData.PoliceStations[0].MaintenanceExpenseType, ExpenseType.PoliceStationMaintenance);
        Assert.AreEqual(gameData.PoliceStations[0].BuildDate.Year, built.Year);
        Assert.AreEqual(gameData.PoliceStations[0].BuildDate.Month, built.Month);
        Assert.AreEqual(gameData.PoliceStations[0].BuildDate.Day, built.Day);
        Assert.IsNotNull(gameData.PoliceStations[0].GetPoint(gameData.PoliceStations[0].GetPointCount()-1));
    }

    [Test]
    public void FireStationTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }

        Assert.AreEqual(0, gameData.FireStations.Count);
        gameData.FactoryLayer.BuildFireStation(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(1, gameData.FireStations.Count);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        DateResponse built = new DateResponse(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.FactoryLayer.BuildFireStation(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        gameData.FactoryLayer.DestroyFireStation(gameData.FireStations[0]);
        Assert.AreEqual(1, gameData.FireStations.Count);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 5].Building);
        Assert.AreEqual(gameData.FireStations[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 5].Building);
        List<Point> cells = new List<Point>();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y + 1));
        gameData.Zones.Add(new Backend.Zones.Zone(ZoneType.Residential, cells, new List<ZoneBuilding>()));
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Zone.ZoneType);
        cells.Clear();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y + 1));
        HWMat<int> matrix = new HWMat<int>(25, 25);
        for (int i = 0; i < 25; i++)
        {
            for (int j = 0; j < 25; j++)
            {
                matrix[i, j] = 0;
            }
        }
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3] = 1;
        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Zone.ZoneType);
        foreach (var fireStation in gameData.FireStations)
        {
            Assert.AreEqual(PowerHelper.GetPowerConsumption(fireStation), 0);
            //todo Assert.AreEqual(0,FireStation.PowerNeeded)
            Assert.IsNotNull(fireStation.PowerNeeded);
        }
        Assert.IsNotNull(gameData.GetExpense(gameData.FireStations[0].MaintenanceExpenseType));
        Assert.AreEqual(gameData.FireStations[0].MaintenanceExpenseType, ExpenseType.FireStationMaintenance);
        Assert.AreEqual(gameData.FireStations[0].BuildDate.Year, built.Year);
        Assert.AreEqual(gameData.FireStations[0].BuildDate.Month, built.Month);
        Assert.AreEqual(gameData.FireStations[0].BuildDate.Day, built.Day);
    }

    [Test]
    public void SecondarySchoolTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }

        Assert.AreEqual(0, gameData.SecondarySchools.Count);
        gameData.FactoryLayer.BuildSecondarySchool(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(1, gameData.SecondarySchools.Count);
        Assert.AreEqual(gameData.SecondarySchools[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.SecondarySchools[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        DateResponse built = new DateResponse(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.FactoryLayer.BuildSecondarySchool(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        gameData.FactoryLayer.DestroySecondarySchool(gameData.SecondarySchools[0]);
        Assert.AreEqual(1, gameData.SecondarySchools.Count);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.SecondarySchools[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.SecondarySchools[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 5].Building);
        List<Point> cells = new List<Point>();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y + 1));
        gameData.Zones.Add(new Backend.Zones.Zone(ZoneType.Residential, cells, new List<ZoneBuilding>()));
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Zone.ZoneType);
        cells.Clear();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y + 1));
        HWMat<int> matrix = new HWMat<int>(25, 25);
        for (int i = 0; i < 25; i++)
        {
            for (int j = 0; j < 25; j++)
            {
                matrix[i, j] = 0;
            }
        }
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3] = 1;
        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Zone.ZoneType);
        foreach (var secondarySchool in gameData.SecondarySchools)
        {
            Assert.AreEqual(PowerHelper.GetPowerConsumption(secondarySchool), 0);
            Assert.False(secondarySchool.IsOnFire);
            Assert.IsNotNull(secondarySchool.PowerNeeded);
        }
        Assert.False(gameData.SecondarySchools[0].IsOnFireSince.HasValue);
        gameData.SecondarySchools[0].IsOnFire = true;
        DateTime since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.SecondarySchools[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        for (int i = 0; i < 10; i++)
        {
            gameData.Tick();
        }
        Assert.AreEqual(since, gameData.SecondarySchools[0].IsOnFireSince);
        gameData.SecondarySchools[0].HasDispatchedTruck = true;
        Assert.IsTrue(gameData.SecondarySchools[0].HasDispatchedTruck);
        Assert.IsNotNull(gameData.GetExpense(gameData.SecondarySchools[0].MaintenanceExpenseType));
        Assert.AreEqual(gameData.SecondarySchools[0].MaintenanceExpenseType, ExpenseType.SchoolMaintenance);
        Assert.AreEqual(gameData.SecondarySchools[0].BuildDate.Year, built.Year);
        Assert.AreEqual(gameData.SecondarySchools[0].BuildDate.Month, built.Month);
        Assert.AreEqual(gameData.SecondarySchools[0].BuildDate.Day, built.Day);
        Assert.IsNotNull(gameData.SecondarySchools[0].GetPoint(gameData.SecondarySchools[0].GetPointCount() - 1));
    }

    [Test]
    public void UniversityTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }

        Assert.AreEqual(0, gameData.Universities.Count);
        gameData.FactoryLayer.BuildUniversity(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(1, gameData.Universities.Count);
        Assert.AreEqual(gameData.Universities[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.Universities[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        DateResponse built = new DateResponse(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.FactoryLayer.BuildUniversity(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        gameData.FactoryLayer.DestroyUniversity(gameData.Universities[0]);
        Assert.AreEqual(1, gameData.Universities.Count);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.Universities[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.Universities[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 5].Building);
        List<Point> cells = new List<Point>();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y + 1));
        gameData.Zones.Add(new Backend.Zones.Zone(ZoneType.Residential, cells, new List<ZoneBuilding>()));
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Zone.ZoneType);
        cells.Clear();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y + 1));
        HWMat<int> matrix = new HWMat<int>(25, 25);
        for (int i = 0; i < 25; i++)
        {
            for (int j = 0; j < 25; j++)
            {
                matrix[i, j] = 0;
            }
        }
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3] = 1;
        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Zone.ZoneType);
        foreach (var university in gameData.Universities)
        {
            Assert.AreEqual(PowerHelper.GetPowerConsumption(university), 0);
            Assert.False(university.IsOnFire);
            Assert.IsNotNull(university.PowerNeeded);
        }
        Assert.False(gameData.Universities[0].IsOnFireSince.HasValue);
        gameData.Universities[0].IsOnFire = true;
        DateTime since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.Universities[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        for (int i = 0; i < 10; i++)
        {
            gameData.Tick();
        }
        Assert.AreEqual(since, gameData.Universities[0].IsOnFireSince);
        gameData.Universities[0].HasDispatchedTruck = true;
        Assert.IsTrue(gameData.Universities[0].HasDispatchedTruck);
        Assert.IsNotNull(gameData.GetExpense(gameData.Universities[0].MaintenanceExpenseType));
        Assert.AreEqual(gameData.Universities[0].MaintenanceExpenseType, ExpenseType.UniversityMaintenance);
        Assert.AreEqual(gameData.Universities[0].BuildDate.Year, built.Year);
        Assert.AreEqual(gameData.Universities[0].BuildDate.Month, built.Month);
        Assert.AreEqual(gameData.Universities[0].BuildDate.Day, built.Day);
        Assert.IsNotNull(gameData.Universities[0].GetPoint(gameData.Universities[0].GetPointCount() - 1));
    }

    [Test]

    public void PowerPlantTests()
    {
        GameData gameData = new GameData(25, 25, 1, System.DateTime.Now, "Test");
        gameData.StartNewGame();
        while (gameData.Forests.Count > 0)
        {
            gameData.FactoryLayer.DestroyForest(gameData.Forests.First());
        }

        Assert.AreEqual(0, gameData.PowerPlants.Count);
        gameData.FactoryLayer.BuildPowerPlant(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        Assert.AreEqual(1, gameData.PowerPlants.Count);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        DateResponse built = new DateResponse(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.FactoryLayer.BuildPowerPlant(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        gameData.FactoryLayer.DestroyPowerPlant(gameData.PowerPlants[0]);
        Assert.AreEqual(1, gameData.PowerPlants.Count);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.IsNull(gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Building);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 4].Building);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 5].Building);
        Assert.AreEqual(gameData.PowerPlants[0], gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 5].Building);
        List<Point> cells = new List<Point>();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 4, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 5, gameData.firstPieceOfRoad.Y + 1));
        gameData.Zones.Add(new Backend.Zones.Zone(ZoneType.Residential, cells, new List<ZoneBuilding>()));
        Assert.AreEqual(ZoneType.Special, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 4].Zone.ZoneType);
        cells.Clear();
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 2, gameData.firstPieceOfRoad.Y + 1));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y));
        cells.Add(new System.Drawing.Point(gameData.firstPieceOfRoad.X + 3, gameData.firstPieceOfRoad.Y + 1));
        HWMat<int> matrix = new HWMat<int>(25, 25);
        for (int i = 0; i < 25; i++)
        {
            for (int j = 0; j < 25; j++)
            {
                matrix[i, j] = 0;
            }
        }
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 2] = 1;
        matrix[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 3] = 1;
        matrix[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3] = 1;
        gameData.FactoryLayer.RearrangeZoneLayout(matrix,new List<Home>(),new List<Workplace>());
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y, gameData.firstPieceOfRoad.X + 2].Zone.ZoneType);
        Assert.AreEqual(ZoneType.Residential, gameData.CellData[gameData.firstPieceOfRoad.Y + 1, gameData.firstPieceOfRoad.X + 3].Zone.ZoneType);
        Assert.AreEqual(gameData.PowerPlants[0].MaintenanceExpenseType, ExpenseType.PowerplantMaintenance);
        Assert.AreEqual(gameData.PowerPlants[0].BuildDate.Year, built.Year);
        Assert.AreEqual(gameData.PowerPlants[0].BuildDate.Month, built.Month);
        Assert.AreEqual(gameData.PowerPlants[0].BuildDate.Day, built.Day);
        foreach (var powerPlant in gameData.PowerPlants)
        {
            Assert.False(powerPlant.IsOnFire);
            Assert.IsNotNull(gameData.PowerPlants.Capacity);
        }
        Assert.False(gameData.PowerPlants[0].IsOnFireSince.HasValue);
        gameData.PowerPlants[0].IsOnFire = true;
        DateTime since = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        gameData.PowerPlants[0].IsOnFireSince = new DateTime(gameData.GetGameDate().Year, gameData.GetGameDate().Month, gameData.GetGameDate().Day);
        for (int i = 0; i < 10; i++)
        {
            gameData.Tick();
        }
        Assert.AreEqual(since, gameData.PowerPlants[0].IsOnFireSince);
        gameData.PowerPlants[0].HasDispatchedTruck = true;
        Assert.IsTrue(gameData.PowerPlants[0].HasDispatchedTruck);
        Assert.IsNotNull(gameData.GetExpense(gameData.PowerPlants[0].MaintenanceExpenseType));
        Assert.AreEqual(gameData.PowerPlants[0].MaintenanceExpenseType, ExpenseType.PowerplantMaintenance);
        Assert.AreEqual(gameData.PowerPlants[0].BuildDate.Year, built.Year);
        Assert.AreEqual(gameData.PowerPlants[0].BuildDate.Month, built.Month);
        Assert.AreEqual(gameData.PowerPlants[0].BuildDate.Day, built.Day);
        Assert.IsNotNull(gameData.PowerPlants[0].Capacity);
        Assert.IsNotNull(gameData.PowerPlants[0].PowerNeeded);
        gameData.PowerPlants[0].PowerConsumption = 10;
        Assert.AreEqual(10,gameData.PowerPlants[0].PowerConsumption);
    }
}
